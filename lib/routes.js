FlowRouter.route('/', {
    name: 'default',
    action(){
        BlazeLayout.render("world");
    }
});


FlowRouter.route('/search', {
    name: 'search',
    action(){
        BlazeLayout.render("world", {main:"search"});
    }
});

FlowRouter.route('/showList', {
    name: 'list',
    action(){
        BlazeLayout.render("world", {main:"showList"});
    }
});

FlowRouter.route('/addRoute', {
    name: 'addRoute',
    action: function(params, queryParams){
        BlazeLayout.render("world", {main:"addRoute"});
    }
});

FlowRouter.route('/removeRoute', {
    name: 'addRoute',
    action: function(params, queryParams){
        BlazeLayout.render("world", {main:"removeRoute"});
    }
});

// Initial build with string param
/*FlowRouter.route("/addUser/:param",{
    name: 'addwithParams',
    action (){
        BlazeLayout.render("world", {main:'userform'});
    }
});*/

FlowRouter.route("/addUser",{
    name: 'addwithParams',
    action: function(params, queryParams){
        BlazeLayout.render("world", {main:'userform'});
    }
})

FlowRouter.route("/viewUser",{
    name: 'viewwithParams',
    action: function(params, queryParams){
        BlazeLayout.render("world", {main:'userdetail'});
    }
})

FlowRouter.route('/*', {
    name: 'unknownRoute',
    action() {
        BlazeLayout.render("unknownRoute");
    }
});

// Exposed FlowRouter to console for development! Will remove!s
this.FlowRouter = FlowRouter;