import { Mongo } from "meteor/mongo";
import { Meteor } from "meteor/meteor";
import { check } from 'meteor/check';

export const Userinfo = new Mongo.Collection('userinfo');
export const NeighbourInfo = new Mongo.Collection('neighbourInfo');

if(Meteor.isServer){
    Meteor.publish('userinfo', function userinfoPublication(){
        return Userinfo.find({});
    })
    Meteor.publish('neighbourInfo', function(){
        return NeighbourInfo.find({});
    })
}

Meteor.methods({
    'user.insert'(title, device ,latitude, longitude, country){
        check(title, String);
        check(device, String);
        check(latitude, String);
        check(longitude, String);
        check(country, String);

        Userinfo.insert({
            title,
            device,
            latitude,
            longitude,
            country,
            createdAt: new Date(),
        });
    },
    'user.remove'(userID){
        check(userID, String);
        Userinfo.remove(userID);
    },
    'user.setEdit'(userID, setEdit) {

        check(userID, String);
    
        check(setEdit, Boolean);
        Userinfo.update(userID, { $set: { editing: setEdit } });
    },
    'user.update'(userID, title, devi){
        check(userID, String);
        check(title, String);
        check(devi, String);
        Userinfo.update(userID, { $set: { title: title,  device: devi} } );
    },
    'user.addNeighb'(userID, nextUserID){
        Userinfo.update(userID, { $push: { neighbour: nextUserID}});
    },
    'neighbour.insert'(userID, title, latitude, longitude){
        check(userID, String);
        check(title, String);
        check(longitude, String);
        check(latitude, String);
       NeighbourInfo.insert({userID,title,latitude, longitude, neighbour:[], createdAt: new Date()})
    },
    'neighbour.add'(userID, neighbourID){
        check(userID, String);
        check(neighbourID, String);
        //Method 1:
        // if(NeighbourInfo.find({userID: userID}).fetch()[0].neighbour.includes(neighbourID)){
        // Method 2:
        NeighbourInfo.update({userID: userID}, {$push: {neighbour: neighbourID}});
    },
    'neighbour.removeNeighbour'(user1, user2){
        NeighbourInfo.update({"userID":user1}, {$pull: {"neighbour": user2}});
        NeighbourInfo.update({"userID":user2}, {$pull: {"neighbour": user1}});
    },
    'neighbour.remove'(userID){
        check(userID, String);
        NeighbourInfo.update({"neighbour":userID}, {$pull:{"neighbour": userID}}, {multi: true});
        NeighbourInfo.remove({userID: userID});
    },
})