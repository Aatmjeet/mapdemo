import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import './world.js';
import './body.html';


// Subscribe to the database!
Template.body.created = function(){
    Meteor.subscribe('userinfo');
    Meteor.subscribe('neighbourInfo');
}
