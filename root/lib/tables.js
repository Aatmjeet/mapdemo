import Tabular from "meteor/aldeed:tabular";
import { Userinfo } from "../../import/api/userinfo.js";
import moment from 'moment';

TabularTables = {};

TabularTables.Users = new Tabular.Table({
    name: "Users",
    collection: Userinfo,
    pub: "Userinfo",
    columns: [
       {
          data:"title",
          title:"Title",
       },
       {
          data:"device",
          title:"Device",
       },
       {
          data:"country",
          title:"Country",
       },
       {
          data:"createdAt",
          title:"CreatedAt",
          render:function (val) {
              return moment(val).format('DD/MM/YYYY H:mm');
          },
       },
   ],
    initComplete: function() {
        $('.dataTables_empty').html('processing');
    },
})

export default TabularTables;