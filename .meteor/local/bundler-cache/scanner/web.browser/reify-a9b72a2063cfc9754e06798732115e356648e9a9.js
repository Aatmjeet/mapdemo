let abs,epsilon;module.link("./math.js",{abs(v){abs=v},epsilon(v){epsilon=v}},0);

module.exportDefault(function(a, b) {
  return abs(a[0] - b[0]) < epsilon && abs(a[1] - b[1]) < epsilon;
});
