let sinusoidalRaw;module.link("../sinusoidal.js",{sinusoidalRaw(v){sinusoidalRaw=v}},0);let interrupt;module.link("./index.js",{default(v){interrupt=v}},1);


var lobes = [[ // northern hemisphere
  [[-180,   0], [-110,  90], [ -40,   0]],
  [[ -40,   0], [   0,  90], [  40,   0]],
  [[  40,   0], [ 110,  90], [ 180,   0]]
], [ // southern hemisphere
  [[-180,   0], [-110, -90], [ -40,   0]],
  [[ -40,   0], [   0, -90], [  40,   0]],
  [[  40,   0], [ 110, -90], [ 180,   0]]
]];

module.exportDefault(function() {
  return interrupt(sinusoidalRaw, lobes)
      .scale(152.63)
      .rotate([-20, 0]);
});
