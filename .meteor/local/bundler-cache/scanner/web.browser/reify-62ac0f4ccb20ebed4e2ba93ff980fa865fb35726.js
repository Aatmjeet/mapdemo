module.export({bonneRaw:()=>bonneRaw});let parallel1;module.link("./parallel1.js",{default(v){parallel1=v}},0);let atan2,cos,sin,sqrt,tan;module.link("./math.js",{atan2(v){atan2=v},cos(v){cos=v},sin(v){sin=v},sqrt(v){sqrt=v},tan(v){tan=v}},1);let sinusoidalRaw;module.link("./sinusoidal.js",{sinusoidalRaw(v){sinusoidalRaw=v}},2);



function bonneRaw(phi0) {
  if (!phi0) return sinusoidalRaw;
  var cotPhi0 = 1 / tan(phi0);

  function forward(lambda, phi) {
    var rho = cotPhi0 + phi0 - phi,
        e = rho ? lambda * cos(phi) / rho : rho;
    return [rho * sin(e), cotPhi0 - rho * cos(e)];
  }

  forward.invert = function(x, y) {
    var rho = sqrt(x * x + (y = cotPhi0 - y) * y),
        phi = cotPhi0 + phi0 - rho;
    return [rho / cos(phi) * atan2(x, y), phi];
  };

  return forward;
}

module.exportDefault(function() {
  return parallel1(bonneRaw)
      .scale(123.082)
      .center([0, 26.1441])
      .parallel(45);
});
