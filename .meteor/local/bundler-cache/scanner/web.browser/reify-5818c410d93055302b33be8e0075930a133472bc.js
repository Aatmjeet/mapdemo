module.export({homolosineRaw:()=>homolosineRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let abs;module.link("./math.js",{abs(v){abs=v}},1);let mollweideRaw;module.link("./mollweide.js",{mollweideRaw(v){mollweideRaw=v}},2);let sinusoidalRaw;module.link("./sinusoidal.js",{sinusoidalRaw(v){sinusoidalRaw=v}},3);let sinuMollweidePhi,sinuMollweideY;module.link("./sinuMollweide.js",{sinuMollweidePhi(v){sinuMollweidePhi=v},sinuMollweideY(v){sinuMollweideY=v}},4);





function homolosineRaw(lambda, phi) {
  return abs(phi) > sinuMollweidePhi
      ? (lambda = mollweideRaw(lambda, phi), lambda[1] -= phi > 0 ? sinuMollweideY : -sinuMollweideY, lambda)
      : sinusoidalRaw(lambda, phi);
}

homolosineRaw.invert = function(x, y) {
  return abs(y) > sinuMollweidePhi
      ? mollweideRaw.invert(x, y + (y > 0 ? sinuMollweideY : -sinuMollweideY))
      : sinusoidalRaw.invert(x, y);
};

module.exportDefault(function() {
  return projection(homolosineRaw)
      .scale(152.63);
});
