module.export({cylindricalEqualAreaRaw:()=>cylindricalEqualAreaRaw});let asin,cos,sin;module.link("./math.js",{asin(v){asin=v},cos(v){cos=v},sin(v){sin=v}},0);let parallel1;module.link("./parallel1.js",{default(v){parallel1=v}},1);


function cylindricalEqualAreaRaw(phi0) {
  var cosPhi0 = cos(phi0);

  function forward(lambda, phi) {
    return [lambda * cosPhi0, sin(phi) / cosPhi0];
  }

  forward.invert = function(x, y) {
    return [x / cosPhi0, asin(y * cosPhi0)];
  };

  return forward;
}

module.exportDefault(function() {
  return parallel1(cylindricalEqualAreaRaw)
      .parallel(38.58) // acos(sqrt(width / height / pi)) * radians
      .scale(195.044); // width / (sqrt(width / height / pi) * 2 * pi)
});
