module.export({littrowRaw:()=>littrowRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let acos,asin,cos,sign,sin,tan,sqrt,sqrt1_2;module.link("./math.js",{acos(v){acos=v},asin(v){asin=v},cos(v){cos=v},sign(v){sign=v},sin(v){sin=v},tan(v){tan=v},sqrt(v){sqrt=v},sqrt1_2(v){sqrt1_2=v}},1);


function littrowRaw(lambda, phi) {
  return [
    sin(lambda) / cos(phi),
    tan(phi) * cos(lambda)
  ];
}

littrowRaw.invert = function(x, y) {
  var x2 = x * x,
      y2 = y * y,
      y2_1 = y2 + 1,
      x2_y2_1 = x2 + y2_1,
      cosPhi = x
          ? sqrt1_2 * sqrt((x2_y2_1 - sqrt(x2_y2_1 * x2_y2_1 - 4 * x2)) / x2)
          : 1 / sqrt(y2_1);
  return [
    asin(x * cosPhi),
    sign(y) * acos(cosPhi)
  ];
};

module.exportDefault(function() {
  return projection(littrowRaw)
      .scale(144.049)
      .clipAngle(90 - 1e-3);
});
