module.export({timesRaw:()=>timesRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let atan,quarterPi,sin,tan;module.link("./math.js",{atan(v){atan=v},quarterPi(v){quarterPi=v},sin(v){sin=v},tan(v){tan=v}},1);


function timesRaw(lambda, phi) {
  var t = tan(phi / 2),
      s = sin(quarterPi * t);
  return [
    lambda * (0.74482 - 0.34588 * s * s),
    1.70711 * t
  ];
}

timesRaw.invert = function(x, y) {
  var t = y / 1.70711,
      s = sin(quarterPi * t);
  return [
    x / (0.74482 - 0.34588 * s * s),
    2 * atan(t)
  ];
};

module.exportDefault(function() {
  return projection(timesRaw)
      .scale(146.153);
});
