module.export({ginzburg6Raw:()=>ginzburg6Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let ginzburgPolyconicRaw;module.link("./ginzburgPolyconic.js",{default(v){ginzburgPolyconicRaw=v}},1);let pi;module.link("./math.js",{pi(v){pi=v}},2);



var ginzburg6Raw = ginzburgPolyconicRaw(5 / 6 * pi, -0.62636, -0.0344, 0, 1.3493, -0.05524, 0, 0.045);

module.exportDefault(function() {
  return projection(ginzburg6Raw)
      .scale(130.945);
});
