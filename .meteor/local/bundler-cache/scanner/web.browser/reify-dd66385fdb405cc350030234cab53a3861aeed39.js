module.export({azimuthalRaw:()=>azimuthalRaw,azimuthalInvert:()=>azimuthalInvert});let asin,atan2,cos,sin,sqrt;module.link("../math.js",{asin(v){asin=v},atan2(v){atan2=v},cos(v){cos=v},sin(v){sin=v},sqrt(v){sqrt=v}},0);

function azimuthalRaw(scale) {
  return function(x, y) {
    var cx = cos(x),
        cy = cos(y),
        k = scale(cx * cy);
        if (k === Infinity) return [2, 0];
    return [
      k * cy * sin(x),
      k * sin(y)
    ];
  }
}

function azimuthalInvert(angle) {
  return function(x, y) {
    var z = sqrt(x * x + y * y),
        c = angle(z),
        sc = sin(c),
        cc = cos(c);
    return [
      atan2(x * sc, z * cc),
      asin(z && y * sc / z)
    ];
  }
}
