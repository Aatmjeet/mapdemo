module.export({ColorModifier:()=>ColorModifier});let __extends;module.link("tslib",{__extends(v){__extends=v}},0);let BaseObject;module.link("../../Base",{BaseObject(v){BaseObject=v}},1);let registry;module.link("../../Registry",{registry(v){registry=v}},2);
/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */


/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * A base class for color modifiers.
 *
 * @ignore Exclude from docs
 */
var ColorModifier = /** @class */ (function (_super) {
    __extends(ColorModifier, _super);
    /**
     * Constructor
     */
    function ColorModifier() {
        var _this = _super.call(this) || this;
        _this.className = "ColorModifier";
        _this.applyTheme();
        return _this;
    }
    /**
     * Modifies color value.
     *
     * @ignore Exclude from docs
     * @param value  Original color
     * @return Modified
     */
    ColorModifier.prototype.modify = function (value) {
        return value;
    };
    return ColorModifier;
}(BaseObject));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["ColorModifier"] = ColorModifier;
//# sourceMappingURL=ColorModifier.js.map