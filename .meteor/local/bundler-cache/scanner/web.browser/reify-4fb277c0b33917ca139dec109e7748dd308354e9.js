module.export({boggsRaw:()=>boggsRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let mollweideBromleyTheta;module.link("./mollweide.js",{mollweideBromleyTheta(v){mollweideBromleyTheta=v}},1);let abs,cos,epsilon,pi,quarterPi,sin,sqrt2;module.link("./math.js",{abs(v){abs=v},cos(v){cos=v},epsilon(v){epsilon=v},pi(v){pi=v},quarterPi(v){quarterPi=v},sin(v){sin=v},sqrt2(v){sqrt2=v}},2);



var k = 2.00276,
    w = 1.11072;

function boggsRaw(lambda, phi) {
  var theta = mollweideBromleyTheta(pi, phi);
  return [k * lambda / (1 / cos(phi) + w / cos(theta)), (phi + sqrt2 * sin(theta)) / k];
}

boggsRaw.invert = function(x, y) {
  var ky = k * y, theta = y < 0 ? -quarterPi : quarterPi, i = 25, delta, phi;
  do {
    phi = ky - sqrt2 * sin(theta);
    theta -= delta = (sin(2 * theta) + 2 * theta - pi * sin(phi)) / (2 * cos(2 * theta) + 2 + pi * cos(phi) * sqrt2 * cos(theta));
  } while (abs(delta) > epsilon && --i > 0);
  phi = ky - sqrt2 * sin(theta);
  return [x * (1 / cos(phi) + w / cos(theta)) / k, phi];
};

module.exportDefault(function() {
  return projection(boggsRaw)
      .scale(160.857);
});
