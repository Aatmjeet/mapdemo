module.export({wagner6Raw:()=>wagner6Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let pi,sqrt;module.link("./math.js",{pi(v){pi=v},sqrt(v){sqrt=v}},1);


function wagner6Raw(lambda, phi) {
  return [lambda * sqrt(1 - 3 * phi * phi / (pi * pi)), phi];
}

wagner6Raw.invert = function(x, y) {
  return [x / sqrt(1 - 3 * y * y / (pi * pi)), y];
};

module.exportDefault(function() {
  return projection(wagner6Raw)
      .scale(152.63);
});
