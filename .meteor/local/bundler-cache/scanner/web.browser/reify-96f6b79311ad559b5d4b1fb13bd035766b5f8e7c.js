module.export({eckert2Raw:()=>eckert2Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let abs,asin,pi,sign,sin,sqrt;module.link("./math.js",{abs(v){abs=v},asin(v){asin=v},pi(v){pi=v},sign(v){sign=v},sin(v){sin=v},sqrt(v){sqrt=v}},1);


function eckert2Raw(lambda, phi) {
  var alpha = sqrt(4 - 3 * sin(abs(phi)));
  return [
    2 / sqrt(6 * pi) * lambda * alpha,
    sign(phi) * sqrt(2 * pi / 3) * (2 - alpha)
  ];
}

eckert2Raw.invert = function(x, y) {
  var alpha = 2 - abs(y) / sqrt(2 * pi / 3);
  return [
    x * sqrt(6 * pi) / (2 * alpha),
    sign(y) * asin((4 - alpha * alpha) / 3)
  ];
};

module.exportDefault(function() {
  return projection(eckert2Raw)
      .scale(165.664);
});
