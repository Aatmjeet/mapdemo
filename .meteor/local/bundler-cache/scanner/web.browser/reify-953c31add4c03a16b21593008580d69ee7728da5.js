module.export({foucautSinusoidalRaw:()=>foucautSinusoidalRaw});let projectionMutator;module.link("d3-geo",{geoProjectionMutator(v){projectionMutator=v}},0);let cos,halfPi,pi,sin,sqrt;module.link("./math.js",{cos(v){cos=v},halfPi(v){halfPi=v},pi(v){pi=v},sin(v){sin=v},sqrt(v){sqrt=v}},1);let solve;module.link("./newton.js",{solve(v){solve=v}},2);



function foucautSinusoidalRaw(alpha) {
  var beta = 1 - alpha,
      equatorial = raw(pi, 0)[0] - raw(-pi, 0)[0],
      polar = raw(0, halfPi)[1] - raw(0, -halfPi)[1],
      ratio = sqrt(2 * polar / equatorial);

  function raw(lambda, phi) {
    var cosphi = cos(phi),
        sinphi = sin(phi);
    return [
      cosphi / (beta + alpha * cosphi) * lambda,
      beta * phi + alpha * sinphi
    ];
  }

  function forward(lambda, phi) {
    var p = raw(lambda, phi);
    return [p[0] * ratio, p[1] / ratio];
  }

  function forwardMeridian(phi) {
    return forward(0, phi)[1];
  }

  forward.invert = function(x, y) {
    var phi = solve(forwardMeridian, y),
        lambda = x / ratio * (alpha + beta / cos(phi));
    return [lambda, phi];
  };

  return forward;
}

module.exportDefault(function() {
  var alpha = 0.5,
      m = projectionMutator(foucautSinusoidalRaw),
      p = m(alpha);

  p.alpha = function(_) {
    return arguments.length ? m(alpha = +_) : alpha;
  };

  return p
      .scale(168.725);
});
