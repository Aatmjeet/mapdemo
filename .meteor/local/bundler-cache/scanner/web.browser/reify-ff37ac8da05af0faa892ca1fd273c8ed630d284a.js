module.export({conicConformalRaw:()=>conicConformalRaw});let abs,atan,atan2,cos,epsilon,halfPi,log,pi,pow,sign,sin,sqrt,tan;module.link("../math.js",{abs(v){abs=v},atan(v){atan=v},atan2(v){atan2=v},cos(v){cos=v},epsilon(v){epsilon=v},halfPi(v){halfPi=v},log(v){log=v},pi(v){pi=v},pow(v){pow=v},sign(v){sign=v},sin(v){sin=v},sqrt(v){sqrt=v},tan(v){tan=v}},0);let conicProjection;module.link("./conic.js",{conicProjection(v){conicProjection=v}},1);let mercatorRaw;module.link("./mercator.js",{mercatorRaw(v){mercatorRaw=v}},2);



function tany(y) {
  return tan((halfPi + y) / 2);
}

function conicConformalRaw(y0, y1) {
  var cy0 = cos(y0),
      n = y0 === y1 ? sin(y0) : log(cy0 / cos(y1)) / log(tany(y1) / tany(y0)),
      f = cy0 * pow(tany(y0), n) / n;

  if (!n) return mercatorRaw;

  function project(x, y) {
    if (f > 0) { if (y < -halfPi + epsilon) y = -halfPi + epsilon; }
    else { if (y > halfPi - epsilon) y = halfPi - epsilon; }
    var r = f / pow(tany(y), n);
    return [r * sin(n * x), f - r * cos(n * x)];
  }

  project.invert = function(x, y) {
    var fy = f - y, r = sign(n) * sqrt(x * x + fy * fy),
      l = atan2(x, abs(fy)) * sign(fy);
    if (fy * n < 0)
      l -= pi * sign(x) * sign(fy);
    return [l / n, 2 * atan(pow(f / r, 1 / n)) - halfPi];
  };

  return project;
}

module.exportDefault(function() {
  return conicProjection(conicConformalRaw)
      .scale(109.5)
      .parallels([30, 30]);
});
