module.export({wagner4Raw:()=>wagner4Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let sqrt,pi;module.link("./math.js",{sqrt(v){sqrt=v},pi(v){pi=v}},1);let mollweideBromleyRaw;module.link("./mollweide.js",{mollweideBromleyRaw(v){mollweideBromleyRaw=v}},2);



var A = 4 * pi + 3 * sqrt(3),
    B = 2 * sqrt(2 * pi * sqrt(3) / A);

var wagner4Raw = mollweideBromleyRaw(B * sqrt(3) / pi, B, A / 6);

module.exportDefault(function() {
  return projection(wagner4Raw)
      .scale(176.84);
});
