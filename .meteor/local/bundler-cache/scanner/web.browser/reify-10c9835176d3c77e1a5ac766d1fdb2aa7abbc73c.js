module.export({mtFlatPolarQuarticRaw:()=>mtFlatPolarQuarticRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let abs,asin,cos,epsilon,sin,sqrt,sqrt1_2,sqrt2;module.link("./math.js",{abs(v){abs=v},asin(v){asin=v},cos(v){cos=v},epsilon(v){epsilon=v},sin(v){sin=v},sqrt(v){sqrt=v},sqrt1_2(v){sqrt1_2=v},sqrt2(v){sqrt2=v}},1);


function mtFlatPolarQuarticRaw(lambda, phi) {
  var k = (1 + sqrt1_2) * sin(phi),
      theta = phi;
  for (var i = 0, delta; i < 25; i++) {
    theta -= delta = (sin(theta / 2) + sin(theta) - k) / (0.5 * cos(theta / 2) + cos(theta));
    if (abs(delta) < epsilon) break;
  }
  return [
    lambda * (1 + 2 * cos(theta) / cos(theta / 2)) / (3 * sqrt2),
    2 * sqrt(3) * sin(theta / 2) / sqrt(2 + sqrt2)
  ];
}

mtFlatPolarQuarticRaw.invert = function(x, y) {
  var sinTheta_2 = y * sqrt(2 + sqrt2) / (2 * sqrt(3)),
      theta = 2 * asin(sinTheta_2);
  return [
    3 * sqrt2 * x / (1 + 2 * cos(theta) / cos(theta / 2)),
    asin((sinTheta_2 + sin(theta)) / (1 + sqrt1_2))
  ];
};

module.exportDefault(function() {
  return projection(mtFlatPolarQuarticRaw)
      .scale(188.209);
});
