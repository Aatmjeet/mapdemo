module.export({transverseMercatorRaw:()=>transverseMercatorRaw});let atan,exp,halfPi,log,tan;module.link("../math.js",{atan(v){atan=v},exp(v){exp=v},halfPi(v){halfPi=v},log(v){log=v},tan(v){tan=v}},0);let mercatorProjection;module.link("./mercator.js",{mercatorProjection(v){mercatorProjection=v}},1);


function transverseMercatorRaw(lambda, phi) {
  return [log(tan((halfPi + phi) / 2)), -lambda];
}

transverseMercatorRaw.invert = function(x, y) {
  return [-y, 2 * atan(exp(x)) - halfPi];
};

module.exportDefault(function() {
  var m = mercatorProjection(transverseMercatorRaw),
      center = m.center,
      rotate = m.rotate;

  m.center = function(_) {
    return arguments.length ? center([-_[1], _[0]]) : (_ = center(), [_[1], -_[0]]);
  };

  m.rotate = function(_) {
    return arguments.length ? rotate([_[0], _[1], _.length > 2 ? _[2] + 90 : 90]) : (_ = rotate(), [_[0], _[1], _[2] - 90]);
  };

  return rotate([0, 0, 90])
      .scale(159.155);
});
