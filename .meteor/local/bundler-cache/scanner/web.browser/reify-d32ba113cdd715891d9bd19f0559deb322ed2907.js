let interpolate,projection,rotation;module.link("d3-geo",{geoInterpolate(v){interpolate=v},geoProjection(v){projection=v},geoRotation(v){rotation=v}},0);let asin,degrees,pi,sin,radians;module.link("./math.js",{asin(v){asin=v},degrees(v){degrees=v},pi(v){pi=v},sin(v){sin=v},radians(v){radians=v}},1);


// Compute the origin as the midpoint of the two reference points.
// Rotate one of the reference points by the origin.
// Apply the spherical law of sines to compute gamma rotation.
module.exportDefault(function(raw, p0, p1) {
  var i = interpolate(p0, p1),
      o = i(0.5),
      a = rotation([-o[0], -o[1]])(p0),
      b = i.distance / 2,
      y = -asin(sin(a[1] * radians) / sin(b)),
      R = [-o[0], -o[1], -(a[0] > 0 ? pi - y : y) * degrees],
      p = projection(raw(b)).rotate(R),
      r = rotation(R),
      center = p.center;

  delete p.rotate;

  p.center = function(_) {
    return arguments.length ? center(r(_)) : r.invert(center());
  };

  return p
      .clipAngle(90);
});
