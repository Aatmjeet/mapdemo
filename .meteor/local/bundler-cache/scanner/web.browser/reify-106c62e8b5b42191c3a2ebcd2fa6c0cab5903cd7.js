module.export({projections:()=>projections,geo:()=>geo,d3geo:()=>d3geo});module.link("./.internal/charts/Legend",{LegendDataItem:"LegendDataItem",Legend:"Legend",LegendSettings:"LegendSettings"},0);module.link("./.internal/charts/elements/HeatLegend",{HeatLegend:"HeatLegend"},1);module.link("./.internal/charts/types/MapChart",{MapChartDataItem:"MapChartDataItem",MapChart:"MapChart"},2);module.link("./.internal/charts/map/MapSeries",{MapSeriesDataItem:"MapSeriesDataItem",MapSeries:"MapSeries"},3);module.link("./.internal/charts/map/MapObject",{MapObject:"MapObject"},4);module.link("./.internal/charts/map/MapPolygon",{MapPolygon:"MapPolygon"},5);module.link("./.internal/charts/map/MapImage",{MapImage:"MapImage"},6);module.link("./.internal/charts/map/MapLine",{MapLine:"MapLine"},7);module.link("./.internal/charts/map/MapLineObject",{MapLineObject:"MapLineObject"},8);module.link("./.internal/charts/map/MapSpline",{MapSpline:"MapSpline"},9);module.link("./.internal/charts/map/MapArc",{MapArc:"MapArc"},10);module.link("./.internal/charts/map/Graticule",{Graticule:"Graticule"},11);module.link("./.internal/charts/map/MapPolygonSeries",{MapPolygonSeriesDataItem:"MapPolygonSeriesDataItem",MapPolygonSeries:"MapPolygonSeries"},12);module.link("./.internal/charts/map/MapLineSeries",{MapLineSeriesDataItem:"MapLineSeriesDataItem",MapLineSeries:"MapLineSeries"},13);module.link("./.internal/charts/map/MapSplineSeries",{MapSplineSeriesDataItem:"MapSplineSeriesDataItem",MapSplineSeries:"MapSplineSeries"},14);module.link("./.internal/charts/map/MapImageSeries",{MapImageSeriesDataItem:"MapImageSeriesDataItem",MapImageSeries:"MapImageSeries"},15);module.link("./.internal/charts/map/MapArcSeries",{MapArcSeriesDataItem:"MapArcSeriesDataItem",MapArcSeries:"MapArcSeries"},16);module.link("./.internal/charts/map/GraticuleSeries",{GraticuleSeriesDataItem:"GraticuleSeriesDataItem",GraticuleSeries:"GraticuleSeries"},17);module.link("./.internal/charts/map/MapUtils",{multiPolygonToGeo:"multiPolygonToGeo",multiLineToGeo:"multiLineToGeo",multiPointToGeo:"multiPointToGeo",pointToGeo:"pointToGeo",multiGeoPolygonToMultipolygon:"multiGeoPolygonToMultipolygon",getBackground:"getBackground",multiGeoLineToMultiLine:"multiGeoLineToMultiLine",multiGeoToPoint:"multiGeoToPoint",getCircle:"getCircle"},18);module.link("./.internal/charts/map/ZoomControl",{ZoomControl:"ZoomControl"},19);module.link("./.internal/charts/map/SmallMap",{SmallMap:"SmallMap"},20);module.link("./.internal/charts/map/projections/Projection",{Projection:"Projection"},21);let projections;module.link("./.internal/charts/map/projections",{"*"(v){projections=v}},22);let geo;module.link("./.internal/charts/map/Geo",{"*"(v){geo=v}},23);let d3geo;module.link("d3-geo",{"*"(v){d3geo=v}},24);/**
 * Duplicated
 */


/**
 * Maps
 */



















/**
 * Elements: projections
 */







//# sourceMappingURL=maps.js.map