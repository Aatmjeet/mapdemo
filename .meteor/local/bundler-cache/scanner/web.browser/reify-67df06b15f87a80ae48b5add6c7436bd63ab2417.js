module.export({orthographicRaw:()=>orthographicRaw});let asin,cos,epsilon,sin;module.link("../math.js",{asin(v){asin=v},cos(v){cos=v},epsilon(v){epsilon=v},sin(v){sin=v}},0);let azimuthalInvert;module.link("./azimuthal.js",{azimuthalInvert(v){azimuthalInvert=v}},1);let projection;module.link("./index.js",{default(v){projection=v}},2);



function orthographicRaw(x, y) {
  return [cos(y) * sin(x), sin(y)];
}

orthographicRaw.invert = azimuthalInvert(asin);

module.exportDefault(function() {
  return projection(orthographicRaw)
      .scale(249.5)
      .clipAngle(90 + epsilon);
});
