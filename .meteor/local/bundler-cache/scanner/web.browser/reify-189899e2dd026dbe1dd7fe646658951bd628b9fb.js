module.export({millerRaw:()=>millerRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let atan,exp,log,quarterPi,pi,tan;module.link("./math.js",{atan(v){atan=v},exp(v){exp=v},log(v){log=v},quarterPi(v){quarterPi=v},pi(v){pi=v},tan(v){tan=v}},1);


function millerRaw(lambda, phi) {
  return [lambda, 1.25 * log(tan(quarterPi + 0.4 * phi))];
}

millerRaw.invert = function(x, y) {
  return [x, 2.5 * atan(exp(0.8 * y)) - 0.625 * pi];
};

module.exportDefault(function() {
  return projection(millerRaw)
      .scale(108.318);
});
