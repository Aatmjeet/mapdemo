let length;module.link("./length.js",{default(v){length=v}},0);

var coordinates = [null, null],
    object = {type: "LineString", coordinates: coordinates};

module.exportDefault(function(a, b) {
  coordinates[0] = a;
  coordinates[1] = b;
  return length(object);
});
