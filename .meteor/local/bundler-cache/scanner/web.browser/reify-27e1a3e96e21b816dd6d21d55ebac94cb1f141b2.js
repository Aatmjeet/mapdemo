module.export({conicProjection:()=>conicProjection});let degrees,pi,radians;module.link("../math.js",{degrees(v){degrees=v},pi(v){pi=v},radians(v){radians=v}},0);let projectionMutator;module.link("./index.js",{projectionMutator(v){projectionMutator=v}},1);


function conicProjection(projectAt) {
  var phi0 = 0,
      phi1 = pi / 3,
      m = projectionMutator(projectAt),
      p = m(phi0, phi1);

  p.parallels = function(_) {
    return arguments.length ? m(phi0 = _[0] * radians, phi1 = _[1] * radians) : [phi0 * degrees, phi1 * degrees];
  };

  return p;
}
