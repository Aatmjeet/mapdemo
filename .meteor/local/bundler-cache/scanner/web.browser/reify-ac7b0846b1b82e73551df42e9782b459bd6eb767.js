module.export({eckert1Raw:()=>eckert1Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let abs,pi,sqrt;module.link("./math.js",{abs(v){abs=v},pi(v){pi=v},sqrt(v){sqrt=v}},1);


function eckert1Raw(lambda, phi) {
  var alpha = sqrt(8 / (3 * pi));
  return [
    alpha * lambda * (1 - abs(phi) / pi),
    alpha * phi
  ];
}

eckert1Raw.invert = function(x, y) {
  var alpha = sqrt(8 / (3 * pi)),
      phi = y / alpha;
  return [
    x / (alpha * (1 - abs(phi) / pi)),
    phi
  ];
};

module.exportDefault(function() {
  return projection(eckert1Raw)
      .scale(165.664);
});
