module.export({sinusoidalRaw:()=>sinusoidalRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let cos;module.link("./math.js",{cos(v){cos=v}},1);


function sinusoidalRaw(lambda, phi) {
  return [lambda * cos(phi), phi];
}

sinusoidalRaw.invert = function(x, y) {
  return [x / cos(y), y];
};

module.exportDefault(function() {
  return projection(sinusoidalRaw)
      .scale(152.63);
});
