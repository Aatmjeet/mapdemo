module.export({loximuthalRaw:()=>loximuthalRaw});let parallel1;module.link("./parallel1.js",{default(v){parallel1=v}},0);let abs,cos,epsilon,halfPi,log,quarterPi,tan;module.link("./math.js",{abs(v){abs=v},cos(v){cos=v},epsilon(v){epsilon=v},halfPi(v){halfPi=v},log(v){log=v},quarterPi(v){quarterPi=v},tan(v){tan=v}},1);


function loximuthalRaw(phi0) {
  var cosPhi0 = cos(phi0),
      tanPhi0 = tan(quarterPi + phi0 / 2);

  function forward(lambda, phi) {
    var y = phi - phi0,
        x = abs(y) < epsilon ? lambda * cosPhi0
            : abs(x = quarterPi + phi / 2) < epsilon || abs(abs(x) - halfPi) < epsilon
            ? 0 : lambda * y / log(tan(x) / tanPhi0);
    return [x, y];
  }

  forward.invert = function(x, y) {
    var lambda,
        phi = y + phi0;
    return [
      abs(y) < epsilon ? x / cosPhi0
          : (abs(lambda = quarterPi + phi / 2) < epsilon || abs(abs(lambda) - halfPi) < epsilon) ? 0
          : x * log(tan(lambda) / tanPhi0) / y,
      phi
    ];
  };

  return forward;
}

module.exportDefault(function() {
  return parallel1(loximuthalRaw)
      .parallel(40)
      .scale(158.837);
});
