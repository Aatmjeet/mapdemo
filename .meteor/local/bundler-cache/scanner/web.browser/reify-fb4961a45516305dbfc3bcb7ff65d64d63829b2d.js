module.export({twoPointAzimuthalRaw:()=>twoPointAzimuthalRaw,twoPointAzimuthalUsa:()=>twoPointAzimuthalUsa,default:()=>twoPointAzimuthal});let gnomonicRaw;module.link("d3-geo",{geoGnomonicRaw(v){gnomonicRaw=v}},0);let cos;module.link("./math.js",{cos(v){cos=v}},1);let twoPoint;module.link("./twoPoint.js",{default(v){twoPoint=v}},2);



function twoPointAzimuthalRaw(d) {
  var cosd = cos(d);

  function forward(lambda, phi) {
    var coordinates = gnomonicRaw(lambda, phi);
    coordinates[0] *= cosd;
    return coordinates;
  }

  forward.invert = function(x, y) {
    return gnomonicRaw.invert(x / cosd, y);
  };

  return forward;
}

function twoPointAzimuthalUsa() {
  return twoPointAzimuthal([-158, 21.5], [-77, 39])
      .clipAngle(60)
      .scale(400);
}

function twoPointAzimuthal(p0, p1) {
  return twoPoint(twoPointAzimuthalRaw, p0, p1);
}
