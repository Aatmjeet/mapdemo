module.export({craigRaw:()=>craigRaw});let asin,atan2,cos,sin,sqrt,tan;module.link("./math.js",{asin(v){asin=v},atan2(v){atan2=v},cos(v){cos=v},sin(v){sin=v},sqrt(v){sqrt=v},tan(v){tan=v}},0);let parallel1;module.link("./parallel1.js",{default(v){parallel1=v}},1);


function craigRaw(phi0) {
  var tanPhi0 = tan(phi0);

  function forward(lambda, phi) {
    return [lambda, (lambda ? lambda / sin(lambda) : 1) * (sin(phi) * cos(lambda) - tanPhi0 * cos(phi))];
  }

  forward.invert = tanPhi0 ? function(x, y) {
    if (x) y *= sin(x) / x;
    var cosLambda = cos(x);
    return [x, 2 * atan2(sqrt(cosLambda * cosLambda + tanPhi0 * tanPhi0 - y * y) - cosLambda, tanPhi0 - y)];
  } : function(x, y) {
    return [x, asin(x ? y * tan(x) / x : y)];
  };

  return forward;
}

module.exportDefault(function() {
  return parallel1(craigRaw)
      .scale(249.828)
      .clipAngle(90);
});
