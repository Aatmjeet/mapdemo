let identity;module.link("../identity.js",{default(v){identity=v}},0);let stream;module.link("../stream.js",{default(v){stream=v}},1);let pathArea;module.link("./area.js",{default(v){pathArea=v}},2);let pathBounds;module.link("./bounds.js",{default(v){pathBounds=v}},3);let pathCentroid;module.link("./centroid.js",{default(v){pathCentroid=v}},4);let PathContext;module.link("./context.js",{default(v){PathContext=v}},5);let pathMeasure;module.link("./measure.js",{default(v){pathMeasure=v}},6);let PathString;module.link("./string.js",{default(v){PathString=v}},7);








module.exportDefault(function(projection, context) {
  var pointRadius = 4.5,
      projectionStream,
      contextStream;

  function path(object) {
    if (object) {
      if (typeof pointRadius === "function") contextStream.pointRadius(+pointRadius.apply(this, arguments));
      stream(object, projectionStream(contextStream));
    }
    return contextStream.result();
  }

  path.area = function(object) {
    stream(object, projectionStream(pathArea));
    return pathArea.result();
  };

  path.measure = function(object) {
    stream(object, projectionStream(pathMeasure));
    return pathMeasure.result();
  };

  path.bounds = function(object) {
    stream(object, projectionStream(pathBounds));
    return pathBounds.result();
  };

  path.centroid = function(object) {
    stream(object, projectionStream(pathCentroid));
    return pathCentroid.result();
  };

  path.projection = function(_) {
    return arguments.length ? (projectionStream = _ == null ? (projection = null, identity) : (projection = _).stream, path) : projection;
  };

  path.context = function(_) {
    if (!arguments.length) return context;
    contextStream = _ == null ? (context = null, new PathString) : new PathContext(context = _);
    if (typeof pointRadius !== "function") contextStream.pointRadius(pointRadius);
    return path;
  };

  path.pointRadius = function(_) {
    if (!arguments.length) return pointRadius;
    pointRadius = typeof _ === "function" ? _ : (contextStream.pointRadius(+_), +_);
    return path;
  };

  return path.projection(projection).context(context);
});
