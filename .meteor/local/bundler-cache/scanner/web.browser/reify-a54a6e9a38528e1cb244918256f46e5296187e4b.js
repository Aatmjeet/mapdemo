module.export({sinuMollweidePhi:()=>sinuMollweidePhi,sinuMollweideY:()=>sinuMollweideY,sinuMollweideRaw:()=>sinuMollweideRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let mollweideRaw;module.link("./mollweide.js",{mollweideRaw(v){mollweideRaw=v}},1);let sinusoidalRaw;module.link("./sinusoidal.js",{sinusoidalRaw(v){sinusoidalRaw=v}},2);



var sinuMollweidePhi = 0.7109889596207567;

var sinuMollweideY = 0.0528035274542;

function sinuMollweideRaw(lambda, phi) {
  return phi > -sinuMollweidePhi
      ? (lambda = mollweideRaw(lambda, phi), lambda[1] += sinuMollweideY, lambda)
      : sinusoidalRaw(lambda, phi);
}

sinuMollweideRaw.invert = function(x, y) {
  return y > -sinuMollweidePhi
      ? mollweideRaw.invert(x, y - sinuMollweideY)
      : sinusoidalRaw.invert(x, y);
};

module.exportDefault(function() {
  return projection(sinuMollweideRaw)
      .rotate([-20, -55])
      .scale(164.263)
      .center([0, -5.4036]);
});
