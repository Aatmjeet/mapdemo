module.export({bromleyRaw:()=>bromleyRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let pi;module.link("./math.js",{pi(v){pi=v}},1);let mollweideBromleyRaw;module.link("./mollweide.js",{mollweideBromleyRaw(v){mollweideBromleyRaw=v}},2);



var bromleyRaw = mollweideBromleyRaw(1, 4 / pi, pi);

module.exportDefault(function() {
  return projection(bromleyRaw)
      .scale(152.63);
});
