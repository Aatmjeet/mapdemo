module.export({Candlestick:()=>Candlestick});let __extends;module.link("tslib",{__extends(v){__extends=v}},0);let Column;module.link("./Column",{Column(v){Column=v}},1);let Line;module.link("../../core/elements/Line",{Line(v){Line=v}},2);let registry;module.link("../../core/Registry",{registry(v){registry=v}},3);/**
 * Module that defines everything related to building Candlesticks.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */



/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Class used to creates Candlesticks.
 *
 * @see {@link ICandlestickEvents} for a list of available events
 * @see {@link ICandlestickAdapters} for a list of available Adapters
 * @todo Usage example
 * @important
 */
var Candlestick = /** @class */ (function (_super) {
    __extends(Candlestick, _super);
    /**
     * Constructor
     */
    function Candlestick() {
        var _this = _super.call(this) || this;
        _this.className = "Candlestick";
        _this.layout = "none";
        return _this;
    }
    /**
     * @ignore
     */
    Candlestick.prototype.createAssets = function () {
        _super.prototype.createAssets.call(this);
        this.lowLine = this.createChild(Line);
        this.lowLine.shouldClone = false;
        this.highLine = this.createChild(Line);
        this.highLine.shouldClone = false;
    };
    /**
     * Copies all parameters from another [[Candlestick]].
     *
     * @param source Source Candlestick
     */
    Candlestick.prototype.copyFrom = function (source) {
        _super.prototype.copyFrom.call(this, source);
        if (this.lowLine) {
            this.lowLine.copyFrom(source.lowLine);
        }
        if (this.highLine) {
            this.highLine.copyFrom(source.highLine);
        }
    };
    return Candlestick;
}(Column));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["Candlestick"] = Candlestick;
//# sourceMappingURL=Candlestick.js.map