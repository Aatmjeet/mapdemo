module.export({vanDerGrinten2Raw:()=>vanDerGrinten2Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let abs,asin,atan,atan2,cos,halfPi,epsilon,pi,sign,sin,sqrt,tan;module.link("./math.js",{abs(v){abs=v},asin(v){asin=v},atan(v){atan=v},atan2(v){atan2=v},cos(v){cos=v},halfPi(v){halfPi=v},epsilon(v){epsilon=v},pi(v){pi=v},sign(v){sign=v},sin(v){sin=v},sqrt(v){sqrt=v},tan(v){tan=v}},1);


function vanDerGrinten2Raw(lambda, phi) {
  if (abs(phi) < epsilon) return [lambda, 0];
  var sinTheta = abs(phi / halfPi),
      theta = asin(sinTheta);
  if (abs(lambda) < epsilon || abs(abs(phi) - halfPi) < epsilon) return [0, sign(phi) * pi * tan(theta / 2)];
  var cosTheta = cos(theta),
      A = abs(pi / lambda - lambda / pi) / 2,
      A2 = A * A,
      x1 = cosTheta * (sqrt(1 + A2) - A * cosTheta) / (1 + A2 * sinTheta * sinTheta);
  return [
    sign(lambda) * pi * x1,
    sign(phi) * pi * sqrt(1 - x1 * (2 * A + x1))
  ];
}

vanDerGrinten2Raw.invert = function(x, y) {
  if (!x) return [0, halfPi * sin(2 * atan(y / pi))];
  var x1 = abs(x / pi),
      A = (1 - x1 * x1 - (y /= pi) * y) / (2 * x1),
      A2 = A * A,
      B = sqrt(A2 + 1);
  return [
    sign(x) * pi * (B - A),
    sign(y) * halfPi * sin(2 * atan2(sqrt((1 - 2 * A * x1) * (A + B) - x1), sqrt(B + A + x1)))
  ];
};

module.exportDefault(function() {
  return projection(vanDerGrinten2Raw)
      .scale(79.4183);
});
