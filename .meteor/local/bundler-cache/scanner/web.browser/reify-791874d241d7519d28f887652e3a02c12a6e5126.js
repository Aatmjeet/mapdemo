module.export({eckert4Raw:()=>eckert4Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let abs,asin,cos,epsilon,halfPi,pi,sin,sqrt;module.link("./math.js",{abs(v){abs=v},asin(v){asin=v},cos(v){cos=v},epsilon(v){epsilon=v},halfPi(v){halfPi=v},pi(v){pi=v},sin(v){sin=v},sqrt(v){sqrt=v}},1);


function eckert4Raw(lambda, phi) {
  var k = (2 + halfPi) * sin(phi);
  phi /= 2;
  for (var i = 0, delta = Infinity; i < 10 && abs(delta) > epsilon; i++) {
    var cosPhi = cos(phi);
    phi -= delta = (phi + sin(phi) * (cosPhi + 2) - k) / (2 * cosPhi * (1 + cosPhi));
  }
  return [
    2 / sqrt(pi * (4 + pi)) * lambda * (1 + cos(phi)),
    2 * sqrt(pi / (4 + pi)) * sin(phi)
  ];
}

eckert4Raw.invert = function(x, y) {
  var A = y * sqrt((4 + pi) / pi) / 2,
      k = asin(A),
      c = cos(k);
  return [
    x / (2 / sqrt(pi * (4 + pi)) * (1 + c)),
    asin((k + A * (c + 2)) / (2 + halfPi))
  ];
};

module.exportDefault(function() {
  return projection(eckert4Raw)
      .scale(180.739);
});
