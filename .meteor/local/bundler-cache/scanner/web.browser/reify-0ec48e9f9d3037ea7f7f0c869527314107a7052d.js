module.export({collignonRaw:()=>collignonRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let asin,pi,sin,sqrt,sqrtPi;module.link("./math.js",{asin(v){asin=v},pi(v){pi=v},sin(v){sin=v},sqrt(v){sqrt=v},sqrtPi(v){sqrtPi=v}},1);


function collignonRaw(lambda, phi) {
  var alpha = sqrt(1 - sin(phi));
  return [(2 / sqrtPi) * lambda * alpha, sqrtPi * (1 - alpha)];
}

collignonRaw.invert = function(x, y) {
  var lambda = (lambda = y / sqrtPi - 1) * lambda;
  return [lambda > 0 ? x * sqrt(pi / lambda) / 2 : 0, asin(1 - lambda)];
};

module.exportDefault(function() {
  return projection(collignonRaw)
      .scale(95.6464)
      .center([0, 30]);
});
