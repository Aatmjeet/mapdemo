module.export({ginzburg9Raw:()=>ginzburg9Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let ginzburgPolyconicRaw;module.link("./ginzburgPolyconic.js",{default(v){ginzburgPolyconicRaw=v}},1);


var ginzburg9Raw = ginzburgPolyconicRaw(2.6516, -0.76534, 0.19123, -0.047094, 1.36289, -0.13965,0.031762);

module.exportDefault(function() {
  return projection(ginzburg9Raw)
      .scale(131.087);
});
