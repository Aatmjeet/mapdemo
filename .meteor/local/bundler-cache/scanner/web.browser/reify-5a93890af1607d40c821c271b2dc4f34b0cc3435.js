let homolosineRaw;module.link("../homolosine.js",{homolosineRaw(v){homolosineRaw=v}},0);let interrupt;module.link("./index.js",{default(v){interrupt=v}},1);


var lobes = [[ // northern hemisphere
  [[-180,   0], [-100,  90], [ -40,   0]],
  [[ -40,   0], [  30,  90], [ 180,   0]]
], [ // southern hemisphere
  [[-180,   0], [-160, -90], [-100,   0]],
  [[-100,   0], [ -60, -90], [ -20,   0]],
  [[ -20,   0], [  20, -90], [  80,   0]],
  [[  80,   0], [ 140, -90], [ 180,   0]]
]];

module.exportDefault(function() {
  return interrupt(homolosineRaw, lobes)
      .scale(152.63);
});
