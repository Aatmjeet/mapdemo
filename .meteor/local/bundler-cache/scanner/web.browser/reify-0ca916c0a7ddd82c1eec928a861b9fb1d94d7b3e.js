let conicEqualArea;module.link("./conicEqualArea.js",{default(v){conicEqualArea=v}},0);

module.exportDefault(function() {
  return conicEqualArea()
      .parallels([29.5, 45.5])
      .scale(1070)
      .translate([480, 250])
      .rotate([96, 0])
      .center([-0.6, 38.7]);
});
