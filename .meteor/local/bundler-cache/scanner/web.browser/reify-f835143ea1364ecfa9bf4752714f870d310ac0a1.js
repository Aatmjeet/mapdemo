let centroid,gnomonic;module.link("d3-geo",{geoCentroid(v){centroid=v},geoGnomonic(v){gnomonic=v}},0);let pi;module.link("../math.js",{pi(v){pi=v}},1);let polyhedral;module.link("./index.js",{default(v){polyhedral=v}},2);let octahedron;module.link("./octahedron.js",{default(v){octahedron=v}},3);




module.exportDefault(function(faceProjection) {

  faceProjection = faceProjection || function(face) {
    var c = centroid({type: "MultiPoint", coordinates: face});
    return gnomonic().scale(1).translate([0, 0]).rotate([-c[0], -c[1]]);
  };

  var faces = octahedron.map(function(face) {
    return {face: face, project: faceProjection(face)};
  });

  [-1, 0, 0, 1, 0, 1, 4, 5].forEach(function(d, i) {
    var node = faces[d];
    node && (node.children || (node.children = [])).push(faces[i]);
  });

  return polyhedral(faces[0], function(lambda, phi) {
        return faces[lambda < -pi / 2 ? phi < 0 ? 6 : 4
            : lambda < 0 ? phi < 0 ? 2 : 0
            : lambda < pi / 2 ? phi < 0 ? 3 : 1
            : phi < 0 ? 7 : 5];
      })
      .angle(-30)
      .scale(101.858)
      .center([0, 45]);
});
