module.export({conicEquidistantRaw:()=>conicEquidistantRaw});let abs,atan2,cos,epsilon,pi,sign,sin,sqrt;module.link("../math.js",{abs(v){abs=v},atan2(v){atan2=v},cos(v){cos=v},epsilon(v){epsilon=v},pi(v){pi=v},sign(v){sign=v},sin(v){sin=v},sqrt(v){sqrt=v}},0);let conicProjection;module.link("./conic.js",{conicProjection(v){conicProjection=v}},1);let equirectangularRaw;module.link("./equirectangular.js",{equirectangularRaw(v){equirectangularRaw=v}},2);



function conicEquidistantRaw(y0, y1) {
  var cy0 = cos(y0),
      n = y0 === y1 ? sin(y0) : (cy0 - cos(y1)) / (y1 - y0),
      g = cy0 / n + y0;

  if (abs(n) < epsilon) return equirectangularRaw;

  function project(x, y) {
    var gy = g - y, nx = n * x;
    return [gy * sin(nx), g - gy * cos(nx)];
  }

  project.invert = function(x, y) {
    var gy = g - y,
        l = atan2(x, abs(gy)) * sign(gy);
    if (gy * n < 0)
      l -= pi * sign(x) * sign(gy);
    return [l / n, g - sign(n) * sqrt(x * x + gy * gy)];
  };

  return project;
}

module.exportDefault(function() {
  return conicProjection(conicEquidistantRaw)
      .scale(131.154)
      .center([0, 13.9389]);
});
