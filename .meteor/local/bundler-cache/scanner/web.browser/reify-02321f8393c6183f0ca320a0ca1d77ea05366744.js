module.export({MapSpline:()=>MapSpline});let __extends;module.link("tslib",{__extends(v){__extends=v}},0);let MapLine;module.link("./MapLine",{MapLine(v){MapLine=v}},1);let Polyspline;module.link("../../core/elements/Polyspline",{Polyspline(v){Polyspline=v}},2);let registry;module.link("../../core/Registry",{registry(v){registry=v}},3);/**
 * Map spline module
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */



/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Used to draw a spline on the map.
 *
 * @see {@link IMapSplineEvents} for a list of available events
 * @see {@link IMapSplineAdapters} for a list of available Adapters
 */
var MapSpline = /** @class */ (function (_super) {
    __extends(MapSpline, _super);
    /**
     * Constructor
     */
    function MapSpline() {
        var _this = 
        // Init
        _super.call(this) || this;
        _this.className = "MapSpline";
        // Apply theme
        _this.applyTheme();
        return _this;
    }
    /**
     * @ignore
     */
    MapSpline.prototype.createLine = function () {
        this.line = new Polyspline();
        this.line.tensionX = 0.8;
        this.line.tensionY = 0.8;
    };
    Object.defineProperty(MapSpline.prototype, "shortestDistance", {
        /**
         * ShortestDistance = true is not supported by MapSpline, only MapLine does support it
         * @default false
         * @param value
         * @todo: review description
         */
        get: function () {
            return false;
        },
        set: function (value) {
        },
        enumerable: true,
        configurable: true
    });
    return MapSpline;
}(MapLine));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["MapSpline"] = MapSpline;
//# sourceMappingURL=MapSpline.js.map