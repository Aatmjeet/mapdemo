module.export({conicEqualAreaRaw:()=>conicEqualAreaRaw});let abs,asin,atan2,cos,epsilon,pi,sign,sin,sqrt;module.link("../math.js",{abs(v){abs=v},asin(v){asin=v},atan2(v){atan2=v},cos(v){cos=v},epsilon(v){epsilon=v},pi(v){pi=v},sign(v){sign=v},sin(v){sin=v},sqrt(v){sqrt=v}},0);let conicProjection;module.link("./conic.js",{conicProjection(v){conicProjection=v}},1);let cylindricalEqualAreaRaw;module.link("./cylindricalEqualArea.js",{cylindricalEqualAreaRaw(v){cylindricalEqualAreaRaw=v}},2);



function conicEqualAreaRaw(y0, y1) {
  var sy0 = sin(y0), n = (sy0 + sin(y1)) / 2;

  // Are the parallels symmetrical around the Equator?
  if (abs(n) < epsilon) return cylindricalEqualAreaRaw(y0);

  var c = 1 + sy0 * (2 * n - sy0), r0 = sqrt(c) / n;

  function project(x, y) {
    var r = sqrt(c - 2 * n * sin(y)) / n;
    return [r * sin(x *= n), r0 - r * cos(x)];
  }

  project.invert = function(x, y) {
    var r0y = r0 - y,
        l = atan2(x, abs(r0y)) * sign(r0y);
    if (r0y * n < 0)
      l -= pi * sign(x) * sign(r0y);
    return [l / n, asin((c - (x * x + r0y * r0y) * n * n) / (2 * n))];
  };

  return project;
}

module.exportDefault(function() {
  return conicProjection(conicEqualAreaRaw)
      .scale(155.424)
      .center([0, 33.6442]);
});
