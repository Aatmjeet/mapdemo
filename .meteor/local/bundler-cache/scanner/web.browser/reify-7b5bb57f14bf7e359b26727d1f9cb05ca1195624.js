module.export({ginzburg5Raw:()=>ginzburg5Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let ginzburgPolyconicRaw;module.link("./ginzburgPolyconic.js",{default(v){ginzburgPolyconicRaw=v}},1);


var ginzburg5Raw = ginzburgPolyconicRaw(2.583819, -0.835827, 0.170354, -0.038094, 1.543313, -0.411435,0.082742);

module.exportDefault(function() {
  return projection(ginzburg5Raw)
      .scale(153.93);
});
