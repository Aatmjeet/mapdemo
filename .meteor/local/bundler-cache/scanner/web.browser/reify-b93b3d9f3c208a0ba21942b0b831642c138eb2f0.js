module.export({ConeColumn:()=>ConeColumn});let __extends;module.link("tslib",{__extends(v){__extends=v}},0);let Column;module.link("./Column",{Column(v){Column=v}},1);let Cone;module.link("../../core/elements/3d/Cone",{Cone(v){Cone=v}},2);let registry;module.link("../../core/Registry",{registry(v){registry=v}},3);/**
 * Module that defines everything related to building Cone Columns.
 * It is a container which has coneColumn element which is a Cone.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */



/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Class used to creates ConeColumns.
 *
 * @see {@link IConeColumnEvents} for a list of available events
 * @see {@link IConeColumnAdapters} for a list of available Adapters
 * @todo Usage example
 * @important
 */
var ConeColumn = /** @class */ (function (_super) {
    __extends(ConeColumn, _super);
    /**
     * Constructor
     */
    function ConeColumn() {
        var _this = _super.call(this) || this;
        _this.className = "ConeColumn";
        return _this;
    }
    /**
     * @ignore
     */
    ConeColumn.prototype.createAssets = function () {
        this.coneColumn = this.createChild(Cone);
        this.coneColumn.shouldClone = false;
        // some dirty hack so that if user access column, it won't get error
        this.column = this.coneColumn;
    };
    /**
     * Copies all parameters from another [[ConeColumn]].
     *
     * @param source Source ConeColumn
     */
    ConeColumn.prototype.copyFrom = function (source) {
        _super.prototype.copyFrom.call(this, source);
        if (this.coneColumn) {
            this.coneColumn.copyFrom(source.coneColumn);
        }
    };
    return ConeColumn;
}(Column));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["ConeColumn"] = ConeColumn;
//# sourceMappingURL=ConeColumn.js.map