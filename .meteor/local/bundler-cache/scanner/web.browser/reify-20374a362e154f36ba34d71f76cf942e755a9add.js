module.export({eckert6Raw:()=>eckert6Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let abs,asin,cos,epsilon,halfPi,pi,sin,sqrt;module.link("./math.js",{abs(v){abs=v},asin(v){asin=v},cos(v){cos=v},epsilon(v){epsilon=v},halfPi(v){halfPi=v},pi(v){pi=v},sin(v){sin=v},sqrt(v){sqrt=v}},1);


function eckert6Raw(lambda, phi) {
  var k = (1 + halfPi) * sin(phi);
  for (var i = 0, delta = Infinity; i < 10 && abs(delta) > epsilon; i++) {
    phi -= delta = (phi + sin(phi) - k) / (1 + cos(phi));
  }
  k = sqrt(2 + pi);
  return [
    lambda * (1 + cos(phi)) / k,
    2 * phi / k
  ];
}

eckert6Raw.invert = function(x, y) {
  var j = 1 + halfPi,
      k = sqrt(j / 2);
  return [
    x * 2 * k / (1 + cos(y *= k)),
    asin((y + sin(y)) / j)
  ];
};

module.exportDefault(function() {
  return projection(eckert6Raw)
      .scale(173.044);
});
