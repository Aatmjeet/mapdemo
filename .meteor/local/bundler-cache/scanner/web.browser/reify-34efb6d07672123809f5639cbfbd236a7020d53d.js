module.export({faheyRaw:()=>faheyRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let atan,cos,radians,sqrt,tan;module.link("./math.js",{atan(v){atan=v},cos(v){cos=v},radians(v){radians=v},sqrt(v){sqrt=v},tan(v){tan=v}},1);


var faheyK = cos(35 * radians);

function faheyRaw(lambda, phi) {
  var t = tan(phi / 2);
  return [lambda * faheyK * sqrt(1 - t * t), (1 + faheyK) * t];
}

faheyRaw.invert = function(x, y) {
  var t = y / (1 + faheyK);
  return [x && x / (faheyK * sqrt(1 - t * t)), 2 * atan(t)];
};

module.exportDefault(function() {
  return projection(faheyRaw)
      .scale(137.152);
});
