module.export({hammerRaw:()=>hammerRaw});let azimuthalEqualAreaRaw,projectionMutator;module.link("d3-geo",{geoAzimuthalEqualAreaRaw(v){azimuthalEqualAreaRaw=v},geoProjectionMutator(v){projectionMutator=v}},0);let asin,cos,sin;module.link("./math.js",{asin(v){asin=v},cos(v){cos=v},sin(v){sin=v}},1);


function hammerRaw(A, B) {
  if (arguments.length < 2) B = A;
  if (B === 1) return azimuthalEqualAreaRaw;
  if (B === Infinity) return hammerQuarticAuthalicRaw;

  function forward(lambda, phi) {
    var coordinates = azimuthalEqualAreaRaw(lambda / B, phi);
    coordinates[0] *= A;
    return coordinates;
  }

  forward.invert = function(x, y) {
    var coordinates = azimuthalEqualAreaRaw.invert(x / A, y);
    coordinates[0] *= B;
    return coordinates;
  };

  return forward;
}

function hammerQuarticAuthalicRaw(lambda, phi) {
  return [
    lambda * cos(phi) / cos(phi /= 2),
    2 * sin(phi)
  ];
}

hammerQuarticAuthalicRaw.invert = function(x, y) {
  var phi = 2 * asin(y / 2);
  return [
    x * cos(phi / 2) / cos(phi),
    phi
  ];
};

module.exportDefault(function() {
  var B = 2,
      m = projectionMutator(hammerRaw),
      p = m(B);

  p.coefficient = function(_) {
    if (!arguments.length) return B;
    return m(B = +_);
  };

  return p
    .scale(169.529);
});
