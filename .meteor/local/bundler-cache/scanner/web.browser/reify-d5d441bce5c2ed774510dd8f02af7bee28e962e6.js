module.export({mollweideBromleyTheta:()=>mollweideBromleyTheta,mollweideBromleyRaw:()=>mollweideBromleyRaw,mollweideRaw:()=>mollweideRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let abs,asin,cos,epsilon,halfPi,pi,sin,sqrt2;module.link("./math.js",{abs(v){abs=v},asin(v){asin=v},cos(v){cos=v},epsilon(v){epsilon=v},halfPi(v){halfPi=v},pi(v){pi=v},sin(v){sin=v},sqrt2(v){sqrt2=v}},1);


function mollweideBromleyTheta(cp, phi) {
  var cpsinPhi = cp * sin(phi), i = 30, delta;
  do phi -= delta = (phi + sin(phi) - cpsinPhi) / (1 + cos(phi));
  while (abs(delta) > epsilon && --i > 0);
  return phi / 2;
}

function mollweideBromleyRaw(cx, cy, cp) {

  function forward(lambda, phi) {
    return [cx * lambda * cos(phi = mollweideBromleyTheta(cp, phi)), cy * sin(phi)];
  }

  forward.invert = function(x, y) {
    return y = asin(y / cy), [x / (cx * cos(y)), asin((2 * y + sin(2 * y)) / cp)];
  };

  return forward;
}

var mollweideRaw = mollweideBromleyRaw(sqrt2 / halfPi, sqrt2, pi);

module.exportDefault(function() {
  return projection(mollweideRaw)
      .scale(169.529);
});
