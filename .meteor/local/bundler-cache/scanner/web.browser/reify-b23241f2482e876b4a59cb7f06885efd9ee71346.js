module.export({guyouRaw:()=>guyouRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let ellipticF,ellipticFi,ellipticJi;module.link("./elliptic.js",{ellipticF(v){ellipticF=v},ellipticFi(v){ellipticFi=v},ellipticJi(v){ellipticJi=v}},1);let abs,atan,atan2,cos,exp,halfPi,log,pi,sin,sqrt,sqrt2,tan;module.link("./math.js",{abs(v){abs=v},atan(v){atan=v},atan2(v){atan2=v},cos(v){cos=v},exp(v){exp=v},halfPi(v){halfPi=v},log(v){log=v},pi(v){pi=v},sin(v){sin=v},sqrt(v){sqrt=v},sqrt2(v){sqrt2=v},tan(v){tan=v}},2);let squareRaw;module.link("./square.js",{default(v){squareRaw=v}},3);




function guyouRaw(lambda, phi) {
  var k_ = (sqrt2 - 1) / (sqrt2 + 1),
      k = sqrt(1 - k_ * k_),
      K = ellipticF(halfPi, k * k),
      f = -1,
      psi = log(tan(pi / 4 + abs(phi) / 2)),
      r = exp(f * psi) / sqrt(k_),
      at = guyouComplexAtan(r * cos(f * lambda), r * sin(f * lambda)),
      t = ellipticFi(at[0], at[1], k * k);
  return [-t[1], (phi >= 0 ? 1 : -1) * (0.5 * K - t[0])];
}

function guyouComplexAtan(x, y) {
  var x2 = x * x,
      y_1 = y + 1,
      t = 1 - x2 - y * y;
  return [
   0.5 * ((x >= 0 ? halfPi : -halfPi) - atan2(t, 2 * x)),
    -0.25 * log(t * t + 4 * x2) +0.5 * log(y_1 * y_1 + x2)
  ];
}

function guyouComplexDivide(a, b) {
  var denominator = b[0] * b[0] + b[1] * b[1];
  return [
    (a[0] * b[0] + a[1] * b[1]) / denominator,
    (a[1] * b[0] - a[0] * b[1]) / denominator
  ];
}

guyouRaw.invert = function(x, y) {
  var k_ = (sqrt2 - 1) / (sqrt2 + 1),
      k = sqrt(1 - k_ * k_),
      K = ellipticF(halfPi, k * k),
      f = -1,
      j = ellipticJi(0.5 * K - y, -x, k * k),
      tn = guyouComplexDivide(j[0], j[1]),
      lambda = atan2(tn[1], tn[0]) / f;
  return [
    lambda,
    2 * atan(exp(0.5 / f * log(k_ * tn[0] * tn[0] + k_ * tn[1] * tn[1]))) - halfPi
  ];
};

module.exportDefault(function() {
  return projection(squareRaw(guyouRaw))
      .scale(151.496);
});
