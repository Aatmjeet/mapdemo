module.export({stereographicRaw:()=>stereographicRaw});let atan,cos,sin;module.link("../math.js",{atan(v){atan=v},cos(v){cos=v},sin(v){sin=v}},0);let azimuthalInvert;module.link("./azimuthal.js",{azimuthalInvert(v){azimuthalInvert=v}},1);let projection;module.link("./index.js",{default(v){projection=v}},2);



function stereographicRaw(x, y) {
  var cy = cos(y), k = 1 + cos(x) * cy;
  return [cy * sin(x) / k, sin(y) / k];
}

stereographicRaw.invert = azimuthalInvert(function(z) {
  return 2 * atan(z);
});

module.exportDefault(function() {
  return projection(stereographicRaw)
      .scale(250)
      .clipAngle(142);
});
