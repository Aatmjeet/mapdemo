module.export({cylindricalStereographicRaw:()=>cylindricalStereographicRaw});let atan,cos,tan;module.link("./math.js",{atan(v){atan=v},cos(v){cos=v},tan(v){tan=v}},0);let parallel1;module.link("./parallel1.js",{default(v){parallel1=v}},1);


function cylindricalStereographicRaw(phi0) {
  var cosPhi0 = cos(phi0);

  function forward(lambda, phi) {
    return [lambda * cosPhi0, (1 + cosPhi0) * tan(phi / 2)];
  }

  forward.invert = function(x, y) {
    return [x / cosPhi0, atan(y / (1 + cosPhi0)) * 2];
  };

  return forward;
}

module.exportDefault(function() {
  return parallel1(cylindricalStereographicRaw)
      .scale(124.75);
});
