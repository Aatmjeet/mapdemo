module.export({ginzburg4Raw:()=>ginzburg4Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let ginzburgPolyconicRaw;module.link("./ginzburgPolyconic.js",{default(v){ginzburgPolyconicRaw=v}},1);


var ginzburg4Raw = ginzburgPolyconicRaw(2.8284, -1.6988, 0.75432, -0.18071, 1.76003, -0.38914, 0.042555);

module.exportDefault(function() {
  return projection(ginzburg4Raw)
      .scale(149.995);
});
