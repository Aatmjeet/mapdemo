module.export({CirclePattern:()=>CirclePattern});let __extends;module.link("tslib",{__extends(v){__extends=v}},0);let Pattern;module.link("./Pattern",{Pattern(v){Pattern=v}},1);let registry;module.link("../../Registry",{registry(v){registry=v}},2);/**
 * Rectangular pattern module.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */


;
/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Circular pattern
 */
var CirclePattern = /** @class */ (function (_super) {
    __extends(CirclePattern, _super);
    /**
     * Constructor
     */
    function CirclePattern() {
        var _this = _super.call(this) || this;
        _this.properties["radius"] = 2;
        _this._circle = _this.paper.add("circle");
        _this.addElement(_this._circle);
        _this.shapeRendering = "auto";
        return _this;
    }
    /**
     * Draws the circle element.
     */
    CirclePattern.prototype.draw = function () {
        _super.prototype.draw.call(this);
        if (this._circle) {
            this._circle.attr({ "r": this.radius, "cx": this.width / 2, "cy": this.height / 2 });
        }
    };
    Object.defineProperty(CirclePattern.prototype, "radius", {
        /**
         * @return Radius (px)
         */
        get: function () {
            return this.properties["radius"];
        },
        /**
         * Circle radius in pixels.
         *
         * @param value Radius (px)
         */
        set: function (value) {
            this.properties["radius"] = value;
            this.draw();
        },
        enumerable: true,
        configurable: true
    });
    return CirclePattern;
}(Pattern));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["CirclePattern"] = CirclePattern;
//# sourceMappingURL=CirclePattern.js.map