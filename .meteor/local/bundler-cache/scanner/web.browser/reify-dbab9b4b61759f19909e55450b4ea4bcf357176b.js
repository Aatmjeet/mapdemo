module.export({nellHammerRaw:()=>nellHammerRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let abs,cos,epsilon,tan;module.link("./math.js",{abs(v){abs=v},cos(v){cos=v},epsilon(v){epsilon=v},tan(v){tan=v}},1);


function nellHammerRaw(lambda, phi) {
  return [
    lambda * (1 + cos(phi)) / 2,
    2 * (phi - tan(phi / 2))
  ];
}

nellHammerRaw.invert = function(x, y) {
  var p = y / 2;
  for (var i = 0, delta = Infinity; i < 10 && abs(delta) > epsilon; ++i) {
    var c = cos(y / 2);
    y -= delta = (y - tan(y / 2) - p) / (1 - 0.5 / (c * c));
  }
  return [
    2 * x / (1 + cos(y)),
    y
  ];
};

module.exportDefault(function() {
  return projection(nellHammerRaw)
      .scale(152.63);
});
