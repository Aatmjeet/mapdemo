module.export({polyconicRaw:()=>polyconicRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let abs,acos,asin,cos,epsilon,halfPi,sign,sin,tan;module.link("./math.js",{abs(v){abs=v},acos(v){acos=v},asin(v){asin=v},cos(v){cos=v},epsilon(v){epsilon=v},halfPi(v){halfPi=v},sign(v){sign=v},sin(v){sin=v},tan(v){tan=v}},1);


function polyconicRaw(lambda, phi) {
  if (abs(phi) < epsilon) return [lambda, 0];
  var tanPhi = tan(phi),
      k = lambda * sin(phi);
  return [
    sin(k) / tanPhi,
    phi + (1 - cos(k)) / tanPhi
  ];
}

polyconicRaw.invert = function(x, y) {
  if (abs(y) < epsilon) return [x, 0];
  var k = x * x + y * y,
      phi = y * 0.5,
      i = 10, delta;
  do {
    var tanPhi = tan(phi),
        secPhi = 1 / cos(phi),
        j = k - 2 * y * phi + phi * phi;
    phi -= delta = (tanPhi * j + 2 * (phi - y)) / (2 + j * secPhi * secPhi + 2 * (phi - y) * tanPhi);
  } while (abs(delta) > epsilon && --i > 0);
  tanPhi = tan(phi);
  return [
    (abs(y) < abs(phi + 1 / tanPhi) ? asin(x * tanPhi) : sign(y) * sign(x) * (acos(abs(x * tanPhi)) + halfPi)) / sin(phi),
    phi
  ];
};

module.exportDefault(function() {
  return projection(polyconicRaw)
      .scale(103.74);
});
