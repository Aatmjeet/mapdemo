module.export({vanDerGrinten3Raw:()=>vanDerGrinten3Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let abs,asin,atan,cos,epsilon,halfPi,pi,sign,sin,sqrt,tan;module.link("./math.js",{abs(v){abs=v},asin(v){asin=v},atan(v){atan=v},cos(v){cos=v},epsilon(v){epsilon=v},halfPi(v){halfPi=v},pi(v){pi=v},sign(v){sign=v},sin(v){sin=v},sqrt(v){sqrt=v},tan(v){tan=v}},1);


function vanDerGrinten3Raw(lambda, phi) {
  if (abs(phi) < epsilon) return [lambda, 0];
  var sinTheta = phi / halfPi,
      theta = asin(sinTheta);
  if (abs(lambda) < epsilon || abs(abs(phi) - halfPi) < epsilon) return [0, pi * tan(theta / 2)];
  var A = (pi / lambda - lambda / pi) / 2,
      y1 = sinTheta / (1 + cos(theta));
  return [
    pi * (sign(lambda) * sqrt(A * A + 1 - y1 * y1) - A),
    pi * y1
  ];
}

vanDerGrinten3Raw.invert = function(x, y) {
  if (!y) return [x, 0];
  var y1 = y / pi,
      A = (pi * pi * (1 - y1 * y1) - x * x) / (2 * pi * x);
  return [
    x ? pi * (sign(x) * sqrt(A * A + 1) - A) : 0,
    halfPi * sin(2 * atan(y1))
  ];
};

module.exportDefault(function() {
  return projection(vanDerGrinten3Raw)
        .scale(79.4183);
});
