module.export({Line:()=>Line});let __extends;module.link("tslib",{__extends(v){__extends=v}},0);let Sprite;module.link("../Sprite",{Sprite(v){Sprite=v}},1);let color;module.link("../utils/Color",{color(v){color=v}},2);let LinearGradient;module.link("../rendering/fills/LinearGradient",{LinearGradient(v){LinearGradient=v}},3);let registry;module.link("../Registry",{registry(v){registry=v}},4);let $type;module.link("../utils/Type",{"*"(v){$type=v}},5);let $math;module.link("../utils/Math",{"*"(v){$math=v}},6);/**
 * Line drawing functionality.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */






/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Draws a line.
 *
 * @see {@link ILineEvents} for a list of available events
 * @see {@link ILineAdapters} for a list of available Adapters
 */
var Line = /** @class */ (function (_super) {
    __extends(Line, _super);
    /**
     * Constructor
     */
    function Line() {
        var _this = _super.call(this) || this;
        _this.className = "Line";
        _this.element = _this.paper.add("line");
        _this.fill = color(); //"none";
        _this.x1 = 0;
        _this.y1 = 0;
        _this.applyTheme();
        return _this;
    }
    /**
     * Draws the line.
     *
     * @ignore Exclude from docs
     */
    Line.prototype.draw = function () {
        _super.prototype.draw.call(this);
        if (this.x1 == this.x2 || this.y1 == this.y2) {
            this.pixelPerfect = true;
        }
        else {
            this.pixelPerfect = false;
        }
        this.x1 = this.x1;
        this.x2 = this.x2;
        this.y1 = this.y1;
        this.y2 = this.y2;
    };
    Object.defineProperty(Line.prototype, "x1", {
        /**
         * @return X
         */
        get: function () {
            return this.getPropertyValue("x1");
        },
        /**
         * X coordinate of first end.
         *
         * @param value X
         */
        set: function (value) {
            if (!$type.isNumber(value)) {
                value = 0;
            }
            var delta = 0;
            if (this.pixelPerfect && this.stroke instanceof LinearGradient) {
                delta = 0.00001;
            }
            this.setPropertyValue("x1", value, true);
            this.element.attr({ "x1": value + delta });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Line.prototype, "x2", {
        /**
         * @return X
         */
        get: function () {
            var value = this.getPropertyValue("x2");
            if (!$type.isNumber(value)) {
                value = this.pixelWidth;
            }
            return value;
        },
        /**
         * X coordinate of second end.
         *
         * @param value X
         */
        set: function (value) {
            if (!$type.isNumber(value)) {
                value = 0;
            }
            this.setPropertyValue("x2", value, true);
            this.element.attr({ "x2": value });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Line.prototype, "y1", {
        /**
         * @return Y
         */
        get: function () {
            return this.getPropertyValue("y1");
        },
        /**
         * Y coordinate of first end.
         *
         * @param value Y
         */
        set: function (value) {
            if (!$type.isNumber(value)) {
                value = 0;
            }
            var delta = 0;
            if (this.pixelPerfect && this.stroke instanceof LinearGradient) {
                delta = 0.00001;
            }
            this.setPropertyValue("y1", value, true);
            this.element.attr({ "y1": value + delta });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Line.prototype, "y2", {
        /**
         * @return Y
         */
        get: function () {
            var value = this.getPropertyValue("y2");
            if (!$type.isNumber(value)) {
                value = this.pixelHeight;
            }
            return value;
        },
        /**
         * Y coordinate of second end.
         *
         * @param value Y
         */
        set: function (value) {
            if (!$type.isNumber(value)) {
                value = 0;
            }
            this.setPropertyValue("y2", value, true);
            this.element.attr({ "y2": value });
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Converts relative position along the line (0-1) into pixel coordinates.
     *
     * @param position  Position (0-1)
     * @return Coordinates
     */
    Line.prototype.positionToPoint = function (position) {
        var point1 = { x: this.x1, y: this.y1 };
        var point2 = { x: this.x2, y: this.y2 };
        var point = $math.getMidPoint(point1, point2, position);
        var angle = $math.getAngle(point1, point2);
        return { x: point.x, y: point.y, angle: angle };
    };
    return Line;
}(Sprite));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["Line"] = Line;
//# sourceMappingURL=Line.js.map