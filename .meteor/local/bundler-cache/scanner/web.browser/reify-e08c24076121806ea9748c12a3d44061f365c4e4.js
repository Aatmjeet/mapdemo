module.export({kavrayskiy7Raw:()=>kavrayskiy7Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let pi,sqrt,tau;module.link("./math.js",{pi(v){pi=v},sqrt(v){sqrt=v},tau(v){tau=v}},1);


function kavrayskiy7Raw(lambda, phi) {
  return [3 / tau * lambda * sqrt(pi * pi / 3 - phi * phi), phi];
}

kavrayskiy7Raw.invert = function(x, y) {
  return [tau / 3 * x / sqrt(pi * pi / 3 - y * y), y];
};

module.exportDefault(function() {
  return projection(kavrayskiy7Raw)
      .scale(158.837);
});
