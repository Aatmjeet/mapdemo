module.export({epsilon:()=>epsilon,epsilon2:()=>epsilon2,pi:()=>pi,halfPi:()=>halfPi,quarterPi:()=>quarterPi,tau:()=>tau,degrees:()=>degrees,radians:()=>radians,abs:()=>abs,atan:()=>atan,atan2:()=>atan2,cos:()=>cos,ceil:()=>ceil,exp:()=>exp,floor:()=>floor,hypot:()=>hypot,log:()=>log,pow:()=>pow,sin:()=>sin,sign:()=>sign,sqrt:()=>sqrt,tan:()=>tan,acos:()=>acos,asin:()=>asin,haversin:()=>haversin});var epsilon = 1e-6;
var epsilon2 = 1e-12;
var pi = Math.PI;
var halfPi = pi / 2;
var quarterPi = pi / 4;
var tau = pi * 2;

var degrees = 180 / pi;
var radians = pi / 180;

var abs = Math.abs;
var atan = Math.atan;
var atan2 = Math.atan2;
var cos = Math.cos;
var ceil = Math.ceil;
var exp = Math.exp;
var floor = Math.floor;
var hypot = Math.hypot;
var log = Math.log;
var pow = Math.pow;
var sin = Math.sin;
var sign = Math.sign || function(x) { return x > 0 ? 1 : x < 0 ? -1 : 0; };
var sqrt = Math.sqrt;
var tan = Math.tan;

function acos(x) {
  return x > 1 ? 0 : x < -1 ? pi : Math.acos(x);
}

function asin(x) {
  return x > 1 ? halfPi : x < -1 ? -halfPi : Math.asin(x);
}

function haversin(x) {
  return (x = sin(x / 2)) * x;
}
