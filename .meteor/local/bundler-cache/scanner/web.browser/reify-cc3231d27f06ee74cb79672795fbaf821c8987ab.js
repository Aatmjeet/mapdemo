module.export({gnomonicRaw:()=>gnomonicRaw});let atan,cos,sin;module.link("../math.js",{atan(v){atan=v},cos(v){cos=v},sin(v){sin=v}},0);let azimuthalInvert;module.link("./azimuthal.js",{azimuthalInvert(v){azimuthalInvert=v}},1);let projection;module.link("./index.js",{default(v){projection=v}},2);



function gnomonicRaw(x, y) {
  var cy = cos(y), k = cos(x) * cy;
  return [cy * sin(x) / k, sin(y) / k];
}

gnomonicRaw.invert = azimuthalInvert(atan);

module.exportDefault(function() {
  return projection(gnomonicRaw)
      .scale(144.049)
      .clipAngle(60);
});
