module.export({azimuthalEquidistantRaw:()=>azimuthalEquidistantRaw});let acos,sin;module.link("../math.js",{acos(v){acos=v},sin(v){sin=v}},0);let azimuthalRaw,azimuthalInvert;module.link("./azimuthal.js",{azimuthalRaw(v){azimuthalRaw=v},azimuthalInvert(v){azimuthalInvert=v}},1);let projection;module.link("./index.js",{default(v){projection=v}},2);



var azimuthalEquidistantRaw = azimuthalRaw(function(c) {
  return (c = acos(c)) && c / sin(c);
});

azimuthalEquidistantRaw.invert = azimuthalInvert(function(z) {
  return z;
});

module.exportDefault(function() {
  return projection(azimuthalEquidistantRaw)
      .scale(79.4188)
      .clipAngle(180 - 1e-3);
});
