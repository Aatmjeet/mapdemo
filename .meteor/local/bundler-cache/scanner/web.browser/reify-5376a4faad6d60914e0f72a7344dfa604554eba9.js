module.export({spherical:()=>spherical,cartesian:()=>cartesian,cartesianDot:()=>cartesianDot,cartesianCross:()=>cartesianCross,cartesianAddInPlace:()=>cartesianAddInPlace,cartesianScale:()=>cartesianScale,cartesianNormalizeInPlace:()=>cartesianNormalizeInPlace});let asin,atan2,cos,sin,sqrt;module.link("./math.js",{asin(v){asin=v},atan2(v){atan2=v},cos(v){cos=v},sin(v){sin=v},sqrt(v){sqrt=v}},0);

function spherical(cartesian) {
  return [atan2(cartesian[1], cartesian[0]), asin(cartesian[2])];
}

function cartesian(spherical) {
  var lambda = spherical[0], phi = spherical[1], cosPhi = cos(phi);
  return [cosPhi * cos(lambda), cosPhi * sin(lambda), sin(phi)];
}

function cartesianDot(a, b) {
  return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

function cartesianCross(a, b) {
  return [a[1] * b[2] - a[2] * b[1], a[2] * b[0] - a[0] * b[2], a[0] * b[1] - a[1] * b[0]];
}

// TODO return a
function cartesianAddInPlace(a, b) {
  a[0] += b[0], a[1] += b[1], a[2] += b[2];
}

function cartesianScale(vector, k) {
  return [vector[0] * k, vector[1] * k, vector[2] * k];
}

// TODO return d
function cartesianNormalizeInPlace(d) {
  var l = sqrt(d[0] * d[0] + d[1] * d[1] + d[2] * d[2]);
  d[0] /= l, d[1] /= l, d[2] /= l;
}
