let Adder;module.link("d3-array",{Adder(v){Adder=v}},0);let abs,atan2,cos,radians,sin,sqrt;module.link("./math.js",{abs(v){abs=v},atan2(v){atan2=v},cos(v){cos=v},radians(v){radians=v},sin(v){sin=v},sqrt(v){sqrt=v}},1);let noop;module.link("./noop.js",{default(v){noop=v}},2);let stream;module.link("./stream.js",{default(v){stream=v}},3);




var lengthSum,
    lambda0,
    sinPhi0,
    cosPhi0;

var lengthStream = {
  sphere: noop,
  point: noop,
  lineStart: lengthLineStart,
  lineEnd: noop,
  polygonStart: noop,
  polygonEnd: noop
};

function lengthLineStart() {
  lengthStream.point = lengthPointFirst;
  lengthStream.lineEnd = lengthLineEnd;
}

function lengthLineEnd() {
  lengthStream.point = lengthStream.lineEnd = noop;
}

function lengthPointFirst(lambda, phi) {
  lambda *= radians, phi *= radians;
  lambda0 = lambda, sinPhi0 = sin(phi), cosPhi0 = cos(phi);
  lengthStream.point = lengthPoint;
}

function lengthPoint(lambda, phi) {
  lambda *= radians, phi *= radians;
  var sinPhi = sin(phi),
      cosPhi = cos(phi),
      delta = abs(lambda - lambda0),
      cosDelta = cos(delta),
      sinDelta = sin(delta),
      x = cosPhi * sinDelta,
      y = cosPhi0 * sinPhi - sinPhi0 * cosPhi * cosDelta,
      z = sinPhi0 * sinPhi + cosPhi0 * cosPhi * cosDelta;
  lengthSum.add(atan2(sqrt(x * x + y * y), z));
  lambda0 = lambda, sinPhi0 = sinPhi, cosPhi0 = cosPhi;
}

module.exportDefault(function(object) {
  lengthSum = new Adder();
  stream(object, lengthStream);
  return +lengthSum;
});
