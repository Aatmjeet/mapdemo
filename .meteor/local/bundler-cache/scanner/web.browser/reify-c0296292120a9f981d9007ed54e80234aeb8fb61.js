module.export({eckert3Raw:()=>eckert3Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let pi,sqrt;module.link("./math.js",{pi(v){pi=v},sqrt(v){sqrt=v}},1);


function eckert3Raw(lambda, phi) {
  var k = sqrt(pi * (4 + pi));
  return [
    2 / k * lambda * (1 + sqrt(1 - 4 * phi * phi / (pi * pi))),
    4 / k * phi
  ];
}

eckert3Raw.invert = function(x, y) {
  var k = sqrt(pi * (4 + pi)) / 2;
  return [
    x * k / (1 + sqrt(1 - y * y * (4 + pi) / (4 * pi))),
    y * k / 2
  ];
};

module.exportDefault(function() {
  return projection(eckert3Raw)
      .scale(180.739);
});
