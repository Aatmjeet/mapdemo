module.export({crasterRaw:()=>crasterRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let asin,cos,sin,sqrt,sqrtPi;module.link("./math.js",{asin(v){asin=v},cos(v){cos=v},sin(v){sin=v},sqrt(v){sqrt=v},sqrtPi(v){sqrtPi=v}},1);


var sqrt3 = sqrt(3);

function crasterRaw(lambda, phi) {
  return [sqrt3 * lambda * (2 * cos(2 * phi / 3) - 1) / sqrtPi, sqrt3 * sqrtPi * sin(phi / 3)];
}

crasterRaw.invert = function(x, y) {
  var phi = 3 * asin(y / (sqrt3 * sqrtPi));
  return [sqrtPi * x / (sqrt3 * (2 * cos(2 * phi / 3) - 1)), phi];
};

module.exportDefault(function() {
  return projection(crasterRaw)
      .scale(156.19);
});
