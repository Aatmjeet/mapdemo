module.export({equirectangularRaw:()=>equirectangularRaw});let projection;module.link("./index.js",{default(v){projection=v}},0);

function equirectangularRaw(lambda, phi) {
  return [lambda, phi];
}

equirectangularRaw.invert = equirectangularRaw;

module.exportDefault(function() {
  return projection(equirectangularRaw)
      .scale(152.63);
});
