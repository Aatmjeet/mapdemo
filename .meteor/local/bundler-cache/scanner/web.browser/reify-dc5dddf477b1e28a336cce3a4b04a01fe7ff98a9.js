let mollweideRaw;module.link("../mollweide.js",{mollweideRaw(v){mollweideRaw=v}},0);let interrupt;module.link("./index.js",{default(v){interrupt=v}},1);


var lobes = [[ // northern hemisphere
  [[-180,   0], [ -90,  90], [   0,   0]],
  [[   0,   0], [  90,  90], [ 180,   0]]
], [ // southern hemisphere
  [[-180,   0], [ -90, -90], [   0,   0]],
  [[   0,   0], [  90, -90], [ 180,   0]]
]];

module.exportDefault(function() {
  return interrupt(mollweideRaw, lobes)
      .scale(169.529)
      .rotate([20, 0]);
});
