module.export({mtFlatPolarParabolicRaw:()=>mtFlatPolarParabolicRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let asin,cos,sin,sqrt;module.link("./math.js",{asin(v){asin=v},cos(v){cos=v},sin(v){sin=v},sqrt(v){sqrt=v}},1);


var sqrt6 = sqrt(6),
    sqrt7 = sqrt(7);

function mtFlatPolarParabolicRaw(lambda, phi) {
  var theta = asin(7 * sin(phi) / (3 * sqrt6));
  return [
    sqrt6 * lambda * (2 * cos(2 * theta / 3) - 1) / sqrt7,
    9 * sin(theta / 3) / sqrt7
  ];
}

mtFlatPolarParabolicRaw.invert = function(x, y) {
  var theta = 3 * asin(y * sqrt7 / 9);
  return [
    x * sqrt7 / (sqrt6 * (2 * cos(2 * theta / 3) - 1)),
    asin(sin(theta) * 3 * sqrt6 / 7)
  ];
};

module.exportDefault(function() {
  return projection(mtFlatPolarParabolicRaw)
      .scale(164.859);
});
