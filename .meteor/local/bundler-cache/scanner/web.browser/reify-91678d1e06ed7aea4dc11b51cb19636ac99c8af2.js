module.export({eckert5Raw:()=>eckert5Raw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let cos,pi,sqrt;module.link("./math.js",{cos(v){cos=v},pi(v){pi=v},sqrt(v){sqrt=v}},1);


function eckert5Raw(lambda, phi) {
  return [
    lambda * (1 + cos(phi)) / sqrt(2 + pi),
    2 * phi / sqrt(2 + pi)
  ];
}

eckert5Raw.invert = function(x, y) {
  var k = sqrt(2 + pi),
      phi = y * k / 2;
  return [
    k * x / (1 + cos(phi)),
    phi
  ];
};

module.exportDefault(function() {
  return projection(eckert5Raw)
      .scale(173.044);
});
