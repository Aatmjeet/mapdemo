let projectionMutator;module.link("d3-geo",{geoProjectionMutator(v){projectionMutator=v}},0);let degrees,radians;module.link("./math.js",{degrees(v){degrees=v},radians(v){radians=v}},1);


module.exportDefault(function(projectAt) {
  var phi0 = 0,
      m = projectionMutator(projectAt),
      p = m(phi0);

  p.parallel = function(_) {
    return arguments.length ? m(phi0 = _ * radians) : phi0 * degrees;
  };

  return p;
});
