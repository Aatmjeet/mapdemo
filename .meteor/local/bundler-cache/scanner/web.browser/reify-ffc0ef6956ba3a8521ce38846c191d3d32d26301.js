let gringortenRaw;module.link("../gringorten.js",{gringortenRaw(v){gringortenRaw=v}},0);let quincuncial;module.link("./index.js",{default(v){quincuncial=v}},1);


module.exportDefault(function() {
  return quincuncial(gringortenRaw)
      .scale(176.423);
});
