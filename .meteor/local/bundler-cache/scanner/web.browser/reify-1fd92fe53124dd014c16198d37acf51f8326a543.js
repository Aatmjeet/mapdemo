module.export({azimuthalEqualAreaRaw:()=>azimuthalEqualAreaRaw});let asin,sqrt;module.link("../math.js",{asin(v){asin=v},sqrt(v){sqrt=v}},0);let azimuthalRaw,azimuthalInvert;module.link("./azimuthal.js",{azimuthalRaw(v){azimuthalRaw=v},azimuthalInvert(v){azimuthalInvert=v}},1);let projection;module.link("./index.js",{default(v){projection=v}},2);



var azimuthalEqualAreaRaw = azimuthalRaw(function(cxcy) {
  return sqrt(2 / (1 + cxcy));
});

azimuthalEqualAreaRaw.invert = azimuthalInvert(function(z) {
  return 2 * asin(z / 2);
});

module.exportDefault(function() {
  return projection(azimuthalEqualAreaRaw)
      .scale(124.75)
      .clipAngle(180 - 1e-3);
});
