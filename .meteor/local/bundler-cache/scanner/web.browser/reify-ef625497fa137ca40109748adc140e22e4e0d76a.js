module.export({foucautRaw:()=>foucautRaw});let projection;module.link("d3-geo",{geoProjection(v){projection=v}},0);let atan,cos,sqrtPi,tan;module.link("./math.js",{atan(v){atan=v},cos(v){cos=v},sqrtPi(v){sqrtPi=v},tan(v){tan=v}},1);


function foucautRaw(lambda, phi) {
  var k = phi / 2, cosk = cos(k);
  return [ 2 * lambda / sqrtPi * cos(phi) * cosk * cosk, sqrtPi * tan(k)];
}

foucautRaw.invert = function(x, y) {
  var k = atan(y / sqrtPi), cosk = cos(k), phi = 2 * k;
  return [x * sqrtPi / 2 / (cos(phi) * cosk * cosk), phi];
};

module.exportDefault(function() {
  return projection(foucautRaw)
      .scale(135.264);
});
