module.export({AxisLine:function(){return AxisLine}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var Sprite;module.link("../../core/Sprite",{Sprite:function(v){Sprite=v}},1);var registry;module.link("../../core/Registry",{registry:function(v){registry=v}},2);var color;module.link("../../core/utils/Color",{color:function(v){color=v}},3);var InterfaceColorSet;module.link("../../core/utils/InterfaceColorSet",{InterfaceColorSet:function(v){InterfaceColorSet=v}},4);/**
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */




/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Used to draw Axis line.
 *
 * @see {@link IAxisLineEvents} for a list of available events
 * @see {@link IAxisLineAdapters} for a list of available Adapters
 */
var AxisLine = /** @class */ (function (_super) {
    __extends(AxisLine, _super);
    /**
     * Constructor
     */
    function AxisLine() {
        var _this = _super.call(this) || this;
        _this.className = "AxisLine";
        _this.element = _this.paper.add("path");
        var interfaceColors = new InterfaceColorSet();
        _this.stroke = interfaceColors.getFor("grid");
        _this.strokeOpacity = 0.15;
        _this.pixelPerfect = true;
        _this.fill = color();
        _this.applyTheme();
        _this.interactionsEnabled = false;
        return _this;
        //this.element.moveTo({ x: 0, y: 0 });
    }
    return AxisLine;
}(Sprite));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["AxisLine"] = AxisLine;
//# sourceMappingURL=AxisLine.js.map