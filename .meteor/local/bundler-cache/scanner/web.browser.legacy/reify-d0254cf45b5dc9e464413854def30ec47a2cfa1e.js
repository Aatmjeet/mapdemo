module.export({Modal:function(){return Modal}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var Popup;module.link("./Popup",{Popup:function(v){Popup=v}},1);var Adapter;module.link("../utils/Adapter",{Adapter:function(v){Adapter=v}},2);/**
 * Modal class is used to display information over chart area.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */


/**
 * Shows an HTML modal which covers window or a chart area.
 *
 * @see {@link https://www.amcharts.com/docs/v4/concepts/popups-and-modals/} For examples and docs on Popups and Modals.
 */
var Modal = /** @class */ (function (_super) {
    __extends(Modal, _super);
    /**
     * Constructor
     */
    function Modal() {
        var _this = _super.call(this) || this;
        /**
         * Adapter.
         */
        _this.adapter = new Adapter(_this);
        _this.className = "Modal";
        _this.showCurtain = true;
        _this.draggable = false;
        return _this;
    }
    return Modal;
}(Popup));

//# sourceMappingURL=Modal.js.map