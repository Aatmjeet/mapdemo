module.export({MapArcSeriesDataItem:function(){return MapArcSeriesDataItem},MapArcSeries:function(){return MapArcSeries}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var MapLineSeries,MapLineSeriesDataItem;module.link("./MapLineSeries",{MapLineSeries:function(v){MapLineSeries=v},MapLineSeriesDataItem:function(v){MapLineSeriesDataItem=v}},1);var MapArc;module.link("./MapArc",{MapArc:function(v){MapArc=v}},2);var registry;module.link("../../core/Registry",{registry:function(v){registry=v}},3);/**
 * Map arc series module.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */



/**
 * ============================================================================
 * DATA ITEM
 * ============================================================================
 * @hidden
 */
/**
 * Defines a [[DataItem]] for [[MapArcSeries]].
 *
 * @see {@link DataItem}
 */
var MapArcSeriesDataItem = /** @class */ (function (_super) {
    __extends(MapArcSeriesDataItem, _super);
    /**
     * Constructor
     */
    function MapArcSeriesDataItem() {
        var _this = _super.call(this) || this;
        _this.className = "MapArcSeriesDataItem";
        _this.applyTheme();
        return _this;
    }
    return MapArcSeriesDataItem;
}(MapLineSeriesDataItem));

/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * A series of arc elements. (curved lines)
 *
 * @see {@link IMapArcSeriesEvents} for a list of available Events
 * @see {@link IMapArcSeriesAdapters} for a list of available Adapters
 * @important
 */
var MapArcSeries = /** @class */ (function (_super) {
    __extends(MapArcSeries, _super);
    /**
     * Constructor
     */
    function MapArcSeries() {
        var _this = _super.call(this) || this;
        _this.className = "MapArcSeries";
        _this.applyTheme();
        return _this;
    }
    /**
     * Returns a new/empty DataItem of the type appropriate for this object.
     *
     * @see {@link DataItem}
     * @return Data Item
     */
    MapArcSeries.prototype.createDataItem = function () {
        return new MapArcSeriesDataItem();
    };
    /**
     * Returns a new line instance of suitable type.
     *
     * @return New line
     */
    MapArcSeries.prototype.createLine = function () {
        return new MapArc();
    };
    return MapArcSeries;
}(MapLineSeries));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["MapArcSeries"] = MapArcSeries;
registry.registeredClasses["MapArcSeriesDataItem"] = MapArcSeriesDataItem;
//# sourceMappingURL=MapArcSeries.js.map