module.export({ConeSeriesDataItem:function(){return ConeSeriesDataItem},ConeSeries:function(){return ConeSeries}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var ColumnSeries,ColumnSeriesDataItem;module.link("./ColumnSeries",{ColumnSeries:function(v){ColumnSeries=v},ColumnSeriesDataItem:function(v){ColumnSeriesDataItem=v}},1);var ConeColumn;module.link("../elements/ConeColumn",{ConeColumn:function(v){ConeColumn=v}},2);var registry;module.link("../../core/Registry",{registry:function(v){registry=v}},3);var $path;module.link("../../core/rendering/Path",{"*":function(v){$path=v}},4);/**
 * ConeSeries module
 * Not recommended using if you use scrollbars or your chart is zoomable in some other way.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */




/**
 * ============================================================================
 * DATA ITEM
 * ============================================================================
 * @hidden
 */
/**
 * Defines a [[DataItem]] for [[ConeSeries]].
 *
 * @see {@link DataItem}
 */
var ConeSeriesDataItem = /** @class */ (function (_super) {
    __extends(ConeSeriesDataItem, _super);
    /**
     * Constructor
     */
    function ConeSeriesDataItem() {
        var _this = _super.call(this) || this;
        _this.className = "ConeSeriesDataItem";
        _this.applyTheme();
        return _this;
    }
    return ConeSeriesDataItem;
}(ColumnSeriesDataItem));

/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Defines [[Series]] for a cone graph.
 *
 * @see {@link IConeSeriesEvents} for a list of available Events
 * @see {@link IConeSeriesAdapters} for a list of available Adapters
 * @todo Example
 * @important
 */
var ConeSeries = /** @class */ (function (_super) {
    __extends(ConeSeries, _super);
    /**
     * Constructor
     */
    function ConeSeries() {
        var _this = _super.call(this) || this;
        _this.className = "ConeSeries";
        _this.applyTheme();
        return _this;
    }
    /**
     * Returns an element to use for Candlestick
     * @ignore
     * @return Element.
     */
    ConeSeries.prototype.createColumnTemplate = function () {
        return new ConeColumn();
    };
    /**
     * Returns an SVG path to use as series mask.
     *
     * @return SVG path
     */
    ConeSeries.prototype.getMaskPath = function () {
        var dx = 0;
        var dy = 0;
        var column = this.columns.getIndex(0);
        if (column) {
            if (this.baseAxis == this.xAxis) {
                dy = column.coneColumn.innerWidth / 2 + 1;
            }
            else {
                dx = column.coneColumn.innerHeight / 2 + 1;
            }
            return $path.rectToPath({
                x: -dx,
                y: 0,
                width: this.xAxis.axisLength + dx,
                height: this.yAxis.axisLength + dy
            });
        }
    };
    /**
     * Validates data item's elements.
     *
     * @ignore Exclude from docs
     * @param dataItem  Data item
     */
    ConeSeries.prototype.validateDataElementReal = function (dataItem) {
        _super.prototype.validateDataElementReal.call(this, dataItem);
        var column = dataItem.column;
        if (column) {
            var coneColumn = dataItem.column.coneColumn;
            coneColumn.fill = dataItem.column.fill;
            if (this.baseAxis == this.yAxis) {
                coneColumn.orientation = "horizontal";
            }
            else {
                coneColumn.orientation = "vertical";
            }
        }
    };
    return ConeSeries;
}(ColumnSeries));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["ConeSeries"] = ConeSeries;
registry.registeredClasses["ConeSeriesDataItem"] = ConeSeriesDataItem;
//# sourceMappingURL=ConeSeries.js.map