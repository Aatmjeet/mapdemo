module.export({MapArc:function(){return MapArc}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var MapLine;module.link("./MapLine",{MapLine:function(v){MapLine=v}},1);var Polyarc;module.link("../../core/elements/Polyarc",{Polyarc:function(v){Polyarc=v}},2);var registry;module.link("../../core/Registry",{registry:function(v){registry=v}},3);/**
 * Map arched line module
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */



/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Used to draw an arched line on the map.
 *
 * @see {@link IMapArcEvents} for a list of available events
 * @see {@link IMapArcAdapters} for a list of available Adapters
 */
var MapArc = /** @class */ (function (_super) {
    __extends(MapArc, _super);
    /**
     * Constructor
     */
    function MapArc() {
        var _this = 
        // Init
        _super.call(this) || this;
        _this.className = "MapArc";
        // Apply theme
        _this.applyTheme();
        return _this;
    }
    /**
     * @ignore
     */
    MapArc.prototype.createLine = function () {
        this.line = new Polyarc();
    };
    Object.defineProperty(MapArc.prototype, "shortestDistance", {
        get: function () {
            return false;
        },
        /**
         * `shortestDistance = true` is not supported by `MapArc`.
         *
         * Only [[MapLine]] supports it.
         *
         * @default false
         * @param value
         */
        set: function (value) {
        },
        enumerable: true,
        configurable: true
    });
    return MapArc;
}(MapLine));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["MapArc"] = MapArc;
//# sourceMappingURL=MapArc.js.map