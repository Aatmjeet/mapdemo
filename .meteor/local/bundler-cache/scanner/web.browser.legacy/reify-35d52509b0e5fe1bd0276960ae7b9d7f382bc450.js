module.link("./.internal/charts/types/GaugeChart",{GaugeChartDataItem:"GaugeChartDataItem",GaugeChart:"GaugeChart"},0);module.link("./.internal/charts/types/RadarChart",{RadarChartDataItem:"RadarChartDataItem",RadarChart:"RadarChart"},1);module.link("./.internal/charts/types/XYChart",{XYChartDataItem:"XYChartDataItem",XYChart:"XYChart"},2);module.link("./.internal/charts/types/SerialChart",{SerialChartDataItem:"SerialChartDataItem",SerialChart:"SerialChart"},3);module.link("./.internal/charts/types/PieChart3D",{PieChart3DDataItem:"PieChart3DDataItem",PieChart3D:"PieChart3D"},4);module.link("./.internal/charts/types/PieChart",{PieChartDataItem:"PieChartDataItem",PieChart:"PieChart"},5);module.link("./.internal/charts/types/SlicedChart",{SlicedChart:"SlicedChart",SlicedChartDataItem:"SlicedChartDataItem"},6);module.link("./.internal/charts/types/FlowDiagram",{FlowDiagramDataItem:"FlowDiagramDataItem",FlowDiagram:"FlowDiagram"},7);module.link("./.internal/charts/types/SankeyDiagram",{SankeyDiagramDataItem:"SankeyDiagramDataItem",SankeyDiagram:"SankeyDiagram"},8);module.link("./.internal/charts/types/ChordDiagram",{ChordDiagramDataItem:"ChordDiagramDataItem",ChordDiagram:"ChordDiagram"},9);module.link("./.internal/charts/types/TreeMap",{TreeMapDataItem:"TreeMapDataItem",TreeMap:"TreeMap"},10);module.link("./.internal/charts/types/XYChart3D",{XYChart3DDataItem:"XYChart3DDataItem",XYChart3D:"XYChart3D"},11);module.link("./.internal/charts/Chart",{ChartDataItem:"ChartDataItem",Chart:"Chart"},12);module.link("./.internal/charts/Legend",{LegendDataItem:"LegendDataItem",Legend:"Legend",LegendSettings:"LegendSettings"},13);module.link("./.internal/charts/elements/HeatLegend",{HeatLegend:"HeatLegend"},14);module.link("./.internal/charts/series/Series",{SeriesDataItem:"SeriesDataItem",Series:"Series"},15);module.link("./.internal/charts/series/XYSeries",{XYSeriesDataItem:"XYSeriesDataItem",XYSeries:"XYSeries"},16);module.link("./.internal/charts/series/LineSeries",{LineSeriesDataItem:"LineSeriesDataItem",LineSeries:"LineSeries"},17);module.link("./.internal/charts/series/LineSeriesSegment",{LineSeriesSegment:"LineSeriesSegment"},18);module.link("./.internal/charts/series/CandlestickSeries",{CandlestickSeriesDataItem:"CandlestickSeriesDataItem",CandlestickSeries:"CandlestickSeries"},19);module.link("./.internal/charts/series/OHLCSeries",{OHLCSeriesDataItem:"OHLCSeriesDataItem",OHLCSeries:"OHLCSeries"},20);module.link("./.internal/charts/series/ColumnSeries",{ColumnSeriesDataItem:"ColumnSeriesDataItem",ColumnSeries:"ColumnSeries"},21);module.link("./.internal/charts/series/StepLineSeries",{StepLineSeriesDataItem:"StepLineSeriesDataItem",StepLineSeries:"StepLineSeries"},22);module.link("./.internal/charts/series/RadarSeries",{RadarSeriesDataItem:"RadarSeriesDataItem",RadarSeries:"RadarSeries"},23);module.link("./.internal/charts/series/RadarColumnSeries",{RadarColumnSeriesDataItem:"RadarColumnSeriesDataItem",RadarColumnSeries:"RadarColumnSeries"},24);module.link("./.internal/charts/series/PieSeries",{PieSeriesDataItem:"PieSeriesDataItem",PieSeries:"PieSeries"},25);module.link("./.internal/charts/series/FunnelSeries",{FunnelSeries:"FunnelSeries",FunnelSeriesDataItem:"FunnelSeriesDataItem"},26);module.link("./.internal/charts/series/PyramidSeries",{PyramidSeries:"PyramidSeries",PyramidSeriesDataItem:"PyramidSeriesDataItem"},27);module.link("./.internal/charts/series/PictorialStackedSeries",{PictorialStackedSeries:"PictorialStackedSeries",PictorialStackedSeriesDataItem:"PictorialStackedSeriesDataItem"},28);module.link("./.internal/charts/elements/PieTick",{PieTick:"PieTick"},29);module.link("./.internal/charts/elements/FunnelSlice",{FunnelSlice:"FunnelSlice"},30);module.link("./.internal/charts/series/PieSeries3D",{PieSeries3DDataItem:"PieSeries3DDataItem",PieSeries3D:"PieSeries3D"},31);module.link("./.internal/charts/series/TreeMapSeries",{TreeMapSeriesDataItem:"TreeMapSeriesDataItem",TreeMapSeries:"TreeMapSeries"},32);module.link("./.internal/charts/series/ColumnSeries3D",{ColumnSeries3DDataItem:"ColumnSeries3DDataItem",ColumnSeries3D:"ColumnSeries3D"},33);module.link("./.internal/charts/series/ConeSeries",{ConeSeriesDataItem:"ConeSeriesDataItem",ConeSeries:"ConeSeries"},34);module.link("./.internal/charts/series/CurvedColumnSeries",{CurvedColumnSeries:"CurvedColumnSeries",CurvedColumnSeriesDataItem:"CurvedColumnSeriesDataItem"},35);module.link("./.internal/charts/axes/Axis",{AxisDataItem:"AxisDataItem",Axis:"Axis"},36);module.link("./.internal/charts/axes/Grid",{Grid:"Grid"},37);module.link("./.internal/charts/axes/AxisTick",{AxisTick:"AxisTick"},38);module.link("./.internal/charts/axes/AxisLabel",{AxisLabel:"AxisLabel"},39);module.link("./.internal/charts/axes/AxisLine",{AxisLine:"AxisLine"},40);module.link("./.internal/charts/axes/AxisFill",{AxisFill:"AxisFill"},41);module.link("./.internal/charts/axes/AxisRenderer",{AxisRenderer:"AxisRenderer"},42);module.link("./.internal/charts/axes/AxisBreak",{AxisBreak:"AxisBreak"},43);module.link("./.internal/charts/axes/AxisBullet",{AxisBullet:"AxisBullet"},44);module.link("./.internal/charts/axes/ValueAxis",{ValueAxisDataItem:"ValueAxisDataItem",ValueAxis:"ValueAxis"},45);module.link("./.internal/charts/axes/CategoryAxis",{CategoryAxisDataItem:"CategoryAxisDataItem",CategoryAxis:"CategoryAxis"},46);module.link("./.internal/charts/axes/CategoryAxisBreak",{CategoryAxisBreak:"CategoryAxisBreak"},47);module.link("./.internal/charts/axes/DateAxis",{DateAxisDataItem:"DateAxisDataItem",DateAxis:"DateAxis"},48);module.link("./.internal/charts/axes/DurationAxis",{DurationAxisDataItem:"DurationAxisDataItem",DurationAxis:"DurationAxis"},49);module.link("./.internal/charts/axes/DateAxisBreak",{DateAxisBreak:"DateAxisBreak"},50);module.link("./.internal/charts/axes/ValueAxisBreak",{ValueAxisBreak:"ValueAxisBreak"},51);module.link("./.internal/charts/axes/AxisRendererX",{AxisRendererX:"AxisRendererX"},52);module.link("./.internal/charts/axes/AxisRendererY",{AxisRendererY:"AxisRendererY"},53);module.link("./.internal/charts/axes/AxisRendererRadial",{AxisRendererRadial:"AxisRendererRadial"},54);module.link("./.internal/charts/axes/AxisLabelCircular",{AxisLabelCircular:"AxisLabelCircular"},55);module.link("./.internal/charts/axes/AxisRendererCircular",{AxisRendererCircular:"AxisRendererCircular"},56);module.link("./.internal/charts/axes/AxisFillCircular",{AxisFillCircular:"AxisFillCircular"},57);module.link("./.internal/charts/axes/GridCircular",{GridCircular:"GridCircular"},58);module.link("./.internal/charts/axes/AxisRendererX3D",{AxisRendererX3D:"AxisRendererX3D"},59);module.link("./.internal/charts/axes/AxisRendererY3D",{AxisRendererY3D:"AxisRendererY3D"},60);module.link("./.internal/charts/elements/Tick",{Tick:"Tick"},61);module.link("./.internal/charts/elements/Bullet",{Bullet:"Bullet"},62);module.link("./.internal/charts/elements/LabelBullet",{LabelBullet:"LabelBullet"},63);module.link("./.internal/charts/elements/CircleBullet",{CircleBullet:"CircleBullet"},64);module.link("./.internal/charts/elements/ErrorBullet",{ErrorBullet:"ErrorBullet"},65);module.link("./.internal/charts/elements/XYChartScrollbar",{XYChartScrollbar:"XYChartScrollbar"},66);module.link("./.internal/charts/elements/ClockHand",{ClockHand:"ClockHand"},67);module.link("./.internal/charts/elements/FlowDiagramNode",{FlowDiagramNode:"FlowDiagramNode"},68);module.link("./.internal/charts/elements/FlowDiagramLink",{FlowDiagramLink:"FlowDiagramLink"},69);module.link("./.internal/charts/elements/SankeyNode",{SankeyNode:"SankeyNode"},70);module.link("./.internal/charts/elements/SankeyLink",{SankeyLink:"SankeyLink"},71);module.link("./.internal/charts/elements/ChordNode",{ChordNode:"ChordNode"},72);module.link("./.internal/charts/elements/ChordLink",{ChordLink:"ChordLink"},73);module.link("./.internal/charts/elements/NavigationBar",{NavigationBarDataItem:"NavigationBarDataItem",NavigationBar:"NavigationBar"},74);module.link("./.internal/charts/elements/Column",{Column:"Column"},75);module.link("./.internal/charts/elements/Candlestick",{Candlestick:"Candlestick"},76);module.link("./.internal/charts/elements/OHLC",{OHLC:"OHLC"},77);module.link("./.internal/charts/elements/RadarColumn",{RadarColumn:"RadarColumn"},78);module.link("./.internal/charts/elements/Column3D",{Column3D:"Column3D"},79);module.link("./.internal/charts/elements/ConeColumn",{ConeColumn:"ConeColumn"},80);module.link("./.internal/charts/elements/CurvedColumn",{CurvedColumn:"CurvedColumn"},81);module.link("./.internal/charts/cursors/XYCursor",{XYCursor:"XYCursor"},82);module.link("./.internal/charts/cursors/Cursor",{Cursor:"Cursor"},83);module.link("./.internal/charts/cursors/RadarCursor",{RadarCursor:"RadarCursor"},84);/**
 * Module: gauge
 */
/**
 * Elements: types
 */












/**
 * Elements: charts
 */



/**
 * Elements: series
 */





















/**
 * Elements: axes
 */

























/**
 * Elements: elements
 */





















/**
 * Elements: cursors
 */



//# sourceMappingURL=charts.js.map