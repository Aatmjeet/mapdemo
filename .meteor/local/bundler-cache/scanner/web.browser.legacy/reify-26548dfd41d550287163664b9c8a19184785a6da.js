module.export({OHLC:function(){return OHLC}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var Candlestick;module.link("./Candlestick",{Candlestick:function(v){Candlestick=v}},1);var Line;module.link("../../core/elements/Line",{Line:function(v){Line=v}},2);var registry;module.link("../../core/Registry",{registry:function(v){registry=v}},3);/**
 * Module that defines everything related to building OHLCs.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */



/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Class used to creates OHLCs.
 *
 * @see {@link IOHLCEvents} for a list of available events
 * @see {@link IOHLCAdapters} for a list of available Adapters
 * @todo Usage example
 * @important
 */
var OHLC = /** @class */ (function (_super) {
    __extends(OHLC, _super);
    /**
     * Constructor
     */
    function OHLC() {
        var _this = _super.call(this) || this;
        _this.className = "OHLC";
        _this.layout = "none";
        return _this;
    }
    /**
     * @ignore
     */
    OHLC.prototype.createAssets = function () {
        //super.createAssets();
        this.openLine = this.createChild(Line);
        this.openLine.shouldClone = false;
        this.highLowLine = this.createChild(Line);
        this.highLowLine.shouldClone = false;
        this.closeLine = this.createChild(Line);
        this.closeLine.shouldClone = false;
    };
    /**
     * Copies all parameters from another [[OHLC]].
     *
     * @param source Source OHLC
     */
    OHLC.prototype.copyFrom = function (source) {
        _super.prototype.copyFrom.call(this, source);
        if (this.openLine) {
            this.openLine.copyFrom(source.openLine);
        }
        if (this.highLowLine) {
            this.highLowLine.copyFrom(source.highLowLine);
        }
        if (this.closeLine) {
            this.closeLine.copyFrom(source.closeLine);
        }
    };
    return OHLC;
}(Candlestick));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["OHLC"] = OHLC;
//# sourceMappingURL=OHLC.js.map