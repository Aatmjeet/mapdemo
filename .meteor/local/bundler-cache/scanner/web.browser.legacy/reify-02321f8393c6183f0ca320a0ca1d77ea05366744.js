module.export({MapSpline:function(){return MapSpline}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var MapLine;module.link("./MapLine",{MapLine:function(v){MapLine=v}},1);var Polyspline;module.link("../../core/elements/Polyspline",{Polyspline:function(v){Polyspline=v}},2);var registry;module.link("../../core/Registry",{registry:function(v){registry=v}},3);/**
 * Map spline module
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */



/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Used to draw a spline on the map.
 *
 * @see {@link IMapSplineEvents} for a list of available events
 * @see {@link IMapSplineAdapters} for a list of available Adapters
 */
var MapSpline = /** @class */ (function (_super) {
    __extends(MapSpline, _super);
    /**
     * Constructor
     */
    function MapSpline() {
        var _this = 
        // Init
        _super.call(this) || this;
        _this.className = "MapSpline";
        // Apply theme
        _this.applyTheme();
        return _this;
    }
    /**
     * @ignore
     */
    MapSpline.prototype.createLine = function () {
        this.line = new Polyspline();
        this.line.tensionX = 0.8;
        this.line.tensionY = 0.8;
    };
    Object.defineProperty(MapSpline.prototype, "shortestDistance", {
        /**
         * ShortestDistance = true is not supported by MapSpline, only MapLine does support it
         * @default false
         * @param value
         * @todo: review description
         */
        get: function () {
            return false;
        },
        set: function (value) {
        },
        enumerable: true,
        configurable: true
    });
    return MapSpline;
}(MapLine));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["MapSpline"] = MapSpline;
//# sourceMappingURL=MapSpline.js.map