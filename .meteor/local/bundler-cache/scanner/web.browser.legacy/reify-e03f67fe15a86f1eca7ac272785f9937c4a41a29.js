module.export({SlicedChartDataItem:function(){return SlicedChartDataItem},SlicedChart:function(){return SlicedChart}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var PercentChart,PercentChartDataItem;module.link("./PercentChart",{PercentChart:function(v){PercentChart=v},PercentChartDataItem:function(v){PercentChartDataItem=v}},1);var registry;module.link("../../core/Registry",{registry:function(v){registry=v}},2);var $type;module.link("../../core/utils/Type",{"*":function(v){$type=v}},3);/**
 * Sliced chart module.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */



/**
 * ============================================================================
 * DATA ITEM
 * ============================================================================
 * @hidden
 */
/**
 * Defines a [[DataItem]] for [[SlicedChart]].
 *
 * @see {@link DataItem}
 */
var SlicedChartDataItem = /** @class */ (function (_super) {
    __extends(SlicedChartDataItem, _super);
    /**
     * Constructor
     */
    function SlicedChartDataItem() {
        var _this = _super.call(this) || this;
        _this.className = "SlicedChartDataItem";
        _this.applyTheme();
        return _this;
    }
    return SlicedChartDataItem;
}(PercentChartDataItem));

/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Creates a Sliced chart.
 *
 * @see {@link ISlicedChartEvents} for a list of available Events
 * @see {@link ISlicedChartAdapters} for a list of available Adapters
 * @see {@link https://www.amcharts.com/docs/v4/chart-types/sliced-chart/} for documentation
 * @important
 */
var SlicedChart = /** @class */ (function (_super) {
    __extends(SlicedChart, _super);
    /**
     * Constructor
     */
    function SlicedChart() {
        var _this = 
        // Init
        _super.call(this) || this;
        _this.className = "SlicedChart";
        _this.seriesContainer.layout = "horizontal";
        _this.padding(15, 15, 15, 15);
        // Apply theme
        _this.applyTheme();
        return _this;
    }
    /**
     * Sets defaults that instantiate some objects that rely on parent, so they
     * cannot be set in constructor.
     */
    SlicedChart.prototype.applyInternalDefaults = function () {
        _super.prototype.applyInternalDefaults.call(this);
        // Add a default screen reader title for accessibility
        // This will be overridden in screen reader if there are any `titles` set
        if (!$type.hasValue(this.readerTitle)) {
            this.readerTitle = this.language.translate("Sliced chart");
        }
    };
    /**
     * (Re)validates the chart, causing it to redraw.
     *
     * @ignore Exclude from docs
     */
    SlicedChart.prototype.validate = function () {
        _super.prototype.validate.call(this);
    };
    return SlicedChart;
}(PercentChart));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["SlicedChart"] = SlicedChart;
registry.registeredClasses["SlicedChartDataItem"] = SlicedChartDataItem;
//# sourceMappingURL=SlicedChart.js.map