module.export({log:function(){return log},warn:function(){return warn}});var __read,__spread;module.link("tslib",{__read:function(v){__read=v},__spread:function(v){__spread=v}},0);var options;module.link("../Options",{options:function(v){options=v}},1);

/**
 * Outputs string to console if `verbose` is `true`.
 */
function log() {
    var messages = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        messages[_i] = arguments[_i];
    }
    if (options.verbose) {
        if (console) {
            console.log.apply(console, __spread(messages));
        }
    }
}
/**
 * Outputs a warning to the console.
 */
function warn() {
    var messages = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        messages[_i] = arguments[_i];
    }
    if (!options.suppressWarnings) {
        if (console) {
            console.warn.apply(console, __spread(messages));
        }
    }
}
//# sourceMappingURL=Log.js.map