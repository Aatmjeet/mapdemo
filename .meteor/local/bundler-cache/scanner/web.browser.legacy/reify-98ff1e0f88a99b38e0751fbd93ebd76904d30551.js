module.export({ErrorBullet:function(){return ErrorBullet}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var Bullet;module.link("./Bullet",{Bullet:function(v){Bullet=v}},1);var Sprite;module.link("../../core/Sprite",{Sprite:function(v){Sprite=v}},2);var registry;module.link("../../core/Registry",{registry:function(v){registry=v}},3);var $path;module.link("../../core/rendering/Path",{"*":function(v){$path=v}},4);/**
 * Bullet module
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */




/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Creates a bullet with a textual label.
 *
 * Uses [[Label]] instance to draw the label, so the label itself is
 * configurable.
 *
 * @see {@link IBulletEvents} for a list of available events
 * @see {@link IBulletAdapters} for a list of available Adapters
 * @todo Usage example
 * @important
 */
var ErrorBullet = /** @class */ (function (_super) {
    __extends(ErrorBullet, _super);
    /**
     * Constructor
     */
    function ErrorBullet() {
        var _this = _super.call(this) || this;
        _this.className = "ErrorBullet";
        _this.errorLine = _this.createChild(Sprite);
        _this.errorLine.shouldClone = false;
        _this.width = 20;
        _this.height = 20;
        _this.strokeOpacity = 1;
        _this.isDynamic = true;
        return _this;
    }
    ErrorBullet.prototype.validatePosition = function () {
        _super.prototype.validatePosition.call(this);
        var w = this.pixelWidth / 2;
        var h = this.pixelHeight / 2;
        this.errorLine.path = $path.moveTo({ x: -w, y: -h }) + $path.lineTo({ x: w, y: -h }) + $path.moveTo({ x: 0, y: -h }) + $path.lineTo({ x: 0, y: h }) + $path.moveTo({ x: -w, y: h }) + $path.lineTo({ x: w, y: h });
    };
    /**
     * Copies all proprities and related stuff from another instance of
     * [[ErrorBullet]].
     *
     * @param source  Source element
     */
    ErrorBullet.prototype.copyFrom = function (source) {
        _super.prototype.copyFrom.call(this, source);
        this.errorLine.copyFrom(source.errorLine);
    };
    return ErrorBullet;
}(Bullet));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["ErrorBullet"] = ErrorBullet;
//# sourceMappingURL=ErrorBullet.js.map