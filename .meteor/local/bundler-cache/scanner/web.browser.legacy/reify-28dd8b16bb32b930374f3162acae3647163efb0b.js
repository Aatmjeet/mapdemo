module.export({isSafari:function(){return isSafari},isInternetExplorer:function(){return isInternetExplorer}});// Also detects iOS
function isSafari() {
    return /apple/i.test(navigator.vendor);
}
function isInternetExplorer() {
    return /MSIE |Trident\//.test(navigator.userAgent);
}
//# sourceMappingURL=Browser.js.map