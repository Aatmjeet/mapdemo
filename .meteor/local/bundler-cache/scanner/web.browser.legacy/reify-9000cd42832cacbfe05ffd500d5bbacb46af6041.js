module.export({PlayButton:function(){return PlayButton}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var Button;module.link("./Button",{Button:function(v){Button=v}},1);var RoundedRectangle;module.link("./RoundedRectangle",{RoundedRectangle:function(v){RoundedRectangle=v}},2);var registry;module.link("../Registry",{registry:function(v){registry=v}},3);var InterfaceColorSet;module.link("../../core/utils/InterfaceColorSet",{InterfaceColorSet:function(v){InterfaceColorSet=v}},4);var Triangle;module.link("./Triangle",{Triangle:function(v){Triangle=v}},5);var $type;module.link("../../core/utils/Type",{"*":function(v){$type=v}},6);/**
 * Play button functionality.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */






/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Creates a zoom out button.
 *
 * @see {@link IPlayButtonEvents} for a list of available events
 * @see {@link IPlayButtonAdapters} for a list of available Adapters
 */
var PlayButton = /** @class */ (function (_super) {
    __extends(PlayButton, _super);
    /**
     * Constructor
     */
    function PlayButton() {
        var _this = 
        // Init
        _super.call(this) || this;
        _this.className = "PlayButton";
        _this.padding(12, 12, 12, 12);
        _this.showSystemTooltip = true;
        var interfaceColors = new InterfaceColorSet();
        var background = _this.background;
        background.cornerRadius(25, 25, 25, 25);
        background.fill = interfaceColors.getFor("primaryButton");
        background.stroke = interfaceColors.getFor("primaryButtonStroke");
        background.strokeOpacity = 0;
        background.states.getKey("hover").properties.fill = interfaceColors.getFor("primaryButtonHover");
        background.states.getKey("down").properties.fill = interfaceColors.getFor("primaryButtonActive");
        // Create a play icon
        var playIcon = new Triangle();
        playIcon.direction = "right";
        playIcon.width = 9;
        playIcon.height = 11;
        playIcon.marginLeft = 1;
        playIcon.marginRight = 1;
        playIcon.horizontalCenter = "middle";
        playIcon.verticalCenter = "middle";
        playIcon.stroke = interfaceColors.getFor("primaryButtonText");
        playIcon.fill = playIcon.stroke;
        _this.icon = playIcon;
        // Create a play icon
        var stopIcon = new RoundedRectangle();
        stopIcon.width = 11;
        stopIcon.height = 11;
        stopIcon.horizontalCenter = "middle";
        stopIcon.verticalCenter = "middle";
        stopIcon.cornerRadius(0, 0, 0, 0);
        stopIcon.stroke = interfaceColors.getFor("primaryButtonText");
        stopIcon.fill = playIcon.stroke;
        _this.togglable = true;
        var activeState = _this.states.create("active");
        activeState.transitionDuration = 0;
        activeState.properties.icon = stopIcon;
        _this.defaultState.transitionDuration = 0;
        // Apply theme
        _this.applyTheme();
        return _this;
    }
    /**
     * Sets defaults that instantiate some objects that rely on parent, so they
     * cannot be set in constructor.
     */
    PlayButton.prototype.applyInternalDefaults = function () {
        _super.prototype.applyInternalDefaults.call(this);
        if (!$type.hasValue(this.readerTitle)) {
            this.readerTitle = this.language.translate("Play");
        }
    };
    return PlayButton;
}(Button));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["PlayButton"] = PlayButton;
//# sourceMappingURL=PlayButton.js.map