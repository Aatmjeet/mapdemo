module.export({MapSplineSeriesDataItem:function(){return MapSplineSeriesDataItem},MapSplineSeries:function(){return MapSplineSeries}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var MapLineSeries,MapLineSeriesDataItem;module.link("./MapLineSeries",{MapLineSeries:function(v){MapLineSeries=v},MapLineSeriesDataItem:function(v){MapLineSeriesDataItem=v}},1);var MapSpline;module.link("./MapSpline",{MapSpline:function(v){MapSpline=v}},2);var registry;module.link("../../core/Registry",{registry:function(v){registry=v}},3);/**
 * Map spline series module
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */



/**
 * ============================================================================
 * DATA ITEM
 * ============================================================================
 * @hidden
 */
/**
 * Defines a [[DataItem]] for [[MapSplineSeries]]
 * @see {@link DataItem}
 */
var MapSplineSeriesDataItem = /** @class */ (function (_super) {
    __extends(MapSplineSeriesDataItem, _super);
    /**
     * Constructor
     */
    function MapSplineSeriesDataItem() {
        var _this = _super.call(this) || this;
        _this.className = "MapSplineSeriesDataItem";
        _this.applyTheme();
        return _this;
    }
    return MapSplineSeriesDataItem;
}(MapLineSeriesDataItem));

/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * A series of map spline elements.
 *
 * @see {@link IMapSplineSeriesEvents} for a list of available Events
 * @see {@link IMapSplineSeriesAdapters} for a list of available Adapters
 * @important
 */
var MapSplineSeries = /** @class */ (function (_super) {
    __extends(MapSplineSeries, _super);
    /**
     * Constructor
     */
    function MapSplineSeries() {
        var _this = _super.call(this) || this;
        _this.className = "MapSplineSeries";
        _this.applyTheme();
        return _this;
    }
    /**
     * Returns a new/empty DataItem of the type appropriate for this object.
     *
     * @see {@link DataItem}
     * @return Data Item
     */
    MapSplineSeries.prototype.createDataItem = function () {
        return new MapSplineSeriesDataItem();
    };
    /**
     * Returns a new line instance of suitable type.
     *
     * @return New line
     */
    MapSplineSeries.prototype.createLine = function () {
        return new MapSpline();
    };
    return MapSplineSeries;
}(MapLineSeries));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["MapSplineSeries"] = MapSplineSeries;
registry.registeredClasses["MapSplineSeriesDataItem"] = MapSplineSeriesDataItem;
//# sourceMappingURL=MapSplineSeries.js.map