module.export({PX:function(){return PX},STRING:function(){return STRING},NUMBER:function(){return NUMBER},DATE:function(){return DATE},DURATION:function(){return DURATION},PLACEHOLDER:function(){return PLACEHOLDER},PLACEHOLDER2:function(){return PLACEHOLDER2}});/**
 * A collection of String-based constants.
 * @hidden
 * @ignore Exclude from docs
 */
/**
 * @ignore Exclude from docs
 */
var PX = "px";
/**
 * @ignore Exclude from docs
 */
var STRING = "string";
/**
 * @ignore Exclude from docs
 */
var NUMBER = "number";
/**
 * @ignore Exclude from docs
 */
var DATE = "date";
/**
 * @ignore Exclude from docs
 */
var DURATION = "duration";
/**
 * @ignore Exclude from docs
 */
var PLACEHOLDER = "__§§§__";
/**
 * @ignore Exclude from docs
 */
var PLACEHOLDER2 = "__§§§§__";
//# sourceMappingURL=Strings.js.map