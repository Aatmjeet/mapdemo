module.export({TextLink:function(){return TextLink}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var Label;module.link("../../core/elements/Label",{Label:function(v){Label=v}},1);var MouseCursorStyle;module.link("../../core/interaction/Mouse",{MouseCursorStyle:function(v){MouseCursorStyle=v}},2);var InterfaceColorSet;module.link("../../core/utils/InterfaceColorSet",{InterfaceColorSet:function(v){InterfaceColorSet=v}},3);var registry;module.link("../Registry",{registry:function(v){registry=v}},4);/**
 * A module that defines Text element used to indicate links.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */




/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Creates a text element with a link.
 *
 * @see {@link ITextLinkEvents} for a list of available events
 * @see {@link ITextLinkAdapters} for a list of available Adapters
 */
var TextLink = /** @class */ (function (_super) {
    __extends(TextLink, _super);
    /**
     * Constructor
     */
    function TextLink() {
        var _this = _super.call(this) || this;
        _this.className = "TextLink";
        _this.selectable = true;
        var interfaceColors = new InterfaceColorSet();
        _this.fill = interfaceColors.getFor("primaryButton").brighten(0.3);
        var hoverState = _this.states.create("hover");
        hoverState.properties.fill = interfaceColors.getFor("primaryButtonHover").brighten(0.3);
        var downState = _this.states.create("down");
        downState.properties.fill = interfaceColors.getFor("primaryButtonDown").brighten(0.3);
        _this.cursorOverStyle = MouseCursorStyle.pointer;
        _this.applyTheme();
        return _this;
    }
    return TextLink;
}(Label));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["TextLink"] = TextLink;
//# sourceMappingURL=TextLink.js.map