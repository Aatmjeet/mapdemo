module.export({Circle:function(){return Circle}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var Sprite;module.link("../Sprite",{Sprite:function(v){Sprite=v}},1);var percent;module.link("../utils/Percent",{percent:function(v){percent=v}},2);var registry;module.link("../Registry",{registry:function(v){registry=v}},3);var $utils;module.link("../utils/Utils",{"*":function(v){$utils=v}},4);var $math;module.link("../utils/Math",{"*":function(v){$math=v}},5);/**
 * Functionality for drawing circles.
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */





/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Used to create a circle
 * @see {@link ICircleEvents} for a list of available events
 * @see {@link ICircleAdapters} for a list of available Adapters
 */
var Circle = /** @class */ (function (_super) {
    __extends(Circle, _super);
    /**
     * Constructor
     */
    function Circle() {
        var _this = _super.call(this) || this;
        _this.className = "Circle";
        _this.element = _this.paper.add("circle");
        _this.setPercentProperty("radius", percent(100));
        _this.setPropertyValue("horizontalCenter", "middle");
        _this.setPropertyValue("verticalCenter", "middle");
        _this.applyTheme();
        return _this;
    }
    /**
     * Draws the circle.
     */
    Circle.prototype.draw = function () {
        _super.prototype.draw.call(this);
        this.element.attr({ "r": this.pixelRadius });
    };
    Object.defineProperty(Circle.prototype, "radius", {
        /**
         * @return Radius
         */
        get: function () {
            return this.getPropertyValue("radius");
        },
        /**
         * Radius of the circle.
         *
         * Can be either absolute (pixels) or relative ([Percent]).
         *
         * @param value  Radius
         */
        set: function (value) {
            this.setPercentProperty("radius", value, true, false, 10, false);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Circle.prototype, "pixelRadius", {
        /**
         * Radius of the circle in pixels.
         *
         * This is a read-only property. To set radius in pixels, use `radius`
         * property.
         *
         * @readonly
         * @return Radius (px)
         */
        get: function () {
            return $utils.relativeToValue(this.radius, $math.min(this.innerWidth / 2, this.innerHeight / 2));
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Updates bounding box.
     *
     * @ignore Exclude from docs
     */
    Circle.prototype.measureElement = function () {
        var pixelRadius = this.pixelRadius;
        this._bbox = {
            x: -pixelRadius,
            y: -pixelRadius,
            width: pixelRadius * 2,
            height: pixelRadius * 2
        };
    };
    return Circle;
}(Sprite));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["Circle"] = Circle;
//# sourceMappingURL=Circle.js.map