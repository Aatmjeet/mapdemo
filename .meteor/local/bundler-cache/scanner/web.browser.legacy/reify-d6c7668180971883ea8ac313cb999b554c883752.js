module.export({Graticule:function(){return Graticule}});var __extends;module.link("tslib",{__extends:function(v){__extends=v}},0);var MapLine;module.link("./MapLine",{MapLine:function(v){MapLine=v}},1);var registry;module.link("../../core/Registry",{registry:function(v){registry=v}},2);/**
 * Graticule (map grid line).
 */

/**
 * ============================================================================
 * IMPORTS
 * ============================================================================
 * @hidden
 */


/**
 * ============================================================================
 * MAIN CLASS
 * ============================================================================
 * @hidden
 */
/**
 * Graticule is a map line spanning from the poles or around the globe.
 *
 * @since 4.3.0
 * @see {@link IGraticuleEvents} for a list of available events
 * @see {@link IGraticuleAdapters} for a list of available Adapters
 */
var Graticule = /** @class */ (function (_super) {
    __extends(Graticule, _super);
    /**
     * Constructor
     */
    function Graticule() {
        var _this = 
        // Init
        _super.call(this) || this;
        _this.className = "Graticule";
        // Apply theme
        _this.applyTheme();
        _this.shortestDistance = true;
        return _this;
    }
    return Graticule;
}(MapLine));

/**
 * Register class in system, so that it can be instantiated using its name from
 * anywhere.
 *
 * @ignore
 */
registry.registeredClasses["Graticule"] = Graticule;
//# sourceMappingURL=Graticule.js.map