//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var meteorInstall = Package.modules.meteorInstall;
var Promise = Package.promise.Promise;
var fetch = Package.fetch.fetch;

var require = meteorInstall({"node_modules":{"meteor":{"dynamic-import":{"client.js":function module(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                       //
// packages/dynamic-import/client.js                                                                     //
//                                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                         //
var Module = module.constructor;
var cache = require("./cache.js");
var meteorInstall = require("meteor/modules").meteorInstall;
var dynamicVersions = require("./dynamic-versions.js");

// Fix for Safari 14 bug (https://bugs.webkit.org/show_bug.cgi?id=226547), do not delete this unused var
var idb = global.indexedDB;

var dynamicImportSettings = Meteor.settings
    && Meteor.settings.public
    && Meteor.settings.public.packages
    && Meteor.settings.public.packages['dynamic-import'] || {};

// Call module.dynamicImport(id) to fetch a module and any/all of its
// dependencies that have not already been fetched, and evaluate them as
// soon as they arrive. This runtime API makes it very easy to implement
// ECMAScript dynamic import(...) syntax.
Module.prototype.dynamicImport = function (id) {
  var module = this;
  return module.prefetch(id).then(function () {
    return getNamespace(module, id);
  });
};

// Called by Module.prototype.prefetch if there are any missing dynamic
// modules that need to be fetched.
meteorInstall.fetch = function (ids) {
  var tree = Object.create(null);
  var versions = Object.create(null);
  var missing;

  function addSource(id, source) {
    addToTree(tree, id, makeModuleFunction(id, source, ids[id].options));
  }

  function addMissing(id) {
    addToTree(missing = missing || Object.create(null), id, 1);
  }

  Object.keys(ids).forEach(function (id) {
    var version = dynamicVersions.get(id);
    if (version) {
      versions[id] = version;
    } else {
      addMissing(id);
    }
  });

  return cache.checkMany(versions).then(function (sources) {
    Object.keys(sources).forEach(function (id) {
      var source = sources[id];
      if (source) {
        addSource(id, source);
      } else {
        addMissing(id);
      }
    });

    return missing && fetchMissing(missing).then(function (results) {
      var versionsAndSourcesById = Object.create(null);
      var flatResults = flattenModuleTree(results);

      Object.keys(flatResults).forEach(function (id) {
        var source = flatResults[id];
        addSource(id, source);

        var version = dynamicVersions.get(id);
        if (version) {
          versionsAndSourcesById[id] = {
            version: version,
            source: source
          };
        }
      });

      cache.setMany(versionsAndSourcesById);
    });

  }).then(function () {
    return tree;
  });
};

function flattenModuleTree(tree) {
  var parts = [""];
  var result = Object.create(null);

  function walk(t) {
    if (t && typeof t === "object") {
      Object.keys(t).forEach(function (key) {
        parts.push(key);
        walk(t[key]);
        parts.pop();
      });
    } else if (typeof t === "string") {
      result[parts.join("/")] = t;
    }
  }

  walk(tree);

  return result;
}

function makeModuleFunction(id, source, options) {
  // By calling (options && options.eval || eval) in a wrapper function,
  // we delay the cost of parsing and evaluating the module code until the
  // module is first imported.
  return function () {
    // If an options.eval function was provided in the second argument to
    // meteorInstall when this bundle was first installed, use that
    // function to parse and evaluate the dynamic module code in the scope
    // of the package. Otherwise fall back to indirect (global) eval.
    return (options && options.eval || eval)(
      // Wrap the function(require,exports,module){...} expression in
      // parentheses to force it to be parsed as an expression.
      "(" + source + ")\n//# sourceURL=" + id
    ).apply(this, arguments);
  };
}

var secretKey = null;
exports.setSecretKey = function (key) {
  secretKey = key;
};

var fetchURL = require("./common.js").fetchURL;

function inIframe() {
  try {
    return window.self !== window.top;
  } catch (e) {
    return true;
  }
}

function fetchMissing(missingTree) {
  // If the hostname of the URL returned by Meteor.absoluteUrl differs
  // from location.host, then we'll be making a cross-origin request here,
  // but that's fine because the dynamic-import server sets appropriate
  // CORS headers to enable fetching dynamic modules from any
  // origin. Browsers that check CORS do so by sending an additional
  // preflight OPTIONS request, which may add latency to the first dynamic
  // import() request, so it's a good idea for ROOT_URL to match
  // location.host if possible, though not strictly necessary.

  var url = fetchURL;

  var useLocationOrigin = dynamicImportSettings.useLocationOrigin;

  var disableLocationOriginIframe = dynamicImportSettings.disableLocationOriginIframe;

  if (useLocationOrigin && location && !(disableLocationOriginIframe && inIframe())) {
    url = location.origin.concat(__meteor_runtime_config__.ROOT_URL_PATH_PREFIX || '', url);
  } else {
    url = Meteor.absoluteUrl(url);
  }

  if (secretKey) {
    url += "key=" + secretKey;
  }

  return fetch(url, {
    method: "POST",
    body: JSON.stringify(missingTree)
  }).then(function (res) {
    if (! res.ok) throw res;
    return res.json();
  });
}

function addToTree(tree, id, value) {
  var parts = id.split("/");
  var lastIndex = parts.length - 1;
  parts.forEach(function (part, i) {
    if (part) {
      tree = tree[part] = tree[part] ||
        (i < lastIndex ? Object.create(null) : value);
    }
  });
}

function getNamespace(module, id) {
  var namespace;

  module.link(id, {
    "*": function (ns) {
      namespace = ns;
    }
  });

  // This helps with Babel interop, since we're not just returning the
  // module.exports object.
  Object.defineProperty(namespace, "__esModule", {
    value: true,
    enumerable: false
  });

  return namespace;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

},"cache.js":function module(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                       //
// packages/dynamic-import/cache.js                                                                      //
//                                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                         //
var dbPromise;

var canUseCache =
  // The server doesn't benefit from dynamic module fetching, and almost
  // certainly doesn't support IndexedDB.
  Meteor.isClient &&
  // Cordova bundles all modules into the monolithic initial bundle, so
  // the dynamic module cache won't be necessary.
  ! Meteor.isCordova &&
  // Caching can be confusing in development, and is designed to be a
  // transparent optimization for production performance.
  Meteor.isProduction;

function getIDB() {
  if (typeof indexedDB !== "undefined") return indexedDB;
  if (typeof webkitIndexedDB !== "undefined") return webkitIndexedDB;
  if (typeof mozIndexedDB !== "undefined") return mozIndexedDB;
  if (typeof OIndexedDB !== "undefined") return OIndexedDB;
  if (typeof msIndexedDB !== "undefined") return msIndexedDB;
}

function withDB(callback) {
  dbPromise = dbPromise || new Promise(function (resolve, reject) {
    var idb = getIDB();
    if (! idb) {
      throw new Error("IndexedDB not available");
    }

    // Incrementing the version number causes all existing object stores
    // to be deleted and recreates those specified by objectStoreMap.
    var request = idb.open("MeteorDynamicImportCache", 2);

    request.onupgradeneeded = function (event) {
      var db = event.target.result;

      // It's fine to delete existing object stores since onupgradeneeded
      // is only called when we change the DB version number, and the data
      // we're storing is disposable/reconstructible.
      Array.from(db.objectStoreNames).forEach(db.deleteObjectStore, db);

      Object.keys(objectStoreMap).forEach(function (name) {
        db.createObjectStore(name, objectStoreMap[name]);
      });
    };

    request.onerror = makeOnError(reject, "indexedDB.open");
    request.onsuccess = function (event) {
      resolve(event.target.result);
    };
  });

  return dbPromise.then(callback, function (error) {
    return callback(null);
  });
}

var objectStoreMap = {
  sourcesByVersion: { keyPath: "version" }
};

function makeOnError(reject, source) {
  return function (event) {
    reject(new Error(
      "IndexedDB failure in " + source + " " +
        JSON.stringify(event.target)
    ));

    // Returning true from an onerror callback function prevents an
    // InvalidStateError in Firefox during Private Browsing. Silencing
    // that error is safe because we handle the error more gracefully by
    // passing it to the Promise reject function above.
    // https://github.com/meteor/meteor/issues/8697
    return true;
  };
}

var checkCount = 0;

exports.checkMany = function (versions) {
  var ids = Object.keys(versions);
  var sourcesById = Object.create(null);

  // Initialize sourcesById with null values to indicate all sources are
  // missing (unless replaced with actual sources below).
  ids.forEach(function (id) {
    sourcesById[id] = null;
  });

  if (! canUseCache) {
    return Promise.resolve(sourcesById);
  }

  return withDB(function (db) {
    if (! db) {
      // We thought we could used IndexedDB, but something went wrong
      // while opening the database, so err on the side of safety.
      return sourcesById;
    }

    var txn = db.transaction([
      "sourcesByVersion"
    ], "readonly");

    var sourcesByVersion = txn.objectStore("sourcesByVersion");

    ++checkCount;

    function finish() {
      --checkCount;
      return sourcesById;
    }

    return Promise.all(ids.map(function (id) {
      return new Promise(function (resolve, reject) {
        var version = versions[id];
        if (version) {
          var sourceRequest = sourcesByVersion.get(version);
          sourceRequest.onerror = makeOnError(reject, "sourcesByVersion.get");
          sourceRequest.onsuccess = function (event) {
            var result = event.target.result;
            if (result) {
              sourcesById[id] = result.source;
            }
            resolve();
          };
        } else resolve();
      });
    })).then(finish, finish);
  });
};

var pendingVersionsAndSourcesById = Object.create(null);

exports.setMany = function (versionsAndSourcesById) {
  if (canUseCache) {
    Object.assign(
      pendingVersionsAndSourcesById,
      versionsAndSourcesById
    );

    // Delay the call to flushSetMany so that it doesn't contribute to the
    // amount of time it takes to call module.dynamicImport.
    if (! flushSetMany.timer) {
      flushSetMany.timer = setTimeout(flushSetMany, 100);
    }
  }
};

function flushSetMany() {
  if (checkCount > 0) {
    // If checkMany is currently underway, postpone the flush until later,
    // since updating the cache is less important than reading from it.
    return flushSetMany.timer = setTimeout(flushSetMany, 100);
  }

  flushSetMany.timer = null;

  var versionsAndSourcesById = pendingVersionsAndSourcesById;
  pendingVersionsAndSourcesById = Object.create(null);

  return withDB(function (db) {
    if (! db) {
      // We thought we could used IndexedDB, but something went wrong
      // while opening the database, so err on the side of safety.
      return;
    }

    var setTxn = db.transaction([
      "sourcesByVersion"
    ], "readwrite");

    var sourcesByVersion = setTxn.objectStore("sourcesByVersion");

    return Promise.all(
      Object.keys(versionsAndSourcesById).map(function (id) {
        var info = versionsAndSourcesById[id];
        return new Promise(function (resolve, reject) {
          var request = sourcesByVersion.put({
            version: info.version,
            source: info.source
          });
          request.onerror = makeOnError(reject, "sourcesByVersion.put");
          request.onsuccess = resolve;
        });
      })
    );
  });
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

},"common.js":function module(require,exports){

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                       //
// packages/dynamic-import/common.js                                                                     //
//                                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                         //
exports.fetchURL = "/__meteor__/dynamic-import/fetch";

///////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dynamic-versions.js":function module(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                       //
// packages/dynamic-import/dynamic-versions.js                                                           //
//                                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                         //
// This magic double-underscored identifier gets replaced in
// tools/isobuild/bundler.js with a tree of hashes of all dynamic
// modules, for use in client.js and cache.js.
var versions = {"node_modules":{"pdfmake":{"build":{"pdfmake.js":"d461d282c9a29ee22bcb514af35c665359483dba"}},"@amcharts":{"amcharts4":{".internal":{"pdfmake":{"vfs_fonts.js":"c15185c4c5eb2c7916dd056f18dcb26a45fef995"},"canvg":{"index.js":"a8111edc9befb507b1eb50b47700184dd93bde93"}}}},"core-js":{"modules":{"es.array.map.js":"9362d101fef4fe8806f13166ebbaa5a57538b254","es.regexp.exec.js":"4a3ae02d65ef591e58bce93b8f1f43d4b38d2ab1","es.string.match.js":"d77bd347b3bc3b7010a5cc89ba6d1f886e5f2dac","es.string.replace.js":"6d5ecf4836e63da9300d0233eba799cc79e83bc1","es.string.starts-with.js":"42e2a40347f8752eff36ab221eab5c48f343c0f9","es.array.join.js":"84908badb44d9f134037b8ce6ca95e38265628fa","es.symbol.js":"6563adfd303be4278bcdadf54f8ad3e0bce98ab9","es.array.filter.js":"37beec27bd396416882f832c279bb58615bc1c92","es.array.for-each.js":"ef5a5d8673d5d17d588bfd674df44d923ea04801","es.object.get-own-property-descriptor.js":"94ecda5a5bbd1bb1da98c94b92adf3ad651ad759","es.object.get-own-property-descriptors.js":"71bbeee6449900a8095e3e9e4070dbcd7c65f373","es.object.keys.js":"6261489ac86a73b2cdcc80ae11c4b8a470ce6d9c","web.dom-collections.for-each.js":"bb03d71a170f0c160b802890424e9e6d9f09ae64","es.array.concat.js":"1d9329890ef0d0646f5e4d533335a50114d77a82","es.array.every.js":"12db317fe54e9ed993b65158a4001f021e1692dc","es.array.reduce.js":"81ed3aadebdffe2a73ab232f2739c398d2a9fb3e","es.object.to-string.js":"10224f1d7b697b6e7128779ddc0a941e2c007103","es.promise.js":"78a0e704c5821df7037d508714ec1608463be1c7","es.string.split.js":"41b71133cd17e321fd80056b90687a99bf8e338b","es.function.name.js":"e1860d11e8049b31fc129a70ba44bad499b4a612","es.reflect.construct.js":"ae9df1fb3881a12bb4855c9270f21513d3e29285","es.regexp.to-string.js":"f28168f680e849ee22a6d73101ebcbfbac4f00cf","es.array.from.js":"2b6a3e852c7c32cbd18eb539cdfd41cccb80ef9e","es.array.includes.js":"92ae7d7ad7fbe80356071768d6476f7d7ad44d9a","es.array.some.js":"d33da8a5c152bcd8fc31dff7c2c2c77964eb76e3","es.string.includes.js":"2860f12b806207dd1217b14735d5a3702d433e4f","es.string.iterator.js":"8d4b646c8bbcf01b88ebfbb529f89d588316aa09","es.string.trim.js":"174a8c834757bc751e2b7600c57add2615719bb8","es.array.index-of.js":"f3da95fd4aab4824c1c414096a1d98ce4b3b5990","es.array.reverse.js":"a7fc8992b633343b6cdd0893db68615f0f78f4b6","es.number.constructor.js":"3a26d09dece88b67951d25cc576dcc091bbb4575","es.array.fill.js":"03993fbe5f203366c485c76dd87269d0a17acd8b","es.reflect.delete-property.js":"5176b2cfefd40fe264eeb5b8e4d10e7e426178ab","es.array.iterator.js":"cc41eafcb1873693f60caccce8d827dacaaef102","web.dom-collections.iterator.js":"28b86f398ed49384b7f8bc36c88c327dd182e601","es.symbol.description.js":"b54f2a35cfee72b2107a65702f6d27e4ea612d23","es.symbol.iterator.js":"42342f06e39b3b68444a503e7d5f4304d264e5ee","es.array.slice.js":"29d808d835868c85fbaaa69dec21d6862b6a9e5b","es.map.js":"9d4965fbe066ea0e3b387de8329aea6f9d3d34bc","es.reflect.apply.js":"f451479c4e8cd36b5b624d9eca11397ec40f6986","es.reflect.get-prototype-of.js":"47b322730ddf7a2a6f0915f591ab6c3c4751b03c"},"internals":{"export.js":"bbd724f3d30b8d32686bf5fb71267f793641d6df","global.js":"e09bcdcb6784f8a8865fdbc916728ccf77f18384","object-get-own-property-descriptor.js":"bfdfd86b0252f84a176b17fc6eb40d99384e1a30","descriptors.js":"e2f6aebcb3590f6d593f8cbadb67a7b2b1b65a8c","fails.js":"469f25a22b6b1abb5b436ee64c8e808cf343b4bb","function-call.js":"1ea6c2bb6e7e868af6373a10de0013a93f18d079","object-property-is-enumerable.js":"c4e3ef9d6eef4a54296bad7a8ed7bcd9dd92ab4a","create-property-descriptor.js":"bab4e0325f71e68c47e6a7e34c138cf4359a7692","to-indexed-object.js":"22d3c0541ffb7f4049af92fb1a4875dc1aebabcc","indexed-object.js":"2418d12660f41685bd00e845cc5d4d008f9f10fd","function-uncurry-this.js":"251bff24075c3c0d92363e97f30acc245acc1547","classof-raw.js":"e12ae4344d9e5d1539d32f64d290c220fcd91298","require-object-coercible.js":"574752d11624c7b1fdeb0a23d6397535cbdff7dd","to-property-key.js":"1da483f0b93c0d89b297f26a92c0f9bd9221096c","to-primitive.js":"17c98ecde8a4e70b7ff3626ae716becabcf1306a","is-object.js":"e053e2fa5f1de6a9685a7d45a9284784eff83b73","is-callable.js":"246276a91c5d1c8ce9a00f463f8a8d44933b8712","is-symbol.js":"39b806e8a639e89f5868604b8785d34d3243cab3","get-built-in.js":"febedbe5a5b928bf15ccccd8f936b4333f1b714e","object-is-prototype-of.js":"60cb84e53f33fdaeeb22dacf007cdc567366f0b1","use-symbol-as-uid.js":"acd644ee21ca3f0c8fe4a8c0ebacaff1ce9d1c34","native-symbol.js":"c9337f63700d355d31a3b33977ecf2c8f4f082f5","engine-v8-version.js":"71ad74e85bb2d8608c42b085dc2f6f38c0f53bc0","engine-user-agent.js":"8a2d76b0a684ddec5dde4b89ee3091a3f16cad5f","get-method.js":"3a4a5184402b90f8beddc9f86a8025e4dd845b31","a-callable.js":"3c419753ccfd45b4eb5761024648e1d3e75e2a7d","try-to-string.js":"c6de6662aecca9573b9e91f58eaf6b9a4e1f0f60","ordinary-to-primitive.js":"598b9819439ce56164bb601a6decab086e105a4a","well-known-symbol.js":"db3ea74392d672671facbf033850739f1b5fb21e","shared.js":"9ba01b63aab31c310067e3d369d3e8f8a690df23","is-pure.js":"ee982f42a11598d9082833b5cd0ae4ae33a77cd9","shared-store.js":"a6935f0e4472653762141bdf9ca72401522dacfd","set-global.js":"f8fd7e33135f0f4a79e4e06719a05aeefd503c68","has-own-property.js":"d49b6ff01b4f49253d14955a503a49e31e62ca43","to-object.js":"85ed2657fa9fe9011d163fcfbd82bc2c8856afef","uid.js":"68c7f200cc67cbc5b360ea5399d96d3d7ec7b4dc","ie8-dom-define.js":"302fa3118ebebc4f93649bf2625b8725d1a3f80f","document-create-element.js":"b930a6fdb5adb82a4ee7c111e993e8ea23dc2410","create-non-enumerable-property.js":"b695cd27368fa0e6bcb81eeb454b4ab251576c49","object-define-property.js":"142b1d501a1bf09f348e680bb64e12771c23d886","an-object.js":"6598a234686b827687f170c04ef84bbcce37f7c0","redefine.js":"904e9fb2e19fc6cf7ea74104ac829b6aebc3f926","inspect-source.js":"965ea8d8ee4f7c14e4cd8aa0ce401499be010ddd","internal-state.js":"0f1ea2feb947a80a4e1a7d0903feedfe1118409e","native-weak-map.js":"0ac5a10fbf5859c0d431658715a6bc48900e502d","shared-key.js":"eff87157bdb0c47e35a917b2b171ffd45db93469","hidden-keys.js":"ac8ac9d64e6852c366bf42fb2188436932d7f6c6","function-name.js":"85367d148ee6edfa785298bd7253f5a07fadaee9","copy-constructor-properties.js":"54999fc3429ab7883d9924b49142585e7e9efe39","own-keys.js":"53baef69812bfa4c87157498fc81126feb3808f2","object-get-own-property-names.js":"edbc565255b727aee63743113605245fd32b5c58","object-keys-internal.js":"1d5790d111170c2c2595c81e73733754ace7d350","array-includes.js":"cfb24768993ca78c03164370417fadcbb9ab7bf2","to-absolute-index.js":"c4037eff5d8a6919d7aa4b7de09ccff07aea2aa8","to-integer-or-infinity.js":"17c9a4e8282fa8f618c6ad3842badfb904dc2aab","length-of-array-like.js":"9f52cfcba592fad96c38b8b96921d2954052b8ed","to-length.js":"92a5992f93eda25641f733fab479190e1d8ad738","enum-bug-keys.js":"578bfd86e20627d044eb28fc8eaddb371804e999","object-get-own-property-symbols.js":"f11bac8c5b3e97cf7247e63e2d005f7c42322f47","is-forced.js":"c769ffb804b6039a242eb555df26ce65b0e166cd","array-iteration.js":"7373185efa17557e07eb7fe2e7ffa4d089357940","function-bind-context.js":"cec266be9d3c87d9426b4600f6d87b819ac4d277","array-species-create.js":"57a6ef54f6b756ddbb62fe05b6f277f3c5790a5b","array-species-constructor.js":"5e13d829cb5be2e04fb7f4569fd4ba7aa3067fdc","is-array.js":"ebd61f0b0c6cec6c179a83c5a349e3706c85d15b","is-constructor.js":"63fd3a4bc5a3e99fdd616e7bd2329d2f200e4d7a","classof.js":"50fd23763bff1d6cc06469eea1854681ae41f9cc","to-string-tag-support.js":"7e0eea990dddbbba7f6f7c1e50e9857f5d182bbc","array-method-has-species-support.js":"4314cf7acf60cdee5beec054696f23dfa573be48","regexp-exec.js":"beda0ace2bbf5fb86b4bb54dc79cc11f6f398d4f","to-string.js":"9c3b7af97a3a63f404809dabe14b3c66a0fc181a","regexp-flags.js":"3093a3f9d069b665127a0a4f1886699e6fd92b06","regexp-sticky-helpers.js":"02d5ee6e7e875144b961a4bd413f363f6bc0cd55","object-create.js":"6e0cc2b145fcba0c2d50dccdf0962c3568a495e9","object-define-properties.js":"d011098af5fe795b111d2b7e556618287dda661b","object-keys.js":"d7142d5685f290ccb63fc70f98c20777a53073fc","html.js":"799344da123ea765b0d1c4154590702324e5866d","regexp-unsupported-dot-all.js":"d912aabbf2f14b0bad23af06e070397fce8f1bd6","regexp-unsupported-ncg.js":"8eb5e8e6fb77a8904be4f5af4969124797ce693c","fix-regexp-well-known-symbol-logic.js":"526b9d2036fd28e6b2bc3d0cc88b067a9b78e8bd","advance-string-index.js":"6a6bbb3583ec4ece6e5b18fbe93c8f0323267a72","string-multibyte.js":"3fb28b9f5921465c92def51e0ceefe75bcd4c3a7","regexp-exec-abstract.js":"c078a1549aa21cb872340616ff4f33b8dcb29273","function-apply.js":"44737fed06712230e461b731e205af28296e8915","get-substitution.js":"9e228e1b347dca44db36b887e3e0747a0fa4c1ac","not-a-regexp.js":"c744967f181989bfe25008ce9ff2bb4cb07699f0","is-regexp.js":"e55eba9e7e0a013ab6353f71819a5b215bde661d","correct-is-regexp-logic.js":"f29bf9ddd7c934d5e3c922cffa7cd532e04537f1","array-method-is-strict.js":"d62c28012230f5385979b8d6377a291e5286717f","object-get-own-property-names-external.js":"1c5712512107ccf9d880240adf65687575bd98be","array-slice.js":"930365d30bbe73a7b7d76a85460569022e060c80","well-known-symbol-wrapped.js":"c12099d536b8a00d19a0a4a324f0b453931fbe17","define-well-known-symbol.js":"0660eb75d7cddca7a7cb1f797da45ffe4ea231b6","path.js":"e5d30938736c8c0de764ba475f73290de151c5ef","set-to-string-tag.js":"bfaf35adab75161e130db8fb0d97e9c219890400","array-for-each.js":"44e9391826e8537395c5676e3fec276a9cb967c6","create-property.js":"d894d1c2d9ec843377af7c88464e2b713c64e373","dom-iterables.js":"afbaa76ccee7336a88b229c1a52fb9d607851736","dom-token-list-prototype.js":"37a5db0a9d460130b0c788e5edbc2203dd876046","array-reduce.js":"01dda15d346d8ec9b79998bbb9d7ba52bc6e568c","engine-is-node.js":"7770ad350a2d2570344f7177ecce988ee351f624","object-to-string.js":"8d0431cbd9cdac69a4f4568ebce57f717248f0ca","native-promise-constructor.js":"1e40db8622b569d6d8c1d0f6bb6b9c8919756f87","redefine-all.js":"0b01e69dcdfdcb65514eece7c56ad40191c5617f","object-set-prototype-of.js":"74be66734b83d89b54fb232ff6f73e77495993cd","a-possible-prototype.js":"1f590edd2c4f8b78ef9df9a30acf9006649e92ab","set-species.js":"7825984794ca348ae9d2bd5bc4c8a4ab56c3b009","an-instance.js":"e27075821a4b5f99d8dbe62a3fdf9d65de16e517","iterate.js":"b318244e2dc8e713d631704ce4cf816269f8edc1","is-array-iterator-method.js":"36a75944b614d32b8b0999de3a7b72a196223f3c","iterators.js":"c938809dd0f477af549a1e6e00503a49cdbc5dc7","get-iterator.js":"10caf3576fe4af79f0c908212f0148e31c22a460","get-iterator-method.js":"bf146cf4f6a184477df6ef57b61ff62d26144e16","iterator-close.js":"7602cbac69b1c29221d709ba8732bda4cb9039b0","check-correctness-of-iteration.js":"11a83970999adb691e45fa79dfa32daf24fa60c2","species-constructor.js":"08626778436351045b0d3d8bcb1a66c84994b50b","a-constructor.js":"dd493b77648295c4707440739f6888f8134ba180","task.js":"865b41f3ea0f0577b7cb44eee8ee30293bfd8a87","engine-is-ios.js":"8ef870d1eee23a3184142cdf4e63814d1b82ded6","microtask.js":"08748be15b8495415d9305b39df140db06d0ba65","engine-is-ios-pebble.js":"7bc23b631ccee9275d5efbcec7b9ab47aec3105b","engine-is-webos-webkit.js":"2bbea2fa9ed69407caafe737bbf11af5e98ef417","promise-resolve.js":"9ab63d90342d3ff62a03689484bdb57902a19d4d","new-promise-capability.js":"0a84b73a0bb650355610660debb4b3b234b2c0aa","host-report-errors.js":"d8627ddb006def8d83bc7e358dabce7f49917dc1","perform.js":"c815026d9b59f3753816f23bd06cc00e5b6ba021","engine-is-browser.js":"89e1687ff9a8034f1aedbd86a049f2d236ccc1c0","function-bind.js":"ac17e4564482d2bf9fe4cb35f709c93696fb30e3","array-from.js":"3fc884fca8ad32a0eb365bc6b012c7caa5c66784","call-with-safe-iteration-closing.js":"ca5664683a71faec96a97bf44541e73e026779e3","add-to-unscopables.js":"aeb985ca363281ddfd03358d3c65ec2898032e89","define-iterator.js":"b040cc927925474540db26240e1b5fc96304f6ef","create-iterator-constructor.js":"eedbacc82e65d879ee2b6508e677ec48152b6059","iterators-core.js":"4b9ea4c9c85639c8db0fecf3d90c70c715fbb482","object-get-prototype-of.js":"8370feb523fd8acea5a16ec88e2bd176d0f18a4e","correct-prototype-getter.js":"c37e45479e97542d4455805fd608ce6524fe8bad","string-trim.js":"ae2247ee787a5e7706b9e3f97408494d30971312","whitespaces.js":"6c92ccaa0ec345f2a0bad1d9865c5da94e57c601","string-trim-forced.js":"718487d8dbfe6b6edc0bf4717b2c3c4c48b9aa2e","inherit-if-required.js":"a15f35d9e136adbb9c5bf6b78e3b06f72d448904","this-number-value.js":"3a8d5c2d16db7d6358ac5faca322bb19950dc89d","array-fill.js":"5efd645889a94d5712720e4ec24e144576c62a67","collection.js":"5edc86dce4d935bf30aeac54e6755b9547cd02e9","internal-metadata.js":"784205c56960d604f5d7b8fe135aeca24b075f07","object-is-extensible.js":"b57c4cbe8bebc03a12f0e58969f6c656950ccb87","array-buffer-non-extensible.js":"0561a9e8cedbd428915ee345149c9bb2b0785b6a","freezing.js":"7ae6cf1e9f7626c9f1ab27e080e1341f84e32335","collection-strong.js":"24170db977701be99a86956e4d531d446cb01acb"}},"@babel":{"runtime":{"helpers":{"slicedToArray.js":"070f36c0657fabfca290dd340a1fdb86c4c99f48","arrayWithHoles.js":"8ec552ae2fdd7209e53bd3597d7b58c40416c9e8","iterableToArrayLimit.js":"47adcc991a35070696d1011bfea073a821f485b3","unsupportedIterableToArray.js":"8644cb0ec5f98ebe3bcf14221a22f24b6f60bd5f","arrayLikeToArray.js":"1412f2a879198f2085fac1caf51415cb79aafd93","nonIterableRest.js":"a4ebd374b9cb7cee522622155e8630403d4a6c96","asyncToGenerator.js":"79df1daf152916402243686e688d8038a97b6a19","classCallCheck.js":"7bebcf84d181388dc11d618dc3444a1235a38714","createClass.js":"87e68f11880fbed3160b3047a3166f0ea192bdde","possibleConstructorReturn.js":"5763ae2a2ec046e7fb0cf0072116c23edb9a8524","typeof.js":"08cb6d31cfaf44d286d3747c220b326259351684","assertThisInitialized.js":"2180d3a5461073b0775f092ecc2282e15feef82f","getPrototypeOf.js":"bed7b82bdce06c00b7c97a24870d8105d1884c15","inherits.js":"53f0de1e59c0b222f16c6cae1ae22488c5251696","setPrototypeOf.js":"ce7b136489a6a5594279beb7e3e81f2ccddb1d7e","toConsumableArray.js":"7cc48a10bde1a7d087fb97f80dce362ebd5fdb7a","arrayWithoutHoles.js":"c02e770e1abe93bb4111c3031ea8d061ea3a23b3","iterableToArray.js":"2a6fe55fe5fbd240828dec95c6b5fcf0cac26840","nonIterableSpread.js":"71174bc926af75866cc5dec4c50c2ef99ee742ad","get.js":"c6228674101091a4675c133136724ab91ab11937","superPropBase.js":"fbd5835f993e78d562720dc45e027534ad5172fa"},"regenerator":{"index.js":"8ed40e9a11ff6b47050f3cd707ebcfdaa2c194f7"}}},"regenerator-runtime":{"package.json":"3fc7856d243eafc3fa61c87ec22f01f0ec05d910","runtime.js":"2de508b9f968828bcb7e95303223921d176a632d"},"raf":{"package.json":"37e8f14992c21679ef602a2ec29e94cce63d3d5b","index.js":"d0386640f2980f63048c1ecc31e9261bd3ef2bf2"},"performance-now":{"package.json":"84356a3bb9afe42ae8e896168fa6d3eca467f58b","lib":{"performance-now.js":"2bf6a5c056a8268ac8e74d5fd3f47f606f354445"}},"rgbcolor":{"package.json":"03eee5096aa6a06957f239f908591a7e55d468a1","index.js":"e87aa6ac7cbd92f2bee74fa844d408a0a77560a5"},"stackblur-canvas":{"package.json":"4e741fc22d3f3b677f262fe5752d01d478804024","dist":{"stackblur-es.js":"5a300019c2e70101b6af195aebe8ee74f24b4253"}},"xlsx":{"package.json":"3f9b4c8874c08a1fecaae5274248c94d0abf6bd3","xlsx.js":"c71f3b584bf689810718be3f5c10a3d002088c4c","dist":{"cpexcel.js":"8dc695e6b0e953943466eb749e04a0877f760df0"},"jszip.js":"81e257b2e764b2421e0e01d8cf4175efce254b9a"},"meteor-node-stubs":{"deps":{"fs.js":"faf8efaddd1a0bedf00cf55111f2901630384469"}}}};

const METEOR_PREFIX = '/node_modules/meteor/';

exports.get = function (id) {
  var tree = versions;
  var version = null;

  id.split("/").some(function (part) {
    if (part) {
      // If the tree contains identifiers for Meteor packages with colons
      // in their names, the colons should not have been replaced by
      // underscores, but there's a bug that results in that behavior, so
      // for now it seems safest to be tolerant of underscores here.
      // https://github.com/meteor/meteor/pull/9103
      tree = tree[part] || tree[part.replace(":", "_")];
    }

    if (! tree) {
      // Terminate the search without reassigning version.
      return true;
    }

    if (typeof tree === "string") {
      version = tree;
      return true;
    }
  });

  return version;
};

function getFlatModuleArray(tree) {
  var parts = [""];
  var result = [];

  function walk(t) {
    if (t && typeof t === "object") {
      Object.keys(t).forEach(function (key) {
        parts.push(key);
        walk(t[key]);
        parts.pop();
      });
    } else if (typeof t === "string") {
      result.push(parts.join("/"));
    }
  }

  walk(tree);

  return result;
}

// If Package.appcache is loaded, preload additional modules after the
// core bundle has been loaded.
function precacheOnLoad(event) {
  // Check inside onload to make sure Package.appcache has had a chance to
  // become available.
  if (! Package.appcache) {
    return;
  }

  // Prefetch in chunks to reduce overhead. If we call module.prefetch(id)
  // multiple times in the same tick of the event loop, all those modules
  // will be fetched in one HTTP POST request.
  function prefetchInChunks(modules, amount) {
    Promise.all(modules.splice(0, amount).map(function (id) {
      return new Promise(function (resolve, reject) {
        module.prefetch(id).then(resolve).catch(
          function (err) {
            // we probably have a : _ mismatch
            // what can get wrong if we do the replacement
            // 1. a package with a name like a_b:package will not resolve
            // 2. someone falsely imports a_package that does not exist but a
            // package a:package exists, so this one gets imported and its usage
            // will fail
            if (id.indexOf(METEOR_PREFIX) === 0) {
              module.prefetch(
                METEOR_PREFIX + id.replace(METEOR_PREFIX, '').replace('_', ':')
              ).then(resolve).catch(reject);
            } else {
              reject(err);
            }
          })
      });
    })).then(function () {
      if (modules.length > 0) {
        setTimeout(function () {
          prefetchInChunks(modules, amount);
        }, 0);
      }
    });
  }

  // Get a flat array of modules and start prefetching.
  prefetchInChunks(getFlatModuleArray(versions), 50);
}

// Use window.onload to only prefetch after the main bundle has loaded.
if (global.addEventListener) {
  global.addEventListener('load', precacheOnLoad, false);
} else if (global.attachEvent) {
  global.attachEvent('onload', precacheOnLoad);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

}}}}},{
  "extensions": [
    ".js",
    ".json"
  ]
});

var exports = require("/node_modules/meteor/dynamic-import/client.js");

/* Exports */
Package._define("dynamic-import", exports);

})();
