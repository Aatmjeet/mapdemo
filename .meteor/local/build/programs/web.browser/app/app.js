var require = meteorInstall({"import":{"ui":{"body.html":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// import/ui/body.html                                                                                                //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
module.link("./template.body.js", { "*": "*+" });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"template.body.js":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// import/ui/template.body.js                                                                                         //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //

(function () {
  var renderFunc = (function() {
  var view = this;
  return HTML.Raw('<div class="container">\n        <div class="topnav">\n          <a href="/">Home</a>\n          <a href="/showList">Showlist</a>\n          <a href="/search">Search</a>\n        </div>\n      </div>');
});
  Template.body.addContent(renderFunc);
  Meteor.startup(Template.body.renderToDocument);
  if (typeof module === "object" && module.hot) {
    module.hot.accept();
    module.hot.dispose(function () {
      var index = Template.body.contentRenderFuncs.indexOf(renderFunc)
      Template.body.contentRenderFuncs.splice(index, 1);
      Template._applyHmrChanges();
    });
  }
})();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"search.html":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// import/ui/search.html                                                                                              //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
module.link("./template.search.js", { "*": "*+" });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"template.search.js":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// import/ui/template.search.js                                                                                       //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //

Template._migrateTemplate(
  "search",
  new Template("Template.search", (function() {
  var view = this;
  return HTML.DIV({
    class: "searchbar"
  }, HTML.Raw('\n    <div class="form-group">\n      <div id="searchb">\n        <input type="text" class="form-control search-query" id="search-query" placeholder="Search">\n      </div>\n    </div>\n    '), Spacebars.include(view.lookupTemplate("people")), "\n  ");
})),
);
if (typeof module === "object" && module.hot) {
  module.hot.accept();
  module.hot.dispose(function () {
    Template.__pendingReplacement.push("search");
    Template._applyHmrChanges("search");
  });
}

Template._migrateTemplate(
  "people",
  new Template("Template.people", (function() {
  var view = this;
  return HTML.DIV({
    class: "table"
  }, "\n      ", Blaze.If(function() {
    return Spacebars.call(Spacebars.dot(view.lookup("searchResults"), "results"));
  }, function() {
    return [ "\n        ", HTML.TABLE({
      class: "table"
    }, "\n          ", HTML.THEAD("\n            ", HTML.TR(HTML.Raw("\n              <th>Name</th>\n              <th>Type</th>\n              <th>Country</th>\n            ")), "\n          "), "\n          ", HTML.TBODY("\n            ", Blaze.Each(function() {
      return Spacebars.call(Spacebars.dot(view.lookup("searchResults"), "results"));
    }, function() {
      return [ HTML.Raw("\n            <!--{{> searchHelper}}-->\n            "), HTML.TR("\n              ", HTML.TD(Blaze.View("lookup:title", function() {
        return Spacebars.mustache(view.lookup("title"));
      })), "\n              ", HTML.TD(Blaze.View("lookup:device", function() {
        return Spacebars.mustache(view.lookup("device"));
      })), "\n              ", HTML.TD(Blaze.View("lookup:country", function() {
        return Spacebars.mustache(view.lookup("country"));
      })), "\n            "), "\n            " ];
    }), "\n          "), "\n        "), "\n      " ];
  }), "\n    ");
})),
);
if (typeof module === "object" && module.hot) {
  module.hot.accept();
  module.hot.dispose(function () {
    Template.__pendingReplacement.push("people");
    Template._applyHmrChanges("people");
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"world.html":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// import/ui/world.html                                                                                               //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
module.link("./template.world.js", { "*": "*+" });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"template.world.js":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// import/ui/template.world.js                                                                                        //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //

Template._migrateTemplate(
  "world",
  new Template("Template.world", (function() {
  var view = this;
  return [ HTML.MAIN("\n        ", Blaze._TemplateWith(function() {
    return {
      template: Spacebars.call(view.lookup("main"))
    };
  }, function() {
    return Spacebars.include(function() {
      return Spacebars.call(Template.__dynamic);
    });
  }), "\n    "), HTML.Raw('\n    <div id="chartdiv"></div>') ];
})),
);
if (typeof module === "object" && module.hot) {
  module.hot.accept();
  module.hot.dispose(function () {
    Template.__pendingReplacement.push("world");
    Template._applyHmrChanges("world");
  });
}

Template._migrateTemplate(
  "showList",
  new Template("Template.showList", (function() {
  var view = this;
  return HTML.DIV({
    class: "row"
  }, HTML.Raw("\n    <h2> List of Users</h2>\n        "), HTML.OL("\n            ", Spacebars.include(view.lookupTemplate("userinfo")), HTML.Raw("\n            <!--{{#each userinfos}}\n                {{> userinfo}}\n            {{/each}}-->\n        ")), "\n    ");
})),
);
if (typeof module === "object" && module.hot) {
  module.hot.accept();
  module.hot.dispose(function () {
    Template.__pendingReplacement.push("showList");
    Template._applyHmrChanges("showList");
  });
}

Template._migrateTemplate(
  "userinfo",
  new Template("Template.userinfo", (function() {
  var view = this;
  return [ Blaze._TemplateWith(function() {
    return {
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "Users")),
      class: Spacebars.call("table table-striped table-bordered table-condensed")
    };
  }, function() {
    return Spacebars.include(view.lookupTemplate("tabular"));
  }), HTML.Raw('\n    <!-- <div class="showL">\n        <li>\n            {{#if editing}}\n                <form class="edit-user" id="edituser">\n                        <input type="text" name="title" required="required" placeholder="{{title}}" />\n                    <div>Device Type</div>\n                    <select name="device" required="required">\n                        <option value="" disabled selected>Previous: {{device}}</option>\n                        <option value="router">Router</option>\n                        <option value="switch">Switch</option>\n                        <option value="hub">Hub</option>\n                    </select>\n                    <div>Latitude: {{latitude}}</div>\n                    <div>Longitude: {{longitude}}</div>\n                </form>\n                <button type="submit" form="edituser" value="Submit">Done</button>\n                <button class="cancel">&times; Cancel</button>\n            {{else}}\n                <ul>{{title}}</ul>\n                <ul>Device Type: {{device}}</ul>\n                <ul>Latitude: {{latitude}}</ul>\n                <ul>Longitude: {{longitude}}</ul>\n                <button class="delete">&cross; delete</button>\n                <button class="toggle-edit">&#9997; edit</button>\n            {{/if}}\n        </li>\n    </div>-->') ];
})),
);
if (typeof module === "object" && module.hot) {
  module.hot.accept();
  module.hot.dispose(function () {
    Template.__pendingReplacement.push("userinfo");
    Template._applyHmrChanges("userinfo");
  });
}

Template._migrateTemplate(
  "userform",
  new Template("Template.userform", (function() {
  var view = this;
  return [ HTML.Raw('<!--<div class="form">-->\n    '), HTML.DIV({
    class: "addR",
    style: function() {
      return [ "top: ", Spacebars.mustache(Spacebars.dot(view.lookup("position"), "Y")), "px; left: ", Spacebars.mustache(Spacebars.dot(view.lookup("position"), "X")), "px;" ];
    }
  }, "\n        ", HTML.FORM({
    class: "new-user",
    id: "newuser"
  }, HTML.Raw('\n            <div>Add new User</div>\n            <input type="text" name="title" placeholder="Title/Name">\n            <div>Choose device type</div>\n            <select name="device">\n                <option value="router">Router</option>\n                <option value="switch">Switch</option>\n                <option value="hub">Hub</option>\n            </select>\n            '), HTML.DIV("Latitude: ", Blaze.View("lookup:latitude", function() {
    return Spacebars.mustache(view.lookup("latitude"));
  })), "\n            ", HTML.DIV("Longitude: ", Blaze.View("lookup:longitude", function() {
    return Spacebars.mustache(view.lookup("longitude"));
  })), "\n        "), HTML.Raw('\n        <button type="submit" form="newuser" value="Submit">Add user</button>\n        <button class="delete">&times; Cancel</button>\n    ')) ];
})),
);
if (typeof module === "object" && module.hot) {
  module.hot.accept();
  module.hot.dispose(function () {
    Template.__pendingReplacement.push("userform");
    Template._applyHmrChanges("userform");
  });
}

Template._migrateTemplate(
  "userdetail",
  new Template("Template.userdetail", (function() {
  var view = this;
  return [ HTML.Raw('<!--<div class="form">-->\n        '), HTML.DIV({
    class: "addR",
    style: function() {
      return [ "top: ", Spacebars.mustache(Spacebars.dot(view.lookup("position"), "Y")), "px; left: ", Spacebars.mustache(Spacebars.dot(view.lookup("position"), "X")), "px;" ];
    }
  }, "\n            ", HTML.H1(Blaze.View("lookup:title", function() {
    return Spacebars.mustache(view.lookup("title"));
  }), HTML.Raw(' <button class="collapse">&times;</button>')), "\n            ", HTML.DIV("Device type: ", Blaze.View("lookup:device", function() {
    return Spacebars.mustache(view.lookup("device"));
  })), "\n            ", HTML.DIV("Latitude: ", Blaze.View("lookup:latitude", function() {
    return Spacebars.mustache(view.lookup("latitude"));
  })), "\n            ", HTML.DIV("Longitude: ", Blaze.View("lookup:longitude", function() {
    return Spacebars.mustache(view.lookup("longitude"));
  })), "\n    ") ];
})),
);
if (typeof module === "object" && module.hot) {
  module.hot.accept();
  module.hot.dispose(function () {
    Template.__pendingReplacement.push("userdetail");
    Template._applyHmrChanges("userdetail");
  });
}

Template._migrateTemplate(
  "addRoute",
  new Template("Template.addRoute", (function() {
  var view = this;
  return HTML.DIV({
    class: "addR",
    style: function() {
      return [ "top: ", Spacebars.mustache(Spacebars.dot(view.lookup("position"), "Y")), "px; left: ", Spacebars.mustache(Spacebars.dot(view.lookup("position"), "X")), "px;" ];
    }
  }, HTML.Raw('\n        <div> Select a destination <button class="cancelAdd">&times;</button></div>\n        '), HTML.SELECT({
    class: "form-control",
    required: "",
    style: "width: 200px;",
    id: "routeDest",
    name: "routeDest"
  }, HTML.Raw('\n            <option value="" disabled="" selected="">Please Select</option>\n            '), Blaze.Each(function() {
    return Spacebars.call(view.lookup("dropDown"));
  }, function() {
    return [ "\n                ", HTML.OPTION({
      value: function() {
        return Spacebars.mustache(Spacebars.dot(view.lookup("."), "_id"));
      }
    }, Blaze.View("lookup:..title", function() {
      return Spacebars.mustache(Spacebars.dot(view.lookup("."), "title"));
    })), "\n            " ];
  }), "\n        "), "\n    ");
})),
);
if (typeof module === "object" && module.hot) {
  module.hot.accept();
  module.hot.dispose(function () {
    Template.__pendingReplacement.push("addRoute");
    Template._applyHmrChanges("addRoute");
  });
}

Template._migrateTemplate(
  "removeRoute",
  new Template("Template.removeRoute", (function() {
  var view = this;
  return HTML.DIV({
    class: "addR",
    style: function() {
      return [ "top: ", Spacebars.mustache(Spacebars.dot(view.lookup("position"), "Y")), "px; left: ", Spacebars.mustache(Spacebars.dot(view.lookup("position"), "X")), "px;" ];
    }
  }, "\n        ", HTML.DIV(" Sure to disconnect route: ", HTML.B(Blaze.View("lookup:name", function() {
    return Spacebars.mustache(view.lookup("name"));
  }))), HTML.Raw('\n        <button class="Remove">Yes</button>\n        <button class="cancelRemove">&times; Cancel</button>\n    '));
})),
);
if (typeof module === "object" && module.hot) {
  module.hot.accept();
  module.hot.dispose(function () {
    Template.__pendingReplacement.push("removeRoute");
    Template._applyHmrChanges("removeRoute");
  });
}

Template._migrateTemplate(
  "unknownRoute",
  new Template("Template.unknownRoute", (function() {
  var view = this;
  return HTML.Raw('<div style="background-color: #f3f0ff; border-radius: 10px; text-align: center; ">\n        <h1> Route Not found 404</h1>\n        <h2> You can go back to home!</h2>\n    </div>');
})),
);
if (typeof module === "object" && module.hot) {
  module.hot.accept();
  module.hot.dispose(function () {
    Template.__pendingReplacement.push("unknownRoute");
    Template._applyHmrChanges("unknownRoute");
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"body.js":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// import/ui/body.js                                                                                                  //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
!function (module1) {
  let Template;
  module1.link("meteor/templating", {
    Template(v) {
      Template = v;
    }

  }, 0);
  let Meteor;
  module1.link("meteor/meteor", {
    Meteor(v) {
      Meteor = v;
    }

  }, 1);
  module1.link("./world.js");
  module1.link("./body.html");

  ___INIT_METEOR_FAST_REFRESH(module);

  // Subscribe to the database!
  Template.body.created = function () {
    Meteor.subscribe('userinfo');
    Meteor.subscribe('neighbourInfo');
  };
}.call(this, module);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"search.js":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// import/ui/search.js                                                                                                //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
!function (module1) {
  module1.link("./search.html");
  let Userinfo;
  module1.link("../api/userinfo.js", {
    Userinfo(v) {
      Userinfo = v;
    }

  }, 0);
  let Template;
  module1.link("meteor/templating", {
    Template(v) {
      Template = v;
    }

  }, 1);
  let ReactiveVar;
  module1.link("meteor/reactive-var", {
    ReactiveVar(v) {
      ReactiveVar = v;
    }

  }, 2);

  ___INIT_METEOR_FAST_REFRESH(module);

  // Search event on changing text
  Template.search.events({
    'keyup input.search-query': function (evt) {
      Session.set("search-query", evt.currentTarget.value);
    }
  }); // <------------------------>
  // This is the table template, which takes input from search bar
  // And compares with db- entries

  Template.people.helpers({
    searchResults() {
      var keyword = Session.get("search-query");
      var query = new RegExp(keyword, 'i');
      var results;

      if (query.test("/^(?!\s*$).+/") === false) {
        results = Userinfo.find({
          $or: [{
            'title': query
          }, {
            'device': query
          }, //{'longitude': query},
          //{'latitude': query},
          {
            'country': query
          }]
        });
      }

      return {
        results: results
      };
    }

  }); // Seach Helper

  /*
  Template.searchHelper.onCreated(function onCreated(){
    this.hoverstate = new ReactiveVar(false);
  })
  
  Template.searchHelper.helpers({
    showHover(){
      return Template.instance().hoverstate.get();
    }
  })
  
  Template.searchHelper.events({
    'mouseenter .hoverMethod'(event, instance){
      instance.hoverstate.set(true);
    },
    'mouseleave .hoverMethod'(event, instance){
      instance.hoverstate.set(false);
    }
  })*/
}.call(this, module);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"world.js":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// import/ui/world.js                                                                                                 //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
!function (module1) {
  let am4core;
  module1.link("@amcharts/amcharts4/core", {
    "*"(v) {
      am4core = v;
    }

  }, 0);
  let am4maps;
  module1.link("@amcharts/amcharts4/maps", {
    "*"(v) {
      am4maps = v;
    }

  }, 1);
  let am4themes_animated;
  module1.link("@amcharts/amcharts4/themes/animated", {
    default(v) {
      am4themes_animated = v;
    }

  }, 2);
  let am4geodata_worldIndiaLow;
  module1.link("@amcharts/amcharts4-geodata/worldIndiaLow", {
    default(v) {
      am4geodata_worldIndiaLow = v;
    }

  }, 3);
  let am4geodata_indiaLow;
  module1.link("@amcharts/amcharts4-geodata/india2020Low", {
    default(v) {
      am4geodata_indiaLow = v;
    }

  }, 4);
  let Template;
  module1.link("meteor/templating", {
    Template(v) {
      Template = v;
    }

  }, 5);
  let Userinfo;
  module1.link("../api/userinfo.js", {
    Userinfo(v) {
      Userinfo = v;
    }

  }, 6);
  let NeighbourInfo;
  module1.link("../api/userinfo.js", {
    NeighbourInfo(v) {
      NeighbourInfo = v;
    }

  }, 7);
  module1.link("./world.html");
  let crg;
  module1.link("country-reverse-geocoding", {
    "*"(v) {
      crg = v;
    }

  }, 8);
  module1.link("./search.js");
  let Meteor;
  module1.link("meteor/meteor", {
    Meteor(v) {
      Meteor = v;
    }

  }, 9);
  let Session;
  module1.link("meteor/session", {
    Session(v) {
      Session = v;
    }

  }, 10);
  let round;
  module1.link("@amcharts/amcharts4/.internal/core/utils/Math", {
    round(v) {
      round = v;
    }

  }, 11);
  let Tracker;
  module1.link("meteor/tracker", {
    Tracker(v) {
      Tracker = v;
    }

  }, 12);

  ___INIT_METEOR_FAST_REFRESH(module);

  this.userinfo = Userinfo;
  this.neighbour = NeighbourInfo; // World Template 

  am4core.useTheme(am4themes_animated);
  Template.world.onRendered(function () {
    var instance = this;
    instance.autorun(function () {
      // Auto dispose
      am4core.options.autoDispose = true; // Creating chart instance

      var chart = am4core.create("chartdiv", am4maps.MapChart); //toggle button to switch india/world

      var button = chart.createChild(am4core.SwitchButton);
      button.align = "right";
      button.marginTop = 40;
      button.marginRight = 40;
      button.valign = "top";
      button.leftLabel.text = "India";
      button.rightLabel.text = "World";
      button.leftLabel.fill = am4core.color("#fff");
      button.rightLabel.fill = am4core.color("#fff");
      button.isActive = false;
      button.events.on("toggled", function () {
        FlowRouter.go("/");

        if (button.isActive) {
          // goHome() resets the zoom
          chart.goHome(); // Adding geodata based on the map we want
          // Since geodata changes, autorun() re-renders it

          chart.geodata = am4geodata_worldIndiaLow;
        } else {
          chart.goHome();
          chart.geodata = am4geodata_indiaLow;
        }
      }); // getting DB data

      const mapdata = Userinfo.find({});
      var jsond = [];
      var lined = [];
      var gg = []; // Simplify data to object format

      mapdata.forEach(post => {
        jsond.push({
          title: post.title,
          longitude: parseFloat(post.longitude),
          latitude: parseFloat(post.latitude)
        });
        var neighbourdata = NeighbourInfo.find({
          userID: post._id
        }).fetch()[0].neighbour;
        neighbourdata.forEach(item => {
          let a = Userinfo.find({
            _id: item
          }).fetch();

          if (post._id > a[0]._id) {
            var _id = post._id + a[0]._id;

            var name = post.title + " to " + a[0].title;
          } else {
            var _id = a[0]._id + post._id;

            var name = a[0].title + " to " + post.title;
          }

          lined.push({
            "name": name,
            "_id": _id,
            "multiGeoLine": [[{
              "latitude": parseFloat(round(post.latitude, 6)),
              "longitude": parseFloat(round(post.longitude, 6))
            }, {
              "latitude": parseFloat(round(a[0].latitude, 6)),
              "longitude": parseFloat(round(a[0].longitude, 6))
            }]]
          });
        });
      }); // Set map definition

      chart.geodata = am4geodata_indiaLow; // Set projection

      chart.projection = new am4maps.projections.Miller(); // Disable pan

      chart.seriesContainer.draggable = false;
      chart.seriesContainer.resizable = false; // Disbale right click on chart

      chart.contextMenuDisabled = true; // Disable Default right click **

      chart.seriesContainer.contextMenuDisabled = true; // configuring zoom
      // set to one in dev, will change

      chart.maxZoomLevel = 64; // Add zoom control

      chart.zoomControl = new am4maps.ZoomControl(); // Create map polygon series

      var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries()); // Exclude Antartica

      polygonSeries.exclude = ["AQ"]; // Make map load polygon (like country names) data from GeoJSON

      polygonSeries.useGeodata = true; // Configure series

      var polygonTemplate = polygonSeries.mapPolygons.template;
      polygonTemplate.tooltipText = "{name}";
      polygonTemplate.fill = am4core.color("#47c78a"); // events on polygonTemplate

      polygonTemplate.events.on("hit", function (ev) {
        var data = chart.svgPointToGeo(ev.svgPoint); // Important: Was adding a logic to verify that addUser doesn't add anyexisting point,
        // but by logic existing points are a layer above and they can't be clicked polygon Series!!

        FlowRouter.go("/addUser?latitude=" + data.latitude.toString() + "&longitude=" + data.longitude.toString() + "&X=" + ev.event.layerX + "&Y=" + ev.event.layerY);
      }); // Create hover state and set alternative fill color

      var hs = polygonTemplate.states.create("hover");
      hs.properties.fill = chart.colors.getIndex(0); // Add image series

      var imageSeries = chart.series.push(new am4maps.MapImageSeries());
      imageSeries.mapImages.template.propertyFields.longitude = "longitude";
      imageSeries.mapImages.template.propertyFields.latitude = "latitude"; // Now creating markers on the graph
      // configuring the graph series

      var imageSeries = chart.series.push(new am4maps.MapImageSeries());
      imageSeries.mapImages.template.propertyFields.longitude = "longitude";
      imageSeries.mapImages.template.propertyFields.latitude = "latitude";
      let hover = imageSeries.mapImages.template.states.create("highlight");
      hover.properties.fill = am4core.color("#69975F");
      imageSeries.mapImages.template.tooltipText = "{title}";
      imageSeries.mapImages.template.tooltipPosition = "fixed";
      imageSeries.events.on("over", function (ev) {
        ev.target.mapImages.each(function (polygon) {//console.log(polygon)
        });
      }); // Marker events

      imageSeries.mapImages.template.events.on("hit", function (ev) {
        var data = ev.target._dataItem.dataContext;
        FlowRouter.go("/viewUser?latitude=" + data.latitude.toString() + "&longitude=" + data.longitude.toString() + "&X=" + ev.event.layerX + "&Y=" + ev.event.layerY);
      });
      imageSeries.mapImages.template.events.on("rightclick", function (ev) {
        var data = ev.target._dataItem.dataContext;
        FlowRouter.go("/addRoute?latitude=" + data.latitude.toString() + "&longitude=" + data.longitude.toString() + "&X=" + ev.event.layerX + "&Y=" + ev.event.layerY);
      }); // Configure marker and its animation

      var circle = imageSeries.mapImages.template.createChild(am4core.Circle);
      circle.radius = 3;
      circle.propertyFields.fill = "color";
      circle.nonScaling = true;
      var circle2 = imageSeries.mapImages.template.createChild(am4core.Circle);
      circle2.radius = 3;
      circle2.propertyFields.fill = "color";
      circle2.events.on("inited", function (event) {
        animateBullet(event.target);
      }); // Function to render markers

      function animateBullet(circle) {
        var animation = circle.animate([{
          property: "scale",
          from: 1 / chart.zoomLevel,
          to: 5 / chart.zoomLevel
        }, {
          property: "opacity",
          from: 1,
          to: 0
        }], 1000, am4core.ease.circleOut);
        animation.events.on("animationended", function (event) {
          animateBullet(event.target.object);
        });
      } //var colorSet = new am4core.ColorSet();


      imageSeries.data = jsond; // Creating line series

      let lineSeries = chart.series.push(new am4maps.MapLineSeries());
      lineSeries.data = lined;
      lineSeries.mapLines.template.interactionsEnabled = true;
      lineSeries.mapLines.template.line.stroke = am4core.color("#f3fff0");
      lineSeries.mapLines.template.line.strokeOpacity = 0.75;
      lineSeries.mapLines.template.line.strokeWidth = 2; //lineSeries.mapLines.template.line.strokeDasharray = "3,3";

      lineSeries.mapLines.template.shortestDistance = false; // Remove Route 

      lineSeries.events.on("rightclick", function (ev) {
        var data = ev.target._dataItem.component._tooltip._dataItem._dataContext;
        FlowRouter.go("/removeRoute?route=" + data._id + "&name=" + data.name + "&X=" + ev.event.layerX + "&Y=" + ev.event.layerY);
      });
      lineSeries.mapLines.template.line.tooltipText = "{name}";
      lineSeries.zIndex = 10;
    });
  }); // Showlist
  // Used to display all the users

  Template.showList.helpers({
    userinfos() {
      return Userinfo.find({}, {
        sort: {
          createdAt: -1
        }
      });
    }

  });
  Template.userinfo.onCreated(function () {
    if (this.data.editing) {
      Meteor.call('user.setEdit', this.data._id, false);
    }
  }); // Userinfo 
  // Helper to showlist, to serve individual users
  // Also helpers to quick edit

  Template.userinfo.events({
    'click .delete'() {
      Meteor.call('user.remove', this._id);
      Meteor.call('neighbour.remove', this._id);
    },

    'click .toggle-edit'() {
      Meteor.call('user.setEdit', this._id, true);
    },

    'click .cancel'() {
      Meteor.call('user.setEdit', this._id, false);
    },

    'submit .edit-user'(event) {
      event.preventDefault();
      const target = event.target;
      const title = target.title.value;
      const devi = target.device.value;
      Meteor.call('user.update', this._id, title, devi);
      Meteor.call('user.setEdit', this._id, false);
      FlowRouter.go("/showList");
    }

  }); /// UserForm
  // To add a new user

  Template.userform.onRendered(function () {// Removed a lot of code!
  });
  Template.userform.helpers({
    longitude() {
      return FlowRouter.getQueryParam("longitude");
    },

    latitude() {
      return FlowRouter.getQueryParam("latitude");
    },

    position() {
      return {
        X: FlowRouter.getQueryParam("X"),
        Y: FlowRouter.getQueryParam("Y")
      };
    }

  }); // A lot og brut-force, will optimize

  Template.userform.events({
    'submit .new-user'(event) {
      event.preventDefault();
      const target = event.target;
      const title = target.title.value;
      const devi = target.device.value;
      const longitude = FlowRouter.getQueryParam("longitude");
      const latitude = FlowRouter.getQueryParam("latitude");
      const country = "india";
      Meteor.call('user.insert', title, devi, latitude, longitude, country);
      target.title.value = '';
      const dbsear = Userinfo.find({
        "title": title
      }).fetch()[0];
      Meteor.call('neighbour.insert', dbsear._id, title, latitude, longitude);
      FlowRouter.go("/");
    },

    'click .delete'() {
      FlowRouter.go("/");
    }

  }); /// User Details
  // This is used to show details or ViewUSer

  Template.userdetail.onRendered(function () {
    var instance = this;
    instance.autorun(function () {
      var mapdata = Userinfo.find({
        latitude: FlowRouter.getQueryParam("latitude"),
        longitude: FlowRouter.getQueryParam("longitude")
      }).fetch();
      Session.set("sData", mapdata[0]);
    });
  });
  Template.userdetail.helpers({
    title() {
      return Session.get("sData").title;
    },

    device() {
      return Session.get("sData").device;
    },

    latitude() {
      return Session.get("sData").latitude;
    },

    longitude() {
      return Session.get("sData").longitude;
    },

    position() {
      return {
        X: FlowRouter.getQueryParam("X"),
        Y: FlowRouter.getQueryParam("Y")
      };
    }

  }); // To removew the view after we are done.

  Template.userdetail.events({
    'click .collapse'() {
      FlowRouter.go("/");
    }

  }); // Add route Template

  Template.addRoute.helpers({
    dropDown() {
      var data = Userinfo.find({});
      var point = Userinfo.find({
        latitude: FlowRouter.getQueryParam("latitude"),
        longitude: FlowRouter.getQueryParam("longitude")
      }).fetch();
      var neData = [];
      data.forEach(post => {
        if (post.longitude === point[0].longitude && post.latitude === point[0].latitude) {
          Session.set("addData", post._id);
        } else {
          if (NeighbourInfo.find({
            $and: [{
              userID: point[0]._id
            }, {
              neighbour: post._id
            }]
          }).count()) {} else {
            neData.push(post);
          }
        }
      });
      return neData;
    },

    position() {
      return {
        X: FlowRouter.getQueryParam("X"),
        Y: FlowRouter.getQueryParam("Y")
      };
    }

  });
  Template.addRoute.events({
    'click #routeDest'(event) {
      if (event.target.value) {
        // Verrification
        //Method 1:
        // if(NeighbourInfo.find({userID: userID}).fetch()[0].neighbour.includes(neighbourID)){
        // Method 2:
        if (NeighbourInfo.find({
          $and: [{
            userID: Session.get("addData")
          }, {
            neighbour: event.target.value
          }]
        }).count()) {} else {
          Meteor.call('neighbour.add', Session.get("addData"), event.target.value);
          Meteor.call('neighbour.add', event.target.value, Session.get("addData"));
        }

        FlowRouter.go("/");
      }
    },

    'contextmenu': function (e) {
      return false;
    },

    'click .cancelAdd'() {
      FlowRouter.go("/");
    }

  }); // Remove Route template

  Template.removeRoute.onCreated(function () {
    this.autorun(function () {});
  });
  Template.removeRoute.events({
    'click .Remove'() {
      var idstring = FlowRouter.getQueryParam("route");
      var id1 = idstring.slice(0, 17);
      var id2 = idstring.slice(17, 34);
      Meteor.call('neighbour.removeNeighbour', id1, id2);
      FlowRouter.go("/");
    },

    'click .cancelRemove'() {
      FlowRouter.go("/");
    }

  });
  Template.removeRoute.helpers({
    name() {
      return FlowRouter.getQueryParam("name");
    },

    position() {
      return {
        X: FlowRouter.getQueryParam("X"),
        Y: FlowRouter.getQueryParam("Y")
      };
    }

  });
}.call(this, module);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"api":{"userinfo.js":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// import/api/userinfo.js                                                                                             //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
!function (module1) {
  module1.export({
    Userinfo: () => Userinfo,
    NeighbourInfo: () => NeighbourInfo
  });
  let Mongo;
  module1.link("meteor/mongo", {
    Mongo(v) {
      Mongo = v;
    }

  }, 0);
  let Meteor;
  module1.link("meteor/meteor", {
    Meteor(v) {
      Meteor = v;
    }

  }, 1);
  let check;
  module1.link("meteor/check", {
    check(v) {
      check = v;
    }

  }, 2);

  ___INIT_METEOR_FAST_REFRESH(module);

  const Userinfo = new Mongo.Collection('userinfo');
  const NeighbourInfo = new Mongo.Collection('neighbourInfo');

  if (Meteor.isServer) {
    Meteor.publish('userinfo', function userinfoPublication() {
      return Userinfo.find({});
    });
    Meteor.publish('neighbourInfo', function () {
      return NeighbourInfo.find({});
    });
  }

  Meteor.methods({
    'user.insert'(title, device, latitude, longitude, country) {
      check(title, String);
      check(device, String);
      check(latitude, String);
      check(longitude, String);
      check(country, String);
      Userinfo.insert({
        title,
        device,
        latitude,
        longitude,
        country,
        createdAt: new Date()
      });
    },

    'user.remove'(userID) {
      check(userID, String);
      Userinfo.remove(userID);
    },

    'user.setEdit'(userID, setEdit) {
      check(userID, String);
      check(setEdit, Boolean);
      Userinfo.update(userID, {
        $set: {
          editing: setEdit
        }
      });
    },

    'user.update'(userID, title, devi) {
      check(userID, String);
      check(title, String);
      check(devi, String);
      Userinfo.update(userID, {
        $set: {
          title: title,
          device: devi
        }
      });
    },

    'user.addNeighb'(userID, nextUserID) {
      Userinfo.update(userID, {
        $push: {
          neighbour: nextUserID
        }
      });
    },

    'neighbour.insert'(userID, title, latitude, longitude) {
      check(userID, String);
      check(title, String);
      check(longitude, String);
      check(latitude, String);
      NeighbourInfo.insert({
        userID,
        title,
        latitude,
        longitude,
        neighbour: [],
        createdAt: new Date()
      });
    },

    'neighbour.add'(userID, neighbourID) {
      check(userID, String);
      check(neighbourID, String); //Method 1:
      // if(NeighbourInfo.find({userID: userID}).fetch()[0].neighbour.includes(neighbourID)){
      // Method 2:

      NeighbourInfo.update({
        userID: userID
      }, {
        $push: {
          neighbour: neighbourID
        }
      });
    },

    'neighbour.removeNeighbour'(user1, user2) {
      NeighbourInfo.update({
        "userID": user1
      }, {
        $pull: {
          "neighbour": user2
        }
      });
      NeighbourInfo.update({
        "userID": user2
      }, {
        $pull: {
          "neighbour": user1
        }
      });
    },

    'neighbour.remove'(userID) {
      check(userID, String);
      NeighbourInfo.update({
        "neighbour": userID
      }, {
        $pull: {
          "neighbour": userID
        }
      }, {
        multi: true
      });
      NeighbourInfo.remove({
        userID: userID
      });
    }

  });
}.call(this, module);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},"root":{"lib":{"tables.js":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// root/lib/tables.js                                                                                                 //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
!function (module1) {
  let Tabular;
  module1.link("meteor/aldeed:tabular", {
    default(v) {
      Tabular = v;
    }

  }, 0);
  let Userinfo;
  module1.link("../../import/api/userinfo.js", {
    Userinfo(v) {
      Userinfo = v;
    }

  }, 1);
  let moment;
  module1.link("moment", {
    default(v) {
      moment = v;
    }

  }, 2);

  ___INIT_METEOR_FAST_REFRESH(module);

  TabularTables = {};
  TabularTables.Users = new Tabular.Table({
    name: "Users",
    collection: Userinfo,
    pub: "Userinfo",
    columns: [{
      data: "title",
      title: "Title"
    }, {
      data: "device",
      title: "Device"
    }, {
      data: "country",
      title: "Country"
    }, {
      data: "createdAt",
      title: "CreatedAt",
      render: function (val) {
        return moment(val).format('DD/MM/YYYY H:mm');
      }
    }],
    initComplete: function () {
      $('.dataTables_empty').html('processing');
    }
  });
  module1.exportDefault(TabularTables);
}.call(this, module);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},"lib":{"routes.js":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// lib/routes.js                                                                                                      //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
!function (module1) {
  ___INIT_METEOR_FAST_REFRESH(module);

  FlowRouter.route('/', {
    name: 'default',

    action() {
      BlazeLayout.render("world");
    }

  });
  FlowRouter.route('/search', {
    name: 'search',

    action() {
      BlazeLayout.render("world", {
        main: "search"
      });
    }

  });
  FlowRouter.route('/showList', {
    name: 'list',

    action() {
      BlazeLayout.render("world", {
        main: "showList"
      });
    }

  });
  FlowRouter.route('/addRoute', {
    name: 'addRoute',
    action: function (params, queryParams) {
      BlazeLayout.render("world", {
        main: "addRoute"
      });
    }
  });
  FlowRouter.route('/removeRoute', {
    name: 'addRoute',
    action: function (params, queryParams) {
      BlazeLayout.render("world", {
        main: "removeRoute"
      });
    }
  }); // Initial build with string param

  /*FlowRouter.route("/addUser/:param",{
      name: 'addwithParams',
      action (){
          BlazeLayout.render("world", {main:'userform'});
      }
  });*/

  FlowRouter.route("/addUser", {
    name: 'addwithParams',
    action: function (params, queryParams) {
      BlazeLayout.render("world", {
        main: 'userform'
      });
    }
  });
  FlowRouter.route("/viewUser", {
    name: 'viewwithParams',
    action: function (params, queryParams) {
      BlazeLayout.render("world", {
        main: 'userdetail'
      });
    }
  });
  FlowRouter.route('/*', {
    name: 'unknownRoute',

    action() {
      BlazeLayout.render("unknownRoute");
    }

  }); // Exposed FlowRouter to console for development! Will remove!s

  this.FlowRouter = FlowRouter;
}.call(this, module);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"client":{"main.js":function module(require,exports,module){

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                    //
// client/main.js                                                                                                     //
//                                                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                      //
!function (module1) {
  module1.link("../import/ui/body.js");

  ___INIT_METEOR_FAST_REFRESH(module);
}.call(this, module);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},{
  "extensions": [
    ".js",
    ".json",
    ".html",
    ".ts",
    ".css",
    ".mjs"
  ]
});

require("/import/ui/template.body.js");
require("/import/ui/template.search.js");
require("/import/ui/template.world.js");
require("/root/lib/tables.js");
require("/lib/routes.js");
require("/import/api/userinfo.js");
require("/import/ui/body.js");
require("/import/ui/search.js");
require("/import/ui/world.js");
require("/client/main.js");