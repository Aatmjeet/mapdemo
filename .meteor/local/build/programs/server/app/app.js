var require = meteorInstall({"root":{"lib":{"tables.js":function module(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                          //
// root/lib/tables.js                                                                       //
//                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////
                                                                                            //
let Tabular;
module.link("meteor/aldeed:tabular", {
  default(v) {
    Tabular = v;
  }

}, 0);
let Userinfo;
module.link("../../import/api/userinfo.js", {
  Userinfo(v) {
    Userinfo = v;
  }

}, 1);
let moment;
module.link("moment", {
  default(v) {
    moment = v;
  }

}, 2);
TabularTables = {};
TabularTables.Users = new Tabular.Table({
  name: "Users",
  collection: Userinfo,
  pub: "Userinfo",
  columns: [{
    data: "title",
    title: "Title"
  }, {
    data: "device",
    title: "Device"
  }, {
    data: "country",
    title: "Country"
  }, {
    data: "createdAt",
    title: "CreatedAt",
    render: function (val) {
      return moment(val).format('DD/MM/YYYY H:mm');
    }
  }],
  initComplete: function () {
    $('.dataTables_empty').html('processing');
  }
});
module.exportDefault(TabularTables);
//////////////////////////////////////////////////////////////////////////////////////////////

}}},"import":{"api":{"userinfo.js":function module(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                          //
// import/api/userinfo.js                                                                   //
//                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////
                                                                                            //
module.export({
  Userinfo: () => Userinfo,
  NeighbourInfo: () => NeighbourInfo
});
let Mongo;
module.link("meteor/mongo", {
  Mongo(v) {
    Mongo = v;
  }

}, 0);
let Meteor;
module.link("meteor/meteor", {
  Meteor(v) {
    Meteor = v;
  }

}, 1);
let check;
module.link("meteor/check", {
  check(v) {
    check = v;
  }

}, 2);
const Userinfo = new Mongo.Collection('userinfo');
const NeighbourInfo = new Mongo.Collection('neighbourInfo');

if (Meteor.isServer) {
  Meteor.publish('userinfo', function userinfoPublication() {
    return Userinfo.find({});
  });
  Meteor.publish('neighbourInfo', function () {
    return NeighbourInfo.find({});
  });
}

Meteor.methods({
  'user.insert'(title, device, latitude, longitude, country) {
    check(title, String);
    check(device, String);
    check(latitude, String);
    check(longitude, String);
    check(country, String);
    Userinfo.insert({
      title,
      device,
      latitude,
      longitude,
      country,
      createdAt: new Date()
    });
  },

  'user.remove'(userID) {
    check(userID, String);
    Userinfo.remove(userID);
  },

  'user.setEdit'(userID, setEdit) {
    check(userID, String);
    check(setEdit, Boolean);
    Userinfo.update(userID, {
      $set: {
        editing: setEdit
      }
    });
  },

  'user.update'(userID, title, devi) {
    check(userID, String);
    check(title, String);
    check(devi, String);
    Userinfo.update(userID, {
      $set: {
        title: title,
        device: devi
      }
    });
  },

  'user.addNeighb'(userID, nextUserID) {
    Userinfo.update(userID, {
      $push: {
        neighbour: nextUserID
      }
    });
  },

  'neighbour.insert'(userID, title, latitude, longitude) {
    check(userID, String);
    check(title, String);
    check(longitude, String);
    check(latitude, String);
    NeighbourInfo.insert({
      userID,
      title,
      latitude,
      longitude,
      neighbour: [],
      createdAt: new Date()
    });
  },

  'neighbour.add'(userID, neighbourID) {
    check(userID, String);
    check(neighbourID, String); //Method 1:
    // if(NeighbourInfo.find({userID: userID}).fetch()[0].neighbour.includes(neighbourID)){
    // Method 2:

    NeighbourInfo.update({
      userID: userID
    }, {
      $push: {
        neighbour: neighbourID
      }
    });
  },

  'neighbour.removeNeighbour'(user1, user2) {
    NeighbourInfo.update({
      "userID": user1
    }, {
      $pull: {
        "neighbour": user2
      }
    });
    NeighbourInfo.update({
      "userID": user2
    }, {
      $pull: {
        "neighbour": user1
      }
    });
  },

  'neighbour.remove'(userID) {
    check(userID, String);
    NeighbourInfo.update({
      "neighbour": userID
    }, {
      $pull: {
        "neighbour": userID
      }
    }, {
      multi: true
    });
    NeighbourInfo.remove({
      userID: userID
    });
  }

});
//////////////////////////////////////////////////////////////////////////////////////////////

}}},"server":{"main.js":function module(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                          //
// server/main.js                                                                           //
//                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////
                                                                                            //
let Meteor;
module.link("meteor/meteor", {
  Meteor(v) {
    Meteor = v;
  }

}, 0);
module.link("../import/api/userinfo.js");
module.link("../root/lib/tables.js");
Meteor.startup(() => {});
//////////////////////////////////////////////////////////////////////////////////////////////

}}},{
  "extensions": [
    ".js",
    ".json",
    ".ts",
    ".mjs"
  ]
});

var exports = require("/server/main.js");
//# sourceURL=meteor://💻app/app/app.js
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1ldGVvcjovL/CfkrthcHAvcm9vdC9saWIvdGFibGVzLmpzIiwibWV0ZW9yOi8v8J+Su2FwcC9pbXBvcnQvYXBpL3VzZXJpbmZvLmpzIiwibWV0ZW9yOi8v8J+Su2FwcC9zZXJ2ZXIvbWFpbi5qcyJdLCJuYW1lcyI6WyJUYWJ1bGFyIiwibW9kdWxlIiwibGluayIsImRlZmF1bHQiLCJ2IiwiVXNlcmluZm8iLCJtb21lbnQiLCJUYWJ1bGFyVGFibGVzIiwiVXNlcnMiLCJUYWJsZSIsIm5hbWUiLCJjb2xsZWN0aW9uIiwicHViIiwiY29sdW1ucyIsImRhdGEiLCJ0aXRsZSIsInJlbmRlciIsInZhbCIsImZvcm1hdCIsImluaXRDb21wbGV0ZSIsIiQiLCJodG1sIiwiZXhwb3J0RGVmYXVsdCIsImV4cG9ydCIsIk5laWdoYm91ckluZm8iLCJNb25nbyIsIk1ldGVvciIsImNoZWNrIiwiQ29sbGVjdGlvbiIsImlzU2VydmVyIiwicHVibGlzaCIsInVzZXJpbmZvUHVibGljYXRpb24iLCJmaW5kIiwibWV0aG9kcyIsImRldmljZSIsImxhdGl0dWRlIiwibG9uZ2l0dWRlIiwiY291bnRyeSIsIlN0cmluZyIsImluc2VydCIsImNyZWF0ZWRBdCIsIkRhdGUiLCJ1c2VySUQiLCJyZW1vdmUiLCJzZXRFZGl0IiwiQm9vbGVhbiIsInVwZGF0ZSIsIiRzZXQiLCJlZGl0aW5nIiwiZGV2aSIsIm5leHRVc2VySUQiLCIkcHVzaCIsIm5laWdoYm91ciIsIm5laWdoYm91cklEIiwidXNlcjEiLCJ1c2VyMiIsIiRwdWxsIiwibXVsdGkiLCJzdGFydHVwIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLElBQUlBLE9BQUo7QUFBWUMsTUFBTSxDQUFDQyxJQUFQLENBQVksdUJBQVosRUFBb0M7QUFBQ0MsU0FBTyxDQUFDQyxDQUFELEVBQUc7QUFBQ0osV0FBTyxHQUFDSSxDQUFSO0FBQVU7O0FBQXRCLENBQXBDLEVBQTRELENBQTVEO0FBQStELElBQUlDLFFBQUo7QUFBYUosTUFBTSxDQUFDQyxJQUFQLENBQVksOEJBQVosRUFBMkM7QUFBQ0csVUFBUSxDQUFDRCxDQUFELEVBQUc7QUFBQ0MsWUFBUSxHQUFDRCxDQUFUO0FBQVc7O0FBQXhCLENBQTNDLEVBQXFFLENBQXJFO0FBQXdFLElBQUlFLE1BQUo7QUFBV0wsTUFBTSxDQUFDQyxJQUFQLENBQVksUUFBWixFQUFxQjtBQUFDQyxTQUFPLENBQUNDLENBQUQsRUFBRztBQUFDRSxVQUFNLEdBQUNGLENBQVA7QUFBUzs7QUFBckIsQ0FBckIsRUFBNEMsQ0FBNUM7QUFJM0tHLGFBQWEsR0FBRyxFQUFoQjtBQUVBQSxhQUFhLENBQUNDLEtBQWQsR0FBc0IsSUFBSVIsT0FBTyxDQUFDUyxLQUFaLENBQWtCO0FBQ3BDQyxNQUFJLEVBQUUsT0FEOEI7QUFFcENDLFlBQVUsRUFBRU4sUUFGd0I7QUFHcENPLEtBQUcsRUFBRSxVQUgrQjtBQUlwQ0MsU0FBTyxFQUFFLENBQ047QUFDR0MsUUFBSSxFQUFDLE9BRFI7QUFFR0MsU0FBSyxFQUFDO0FBRlQsR0FETSxFQUtOO0FBQ0dELFFBQUksRUFBQyxRQURSO0FBRUdDLFNBQUssRUFBQztBQUZULEdBTE0sRUFTTjtBQUNHRCxRQUFJLEVBQUMsU0FEUjtBQUVHQyxTQUFLLEVBQUM7QUFGVCxHQVRNLEVBYU47QUFDR0QsUUFBSSxFQUFDLFdBRFI7QUFFR0MsU0FBSyxFQUFDLFdBRlQ7QUFHR0MsVUFBTSxFQUFDLFVBQVVDLEdBQVYsRUFBZTtBQUNsQixhQUFPWCxNQUFNLENBQUNXLEdBQUQsQ0FBTixDQUFZQyxNQUFaLENBQW1CLGlCQUFuQixDQUFQO0FBQ0g7QUFMSixHQWJNLENBSjJCO0FBeUJwQ0MsY0FBWSxFQUFFLFlBQVc7QUFDckJDLEtBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCQyxJQUF2QixDQUE0QixZQUE1QjtBQUNIO0FBM0JtQyxDQUFsQixDQUF0QjtBQU5BcEIsTUFBTSxDQUFDcUIsYUFBUCxDQW9DZWYsYUFwQ2YsRTs7Ozs7Ozs7Ozs7QUNBQU4sTUFBTSxDQUFDc0IsTUFBUCxDQUFjO0FBQUNsQixVQUFRLEVBQUMsTUFBSUEsUUFBZDtBQUF1Qm1CLGVBQWEsRUFBQyxNQUFJQTtBQUF6QyxDQUFkO0FBQXVFLElBQUlDLEtBQUo7QUFBVXhCLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZLGNBQVosRUFBMkI7QUFBQ3VCLE9BQUssQ0FBQ3JCLENBQUQsRUFBRztBQUFDcUIsU0FBSyxHQUFDckIsQ0FBTjtBQUFROztBQUFsQixDQUEzQixFQUErQyxDQUEvQztBQUFrRCxJQUFJc0IsTUFBSjtBQUFXekIsTUFBTSxDQUFDQyxJQUFQLENBQVksZUFBWixFQUE0QjtBQUFDd0IsUUFBTSxDQUFDdEIsQ0FBRCxFQUFHO0FBQUNzQixVQUFNLEdBQUN0QixDQUFQO0FBQVM7O0FBQXBCLENBQTVCLEVBQWtELENBQWxEO0FBQXFELElBQUl1QixLQUFKO0FBQVUxQixNQUFNLENBQUNDLElBQVAsQ0FBWSxjQUFaLEVBQTJCO0FBQUN5QixPQUFLLENBQUN2QixDQUFELEVBQUc7QUFBQ3VCLFNBQUssR0FBQ3ZCLENBQU47QUFBUTs7QUFBbEIsQ0FBM0IsRUFBK0MsQ0FBL0M7QUFJdE0sTUFBTUMsUUFBUSxHQUFHLElBQUlvQixLQUFLLENBQUNHLFVBQVYsQ0FBcUIsVUFBckIsQ0FBakI7QUFDQSxNQUFNSixhQUFhLEdBQUcsSUFBSUMsS0FBSyxDQUFDRyxVQUFWLENBQXFCLGVBQXJCLENBQXRCOztBQUVQLElBQUdGLE1BQU0sQ0FBQ0csUUFBVixFQUFtQjtBQUNmSCxRQUFNLENBQUNJLE9BQVAsQ0FBZSxVQUFmLEVBQTJCLFNBQVNDLG1CQUFULEdBQThCO0FBQ3JELFdBQU8xQixRQUFRLENBQUMyQixJQUFULENBQWMsRUFBZCxDQUFQO0FBQ0gsR0FGRDtBQUdBTixRQUFNLENBQUNJLE9BQVAsQ0FBZSxlQUFmLEVBQWdDLFlBQVU7QUFDdEMsV0FBT04sYUFBYSxDQUFDUSxJQUFkLENBQW1CLEVBQW5CLENBQVA7QUFDSCxHQUZEO0FBR0g7O0FBRUROLE1BQU0sQ0FBQ08sT0FBUCxDQUFlO0FBQ1gsZ0JBQWNsQixLQUFkLEVBQXFCbUIsTUFBckIsRUFBNkJDLFFBQTdCLEVBQXVDQyxTQUF2QyxFQUFrREMsT0FBbEQsRUFBMEQ7QUFDdERWLFNBQUssQ0FBQ1osS0FBRCxFQUFRdUIsTUFBUixDQUFMO0FBQ0FYLFNBQUssQ0FBQ08sTUFBRCxFQUFTSSxNQUFULENBQUw7QUFDQVgsU0FBSyxDQUFDUSxRQUFELEVBQVdHLE1BQVgsQ0FBTDtBQUNBWCxTQUFLLENBQUNTLFNBQUQsRUFBWUUsTUFBWixDQUFMO0FBQ0FYLFNBQUssQ0FBQ1UsT0FBRCxFQUFVQyxNQUFWLENBQUw7QUFFQWpDLFlBQVEsQ0FBQ2tDLE1BQVQsQ0FBZ0I7QUFDWnhCLFdBRFk7QUFFWm1CLFlBRlk7QUFHWkMsY0FIWTtBQUlaQyxlQUpZO0FBS1pDLGFBTFk7QUFNWkcsZUFBUyxFQUFFLElBQUlDLElBQUo7QUFOQyxLQUFoQjtBQVFILEdBaEJVOztBQWlCWCxnQkFBY0MsTUFBZCxFQUFxQjtBQUNqQmYsU0FBSyxDQUFDZSxNQUFELEVBQVNKLE1BQVQsQ0FBTDtBQUNBakMsWUFBUSxDQUFDc0MsTUFBVCxDQUFnQkQsTUFBaEI7QUFDSCxHQXBCVTs7QUFxQlgsaUJBQWVBLE1BQWYsRUFBdUJFLE9BQXZCLEVBQWdDO0FBRTVCakIsU0FBSyxDQUFDZSxNQUFELEVBQVNKLE1BQVQsQ0FBTDtBQUVBWCxTQUFLLENBQUNpQixPQUFELEVBQVVDLE9BQVYsQ0FBTDtBQUNBeEMsWUFBUSxDQUFDeUMsTUFBVCxDQUFnQkosTUFBaEIsRUFBd0I7QUFBRUssVUFBSSxFQUFFO0FBQUVDLGVBQU8sRUFBRUo7QUFBWDtBQUFSLEtBQXhCO0FBQ0gsR0EzQlU7O0FBNEJYLGdCQUFjRixNQUFkLEVBQXNCM0IsS0FBdEIsRUFBNkJrQyxJQUE3QixFQUFrQztBQUM5QnRCLFNBQUssQ0FBQ2UsTUFBRCxFQUFTSixNQUFULENBQUw7QUFDQVgsU0FBSyxDQUFDWixLQUFELEVBQVF1QixNQUFSLENBQUw7QUFDQVgsU0FBSyxDQUFDc0IsSUFBRCxFQUFPWCxNQUFQLENBQUw7QUFDQWpDLFlBQVEsQ0FBQ3lDLE1BQVQsQ0FBZ0JKLE1BQWhCLEVBQXdCO0FBQUVLLFVBQUksRUFBRTtBQUFFaEMsYUFBSyxFQUFFQSxLQUFUO0FBQWlCbUIsY0FBTSxFQUFFZTtBQUF6QjtBQUFSLEtBQXhCO0FBQ0gsR0FqQ1U7O0FBa0NYLG1CQUFpQlAsTUFBakIsRUFBeUJRLFVBQXpCLEVBQW9DO0FBQ2hDN0MsWUFBUSxDQUFDeUMsTUFBVCxDQUFnQkosTUFBaEIsRUFBd0I7QUFBRVMsV0FBSyxFQUFFO0FBQUVDLGlCQUFTLEVBQUVGO0FBQWI7QUFBVCxLQUF4QjtBQUNILEdBcENVOztBQXFDWCxxQkFBbUJSLE1BQW5CLEVBQTJCM0IsS0FBM0IsRUFBa0NvQixRQUFsQyxFQUE0Q0MsU0FBNUMsRUFBc0Q7QUFDbERULFNBQUssQ0FBQ2UsTUFBRCxFQUFTSixNQUFULENBQUw7QUFDQVgsU0FBSyxDQUFDWixLQUFELEVBQVF1QixNQUFSLENBQUw7QUFDQVgsU0FBSyxDQUFDUyxTQUFELEVBQVlFLE1BQVosQ0FBTDtBQUNBWCxTQUFLLENBQUNRLFFBQUQsRUFBV0csTUFBWCxDQUFMO0FBQ0RkLGlCQUFhLENBQUNlLE1BQWQsQ0FBcUI7QUFBQ0csWUFBRDtBQUFRM0IsV0FBUjtBQUFjb0IsY0FBZDtBQUF3QkMsZUFBeEI7QUFBbUNnQixlQUFTLEVBQUMsRUFBN0M7QUFBaURaLGVBQVMsRUFBRSxJQUFJQyxJQUFKO0FBQTVELEtBQXJCO0FBQ0YsR0EzQ1U7O0FBNENYLGtCQUFnQkMsTUFBaEIsRUFBd0JXLFdBQXhCLEVBQW9DO0FBQ2hDMUIsU0FBSyxDQUFDZSxNQUFELEVBQVNKLE1BQVQsQ0FBTDtBQUNBWCxTQUFLLENBQUMwQixXQUFELEVBQWNmLE1BQWQsQ0FBTCxDQUZnQyxDQUdoQztBQUNBO0FBQ0E7O0FBQ0FkLGlCQUFhLENBQUNzQixNQUFkLENBQXFCO0FBQUNKLFlBQU0sRUFBRUE7QUFBVCxLQUFyQixFQUF1QztBQUFDUyxXQUFLLEVBQUU7QUFBQ0MsaUJBQVMsRUFBRUM7QUFBWjtBQUFSLEtBQXZDO0FBQ0gsR0FuRFU7O0FBb0RYLDhCQUE0QkMsS0FBNUIsRUFBbUNDLEtBQW5DLEVBQXlDO0FBQ3JDL0IsaUJBQWEsQ0FBQ3NCLE1BQWQsQ0FBcUI7QUFBQyxnQkFBU1E7QUFBVixLQUFyQixFQUF1QztBQUFDRSxXQUFLLEVBQUU7QUFBQyxxQkFBYUQ7QUFBZDtBQUFSLEtBQXZDO0FBQ0EvQixpQkFBYSxDQUFDc0IsTUFBZCxDQUFxQjtBQUFDLGdCQUFTUztBQUFWLEtBQXJCLEVBQXVDO0FBQUNDLFdBQUssRUFBRTtBQUFDLHFCQUFhRjtBQUFkO0FBQVIsS0FBdkM7QUFDSCxHQXZEVTs7QUF3RFgscUJBQW1CWixNQUFuQixFQUEwQjtBQUN0QmYsU0FBSyxDQUFDZSxNQUFELEVBQVNKLE1BQVQsQ0FBTDtBQUNBZCxpQkFBYSxDQUFDc0IsTUFBZCxDQUFxQjtBQUFDLG1CQUFZSjtBQUFiLEtBQXJCLEVBQTJDO0FBQUNjLFdBQUssRUFBQztBQUFDLHFCQUFhZDtBQUFkO0FBQVAsS0FBM0MsRUFBMEU7QUFBQ2UsV0FBSyxFQUFFO0FBQVIsS0FBMUU7QUFDQWpDLGlCQUFhLENBQUNtQixNQUFkLENBQXFCO0FBQUNELFlBQU0sRUFBRUE7QUFBVCxLQUFyQjtBQUNIOztBQTVEVSxDQUFmLEU7Ozs7Ozs7Ozs7O0FDaEJBLElBQUloQixNQUFKO0FBQVd6QixNQUFNLENBQUNDLElBQVAsQ0FBWSxlQUFaLEVBQTRCO0FBQUN3QixRQUFNLENBQUN0QixDQUFELEVBQUc7QUFBQ3NCLFVBQU0sR0FBQ3RCLENBQVA7QUFBUzs7QUFBcEIsQ0FBNUIsRUFBa0QsQ0FBbEQ7QUFBcURILE1BQU0sQ0FBQ0MsSUFBUCxDQUFZLDJCQUFaO0FBQXlDRCxNQUFNLENBQUNDLElBQVAsQ0FBWSx1QkFBWjtBQUl6R3dCLE1BQU0sQ0FBQ2dDLE9BQVAsQ0FBZSxNQUFNLENBQ3BCLENBREQsRSIsImZpbGUiOiIvYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFRhYnVsYXIgZnJvbSBcIm1ldGVvci9hbGRlZWQ6dGFidWxhclwiO1xuaW1wb3J0IHsgVXNlcmluZm8gfSBmcm9tIFwiLi4vLi4vaW1wb3J0L2FwaS91c2VyaW5mby5qc1wiO1xuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQnO1xuXG5UYWJ1bGFyVGFibGVzID0ge307XG5cblRhYnVsYXJUYWJsZXMuVXNlcnMgPSBuZXcgVGFidWxhci5UYWJsZSh7XG4gICAgbmFtZTogXCJVc2Vyc1wiLFxuICAgIGNvbGxlY3Rpb246IFVzZXJpbmZvLFxuICAgIHB1YjogXCJVc2VyaW5mb1wiLFxuICAgIGNvbHVtbnM6IFtcbiAgICAgICB7XG4gICAgICAgICAgZGF0YTpcInRpdGxlXCIsXG4gICAgICAgICAgdGl0bGU6XCJUaXRsZVwiLFxuICAgICAgIH0sXG4gICAgICAge1xuICAgICAgICAgIGRhdGE6XCJkZXZpY2VcIixcbiAgICAgICAgICB0aXRsZTpcIkRldmljZVwiLFxuICAgICAgIH0sXG4gICAgICAge1xuICAgICAgICAgIGRhdGE6XCJjb3VudHJ5XCIsXG4gICAgICAgICAgdGl0bGU6XCJDb3VudHJ5XCIsXG4gICAgICAgfSxcbiAgICAgICB7XG4gICAgICAgICAgZGF0YTpcImNyZWF0ZWRBdFwiLFxuICAgICAgICAgIHRpdGxlOlwiQ3JlYXRlZEF0XCIsXG4gICAgICAgICAgcmVuZGVyOmZ1bmN0aW9uICh2YWwpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIG1vbWVudCh2YWwpLmZvcm1hdCgnREQvTU0vWVlZWSBIOm1tJyk7XG4gICAgICAgICAgfSxcbiAgICAgICB9LFxuICAgXSxcbiAgICBpbml0Q29tcGxldGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAkKCcuZGF0YVRhYmxlc19lbXB0eScpLmh0bWwoJ3Byb2Nlc3NpbmcnKTtcbiAgICB9LFxufSlcblxuZXhwb3J0IGRlZmF1bHQgVGFidWxhclRhYmxlczsiLCJpbXBvcnQgeyBNb25nbyB9IGZyb20gXCJtZXRlb3IvbW9uZ29cIjtcbmltcG9ydCB7IE1ldGVvciB9IGZyb20gXCJtZXRlb3IvbWV0ZW9yXCI7XG5pbXBvcnQgeyBjaGVjayB9IGZyb20gJ21ldGVvci9jaGVjayc7XG5cbmV4cG9ydCBjb25zdCBVc2VyaW5mbyA9IG5ldyBNb25nby5Db2xsZWN0aW9uKCd1c2VyaW5mbycpO1xuZXhwb3J0IGNvbnN0IE5laWdoYm91ckluZm8gPSBuZXcgTW9uZ28uQ29sbGVjdGlvbignbmVpZ2hib3VySW5mbycpO1xuXG5pZihNZXRlb3IuaXNTZXJ2ZXIpe1xuICAgIE1ldGVvci5wdWJsaXNoKCd1c2VyaW5mbycsIGZ1bmN0aW9uIHVzZXJpbmZvUHVibGljYXRpb24oKXtcbiAgICAgICAgcmV0dXJuIFVzZXJpbmZvLmZpbmQoe30pO1xuICAgIH0pXG4gICAgTWV0ZW9yLnB1Ymxpc2goJ25laWdoYm91ckluZm8nLCBmdW5jdGlvbigpe1xuICAgICAgICByZXR1cm4gTmVpZ2hib3VySW5mby5maW5kKHt9KTtcbiAgICB9KVxufVxuXG5NZXRlb3IubWV0aG9kcyh7XG4gICAgJ3VzZXIuaW5zZXJ0Jyh0aXRsZSwgZGV2aWNlICxsYXRpdHVkZSwgbG9uZ2l0dWRlLCBjb3VudHJ5KXtcbiAgICAgICAgY2hlY2sodGl0bGUsIFN0cmluZyk7XG4gICAgICAgIGNoZWNrKGRldmljZSwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2sobGF0aXR1ZGUsIFN0cmluZyk7XG4gICAgICAgIGNoZWNrKGxvbmdpdHVkZSwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2soY291bnRyeSwgU3RyaW5nKTtcblxuICAgICAgICBVc2VyaW5mby5pbnNlcnQoe1xuICAgICAgICAgICAgdGl0bGUsXG4gICAgICAgICAgICBkZXZpY2UsXG4gICAgICAgICAgICBsYXRpdHVkZSxcbiAgICAgICAgICAgIGxvbmdpdHVkZSxcbiAgICAgICAgICAgIGNvdW50cnksXG4gICAgICAgICAgICBjcmVhdGVkQXQ6IG5ldyBEYXRlKCksXG4gICAgICAgIH0pO1xuICAgIH0sXG4gICAgJ3VzZXIucmVtb3ZlJyh1c2VySUQpe1xuICAgICAgICBjaGVjayh1c2VySUQsIFN0cmluZyk7XG4gICAgICAgIFVzZXJpbmZvLnJlbW92ZSh1c2VySUQpO1xuICAgIH0sXG4gICAgJ3VzZXIuc2V0RWRpdCcodXNlcklELCBzZXRFZGl0KSB7XG5cbiAgICAgICAgY2hlY2sodXNlcklELCBTdHJpbmcpO1xuICAgIFxuICAgICAgICBjaGVjayhzZXRFZGl0LCBCb29sZWFuKTtcbiAgICAgICAgVXNlcmluZm8udXBkYXRlKHVzZXJJRCwgeyAkc2V0OiB7IGVkaXRpbmc6IHNldEVkaXQgfSB9KTtcbiAgICB9LFxuICAgICd1c2VyLnVwZGF0ZScodXNlcklELCB0aXRsZSwgZGV2aSl7XG4gICAgICAgIGNoZWNrKHVzZXJJRCwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2sodGl0bGUsIFN0cmluZyk7XG4gICAgICAgIGNoZWNrKGRldmksIFN0cmluZyk7XG4gICAgICAgIFVzZXJpbmZvLnVwZGF0ZSh1c2VySUQsIHsgJHNldDogeyB0aXRsZTogdGl0bGUsICBkZXZpY2U6IGRldml9IH0gKTtcbiAgICB9LFxuICAgICd1c2VyLmFkZE5laWdoYicodXNlcklELCBuZXh0VXNlcklEKXtcbiAgICAgICAgVXNlcmluZm8udXBkYXRlKHVzZXJJRCwgeyAkcHVzaDogeyBuZWlnaGJvdXI6IG5leHRVc2VySUR9fSk7XG4gICAgfSxcbiAgICAnbmVpZ2hib3VyLmluc2VydCcodXNlcklELCB0aXRsZSwgbGF0aXR1ZGUsIGxvbmdpdHVkZSl7XG4gICAgICAgIGNoZWNrKHVzZXJJRCwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2sodGl0bGUsIFN0cmluZyk7XG4gICAgICAgIGNoZWNrKGxvbmdpdHVkZSwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2sobGF0aXR1ZGUsIFN0cmluZyk7XG4gICAgICAgTmVpZ2hib3VySW5mby5pbnNlcnQoe3VzZXJJRCx0aXRsZSxsYXRpdHVkZSwgbG9uZ2l0dWRlLCBuZWlnaGJvdXI6W10sIGNyZWF0ZWRBdDogbmV3IERhdGUoKX0pXG4gICAgfSxcbiAgICAnbmVpZ2hib3VyLmFkZCcodXNlcklELCBuZWlnaGJvdXJJRCl7XG4gICAgICAgIGNoZWNrKHVzZXJJRCwgU3RyaW5nKTtcbiAgICAgICAgY2hlY2sobmVpZ2hib3VySUQsIFN0cmluZyk7XG4gICAgICAgIC8vTWV0aG9kIDE6XG4gICAgICAgIC8vIGlmKE5laWdoYm91ckluZm8uZmluZCh7dXNlcklEOiB1c2VySUR9KS5mZXRjaCgpWzBdLm5laWdoYm91ci5pbmNsdWRlcyhuZWlnaGJvdXJJRCkpe1xuICAgICAgICAvLyBNZXRob2QgMjpcbiAgICAgICAgTmVpZ2hib3VySW5mby51cGRhdGUoe3VzZXJJRDogdXNlcklEfSwgeyRwdXNoOiB7bmVpZ2hib3VyOiBuZWlnaGJvdXJJRH19KTtcbiAgICB9LFxuICAgICduZWlnaGJvdXIucmVtb3ZlTmVpZ2hib3VyJyh1c2VyMSwgdXNlcjIpe1xuICAgICAgICBOZWlnaGJvdXJJbmZvLnVwZGF0ZSh7XCJ1c2VySURcIjp1c2VyMX0sIHskcHVsbDoge1wibmVpZ2hib3VyXCI6IHVzZXIyfX0pO1xuICAgICAgICBOZWlnaGJvdXJJbmZvLnVwZGF0ZSh7XCJ1c2VySURcIjp1c2VyMn0sIHskcHVsbDoge1wibmVpZ2hib3VyXCI6IHVzZXIxfX0pO1xuICAgIH0sXG4gICAgJ25laWdoYm91ci5yZW1vdmUnKHVzZXJJRCl7XG4gICAgICAgIGNoZWNrKHVzZXJJRCwgU3RyaW5nKTtcbiAgICAgICAgTmVpZ2hib3VySW5mby51cGRhdGUoe1wibmVpZ2hib3VyXCI6dXNlcklEfSwgeyRwdWxsOntcIm5laWdoYm91clwiOiB1c2VySUR9fSwge211bHRpOiB0cnVlfSk7XG4gICAgICAgIE5laWdoYm91ckluZm8ucmVtb3ZlKHt1c2VySUQ6IHVzZXJJRH0pO1xuICAgIH0sXG59KSIsImltcG9ydCB7IE1ldGVvciB9IGZyb20gJ21ldGVvci9tZXRlb3InO1xuaW1wb3J0ICcuLi9pbXBvcnQvYXBpL3VzZXJpbmZvLmpzJztcbmltcG9ydCBcIi4uL3Jvb3QvbGliL3RhYmxlcy5qc1wiXG5cbk1ldGVvci5zdGFydHVwKCgpID0+IHtcbn0pO1xuIl19
